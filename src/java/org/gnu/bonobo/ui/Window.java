/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.bonobo.ui;

import org.gnu.glib.Handle;

/**
 */
public class Window extends org.gnu.gtk.Window {
	
	public Window(Handle handle) {
		super(handle);
	}


	native static final protected int bonobo_window_get_type();
	native static final protected Handle bonobo_window_new(String name, String title);
	native static final protected void bonobo_window_set_contents(Handle win, Handle contents);
	native static final protected Handle bonobo_window_get_contents(Handle win);
	native static final protected Handle bonobo_window_get_ui_engine(Handle win);
	native static final protected Handle bonobo_window_get_ui_container(Handle win);
	native static final protected void bonobo_window_set_name(Handle win, String name);
	native static final protected String bonobo_window_get_name(Handle win);
	native static final protected Handle bonobo_window_get_accel_group(Handle win);
	native static final protected void bonobo_window_add_popup(Handle win, Handle popup, String path);
	

}
