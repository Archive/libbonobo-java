/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.bonobo.ui;

import org.gnu.glib.Handle;
import org.gnu.gtk.Bin;

/**
 * The purpose of Widget is to make container-side use of Bonobo very simple.
 * The widget has two functions:
 * 
 * <ol>
 * <li>Provide a simple wrapper for embedding Controls. 
 * <li>To provide a simple wrapper for using Monikers.
 * </ol>
 */
public class Widget extends Bin {
	
	public Widget(Handle handle) {
		super(handle);
	}


	native static final protected Handle bonobo_widget_get_objref(Handle widget);
    native static final protected Handle bonobo_widget_new_control(String moniker, Handle uiContainer);
    native static final protected Handle bonobo_widget_new_control_from_objref(Handle control, Handle uiContainer);
    native static final protected Handle bonobo_widget_get_control_frame(Handle widget);
    native static final protected Handle bonobo_widget_get_ui_container(Handle widget);
    

}
