/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.bonobo.ui;

import org.gnu.bonobo.BonoboObject;
import org.gnu.glib.Handle;

/**
 */
public class UIComponent extends BonoboObject {


	native static final protected int bonobo_ui_component_get_type();
	native static final protected Handle bonobo_ui_component_new(String name);
	native static final protected Handle bonobo_ui_component_new_default();
	native static final protected void bonobo_ui_component_set_name(Handle comp, String name);
	native static final protected String bonobo_ui_component_get_name(Handle comp);
	native static final protected void bonobo_ui_component_set_container(Handle comp, Handle cont, int optEv);
	native static final protected void bonobo_ui_component_unset_container(Handle comp, int optEv);
	native static final protected int bonobo_ui_component_get_container(Handle comp);
	

}
