/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.bonobo;

import org.gnu.glib.GObject;
import org.gnu.glib.Handle;

/**
 */
public class BonoboObject extends GObject {


	native static final protected int bonobo_object_get_type();
	native static final protected void bonobo_object_add_interface(Handle obj, Handle newobj);
	native static final protected Handle bonobo_object_query_local_interface(Handle obj, String repoId);
	native static final protected Handle bonobo_object_ref(Handle obj);
	native static final protected Handle bonobo_object_unref(Handle obj);

}
