package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "BadArg"
 *	@author JacORB IDL compiler 
 */

public final class BadArgHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.BadArg value;

	public BadArgHolder ()
	{
	}
	public BadArgHolder(final org.gnu.bonobo.BadArg initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.BadArgHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.BadArgHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.BadArgHelper.write(_out, value);
	}
}
