package org.gnu.bonobo;

/**
 *	Generated from IDL definition of union "ActivationPropertyValue"
 *	@author JacORB IDL compiler 
 */

public final class ActivationPropertyValue
	implements org.omg.CORBA.portable.IDLEntity
{
	private org.gnu.bonobo.ActivationPropertyType discriminator;
	private java.lang.String value_string;
	private double value_number;
	private boolean value_boolean;
	private java.lang.String[] value_stringv;

	public ActivationPropertyValue ()
	{
	}

	public org.gnu.bonobo.ActivationPropertyType discriminator ()
	{
		return discriminator;
	}

	public java.lang.String value_string ()
	{
		if (discriminator != org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_STRING)
			throw new org.omg.CORBA.BAD_OPERATION();
		return value_string;
	}

	public void value_string (java.lang.String _x)
	{
		discriminator = org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_STRING;
		value_string = _x;
	}

	public double value_number ()
	{
		if (discriminator != org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_NUMBER)
			throw new org.omg.CORBA.BAD_OPERATION();
		return value_number;
	}

	public void value_number (double _x)
	{
		discriminator = org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_NUMBER;
		value_number = _x;
	}

	public boolean value_boolean ()
	{
		if (discriminator != org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_BOOLEAN)
			throw new org.omg.CORBA.BAD_OPERATION();
		return value_boolean;
	}

	public void value_boolean (boolean _x)
	{
		discriminator = org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_BOOLEAN;
		value_boolean = _x;
	}

	public java.lang.String[] value_stringv ()
	{
		if (discriminator != org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_STRINGV)
			throw new org.omg.CORBA.BAD_OPERATION();
		return value_stringv;
	}

	public void value_stringv (java.lang.String[] _x)
	{
		discriminator = org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_STRINGV;
		value_stringv = _x;
	}

}
