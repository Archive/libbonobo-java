package org.gnu.bonobo.StreamPackage;
/**
 *	Generated from IDL definition of enum "SeekType"
 *	@author JacORB IDL compiler 
 */

public final class SeekTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public SeekType value;

	public SeekTypeHolder ()
	{
	}
	public SeekTypeHolder (final SeekType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return SeekTypeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = SeekTypeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		SeekTypeHelper.write (out,value);
	}
}
