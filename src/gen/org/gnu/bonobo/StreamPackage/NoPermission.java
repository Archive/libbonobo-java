package org.gnu.bonobo.StreamPackage;

/**
 *	Generated from IDL definition of exception "NoPermission"
 *	@author JacORB IDL compiler 
 */

public final class NoPermission
	extends org.omg.CORBA.UserException
{
	public NoPermission()
	{
		super(org.gnu.bonobo.StreamPackage.NoPermissionHelper.id());
	}

	public NoPermission(String value)
	{
		super(value);
	}
}
