package org.gnu.bonobo.StreamPackage;
/**
 *	Generated from IDL definition of enum "SeekType"
 *	@author JacORB IDL compiler 
 */

public final class SeekType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _SeekSet = 0;
	public static final SeekType SeekSet = new SeekType(_SeekSet);
	public static final int _SeekCur = 1;
	public static final SeekType SeekCur = new SeekType(_SeekCur);
	public static final int _SeekEnd = 2;
	public static final SeekType SeekEnd = new SeekType(_SeekEnd);
	public int value()
	{
		return value;
	}
	public static SeekType from_int(int value)
	{
		switch (value) {
			case _SeekSet: return SeekSet;
			case _SeekCur: return SeekCur;
			case _SeekEnd: return SeekEnd;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected SeekType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
