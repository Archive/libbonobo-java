package org.gnu.bonobo.StreamPackage;
/**
 *	Generated from IDL definition of enum "SeekType"
 *	@author JacORB IDL compiler 
 */

public final class SeekTypeHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.StreamPackage.SeekTypeHelper.id(),"SeekType",new String[]{"SeekSet","SeekCur","SeekEnd"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.StreamPackage.SeekType s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.StreamPackage.SeekType extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Stream/SeekType:1.0";
	}
	public static SeekType read (final org.omg.CORBA.portable.InputStream in)
	{
		return SeekType.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final SeekType s)
	{
		out.write_long(s.value());
	}
}
