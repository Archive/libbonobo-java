package org.gnu.bonobo.StreamPackage;

/**
 *	Generated from IDL definition of alias "iobuf"
 *	@author JacORB IDL compiler 
 */

public final class iobufHolder
	implements org.omg.CORBA.portable.Streamable
{
	public byte[] value;

	public iobufHolder ()
	{
	}
	public iobufHolder (final byte[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return iobufHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = iobufHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		iobufHelper.write (out,value);
	}
}
