package org.gnu.bonobo.StreamPackage;

/**
 *	Generated from IDL definition of exception "NoPermission"
 *	@author JacORB IDL compiler 
 */

public final class NoPermissionHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StreamPackage.NoPermission value;

	public NoPermissionHolder ()
	{
	}
	public NoPermissionHolder(final org.gnu.bonobo.StreamPackage.NoPermission initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StreamPackage.NoPermissionHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StreamPackage.NoPermissionHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StreamPackage.NoPermissionHelper.write(_out, value);
	}
}
