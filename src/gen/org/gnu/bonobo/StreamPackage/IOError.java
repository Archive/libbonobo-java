package org.gnu.bonobo.StreamPackage;

/**
 *	Generated from IDL definition of exception "IOError"
 *	@author JacORB IDL compiler 
 */

public final class IOError
	extends org.omg.CORBA.UserException
{
	public IOError()
	{
		super(org.gnu.bonobo.StreamPackage.IOErrorHelper.id());
	}

	public IOError(String value)
	{
		super(value);
	}
}
