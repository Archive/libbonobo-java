package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "StorageType"
 *	@author JacORB IDL compiler 
 */

public final class StorageTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public StorageType value;

	public StorageTypeHolder ()
	{
	}
	public StorageTypeHolder (final StorageType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return StorageTypeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = StorageTypeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		StorageTypeHelper.write (out,value);
	}
}
