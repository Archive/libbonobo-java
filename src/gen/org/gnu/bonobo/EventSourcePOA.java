package org.gnu.bonobo;

/**
 *	Generated from IDL interface "EventSource"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class EventSourcePOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.EventSourceOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "ref", new java.lang.Integer(0));
		m_opsHash.put ( "unref", new java.lang.Integer(1));
		m_opsHash.put ( "unImplemented", new java.lang.Integer(2));
		m_opsHash.put ( "addListenerWithMask", new java.lang.Integer(3));
		m_opsHash.put ( "addListener", new java.lang.Integer(4));
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(5));
		m_opsHash.put ( "removeListener", new java.lang.Integer(6));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(7));
	}
	private String[] ids = {"IDL:Bonobo/EventSource:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.EventSource _this()
	{
		return org.gnu.bonobo.EventSourceHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.EventSource _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.EventSourceHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 1: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 2: // unImplemented
			{
				_out = handler.createReply();
				unImplemented();
				break;
			}
			case 3: // addListenerWithMask
			{
				org.gnu.bonobo.Listener _arg0=org.gnu.bonobo.ListenerHelper.read(_input);
				java.lang.String _arg1=_input.read_string();
				_out = handler.createReply();
				addListenerWithMask(_arg0,_arg1);
				break;
			}
			case 4: // addListener
			{
				org.gnu.bonobo.Listener _arg0=org.gnu.bonobo.ListenerHelper.read(_input);
				_out = handler.createReply();
				addListener(_arg0);
				break;
			}
			case 5: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 6: // removeListener
			{
			try
			{
				org.gnu.bonobo.Listener _arg0=org.gnu.bonobo.ListenerHelper.read(_input);
				_out = handler.createReply();
				removeListener(_arg0);
			}
			catch(org.gnu.bonobo.EventSourcePackage.UnknownListener _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.EventSourcePackage.UnknownListenerHelper.write(_out, _ex0);
			}
				break;
			}
			case 7: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
