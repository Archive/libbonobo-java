package org.gnu.bonobo.EventSourcePackage;

/**
 *	Generated from IDL definition of exception "UnknownListener"
 *	@author JacORB IDL compiler 
 */

public final class UnknownListenerHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.EventSourcePackage.UnknownListener value;

	public UnknownListenerHolder ()
	{
	}
	public UnknownListenerHolder(final org.gnu.bonobo.EventSourcePackage.UnknownListener initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.EventSourcePackage.UnknownListenerHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.EventSourcePackage.UnknownListenerHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.EventSourcePackage.UnknownListenerHelper.write(_out, value);
	}
}
