package org.gnu.bonobo.EventSourcePackage;

/**
 *	Generated from IDL definition of exception "UnknownListener"
 *	@author JacORB IDL compiler 
 */

public final class UnknownListener
	extends org.omg.CORBA.UserException
{
	public UnknownListener()
	{
		super(org.gnu.bonobo.EventSourcePackage.UnknownListenerHelper.id());
	}

	public UnknownListener(String value)
	{
		super(value);
	}
}
