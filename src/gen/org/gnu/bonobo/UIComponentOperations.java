package org.gnu.bonobo;

/**
 *	Generated from IDL interface "UIComponent"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface UIComponentOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	void setContainer(org.gnu.bonobo.UIContainer container);
	void unsetContainer();
	java.lang.String name();
	java.lang.String describeVerbs();
	void execVerb(java.lang.String cname);
	void uiEvent(java.lang.String id, org.gnu.bonobo.UIComponentPackage.EventType type, java.lang.String state);
	void unImplemented();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
