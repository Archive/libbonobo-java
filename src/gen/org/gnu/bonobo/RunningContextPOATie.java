package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "RunningContext"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class RunningContextPOATie
	extends RunningContextPOA
{
	private RunningContextOperations _delegate;

	private POA _poa;
	public RunningContextPOATie(RunningContextOperations delegate)
	{
		_delegate = delegate;
	}
	public RunningContextPOATie(RunningContextOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.RunningContext _this()
	{
		return org.gnu.bonobo.RunningContextHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.RunningContext _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.RunningContextHelper.narrow(_this_object(orb));
	}
	public RunningContextOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(RunningContextOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void ref()
	{
_delegate.ref();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void removeKey(java.lang.String key)
	{
_delegate.removeKey(key);
	}

	public void atExitUnref(org.omg.CORBA.Object obj)
	{
_delegate.atExitUnref(obj);
	}

	public void removeObject(org.omg.CORBA.Object obj)
	{
_delegate.removeObject(obj);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public void addKey(java.lang.String key)
	{
_delegate.addKey(key);
	}

	public void addObject(org.omg.CORBA.Object obj)
	{
_delegate.addObject(obj);
	}

}
