package org.gnu.bonobo;

/**
 *	Generated from IDL interface "PersistFile"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface PersistFileOperations
	extends org.gnu.bonobo.PersistOperations
{
	/* constants */
	/* operations  */
	void load(java.lang.String uri) throws org.gnu.bonobo.PersistPackage.WrongDataType,org.gnu.bonobo.IOError,org.gnu.bonobo.PersistPackage.FileNotFound,org.gnu.bonobo.NotSupported;
	void save(java.lang.String uri) throws org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported;
	java.lang.String getCurrentFile() throws org.gnu.bonobo.PersistFilePackage.NoCurrentName;
	void unImplemented3();
	void unImplemented4();
}
