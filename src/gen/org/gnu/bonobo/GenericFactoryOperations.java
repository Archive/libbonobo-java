package org.gnu.bonobo;

/**
 *	Generated from IDL interface "GenericFactory"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface GenericFactoryOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	org.omg.CORBA.Object createObject(java.lang.String iid) throws org.gnu.bonobo.GenericFactoryPackage.CannotActivate;
}
