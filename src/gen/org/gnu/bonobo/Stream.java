package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Stream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public interface Stream
	extends StreamOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity, org.gnu.bonobo.Unknown
{
}
