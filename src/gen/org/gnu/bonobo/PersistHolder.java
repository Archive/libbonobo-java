package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Persist"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class PersistHolder	implements org.omg.CORBA.portable.Streamable{
	 public Persist value;
	public PersistHolder()
	{
	}
	public PersistHolder (final Persist initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return PersistHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = PersistHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		PersistHelper.write (_out,value);
	}
}
