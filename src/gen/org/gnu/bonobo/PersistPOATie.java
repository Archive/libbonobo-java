package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Persist"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class PersistPOATie
	extends PersistPOA
{
	private PersistOperations _delegate;

	private POA _poa;
	public PersistPOATie(PersistOperations delegate)
	{
		_delegate = delegate;
	}
	public PersistPOATie(PersistOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Persist _this()
	{
		return org.gnu.bonobo.PersistHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Persist _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.PersistHelper.narrow(_this_object(orb));
	}
	public PersistOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(PersistOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void ref()
	{
_delegate.ref();
	}

	public java.lang.String getIId()
	{
		return _delegate.getIId();
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public void unref()
	{
_delegate.unref();
	}

	public java.lang.String[] getContentTypes()
	{
		return _delegate.getContentTypes();
	}

	public boolean isDirty()
	{
		return _delegate.isDirty();
	}

	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
