package org.gnu.bonobo;

/**
 *	Generated from IDL interface "MonikerExtender"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class MonikerExtenderHolder	implements org.omg.CORBA.portable.Streamable{
	 public MonikerExtender value;
	public MonikerExtenderHolder()
	{
	}
	public MonikerExtenderHolder (final MonikerExtender initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return MonikerExtenderHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = MonikerExtenderHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		MonikerExtenderHelper.write (_out,value);
	}
}
