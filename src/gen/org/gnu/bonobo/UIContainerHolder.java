package org.gnu.bonobo;

/**
 *	Generated from IDL interface "UIContainer"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class UIContainerHolder	implements org.omg.CORBA.portable.Streamable{
	 public UIContainer value;
	public UIContainerHolder()
	{
	}
	public UIContainerHolder (final UIContainer initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return UIContainerHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = UIContainerHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		UIContainerHelper.write (_out,value);
	}
}
