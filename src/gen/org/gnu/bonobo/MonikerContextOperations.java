package org.gnu.bonobo;

/**
 *	Generated from IDL interface "MonikerContext"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface MonikerContextOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	org.gnu.bonobo.Unknown getObject(java.lang.String name, java.lang.String repoId);
	org.gnu.bonobo.Moniker createFromName(java.lang.String name);
	org.gnu.bonobo.MonikerExtender getExtender(java.lang.String monikerPrefix, java.lang.String interfaceId);
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
	void unImplemented5();
	void unImplemented6();
}
