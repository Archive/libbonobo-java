package org.gnu.bonobo.MonikerPackage;

/**
 *	Generated from IDL definition of exception "TimeOut"
 *	@author JacORB IDL compiler 
 */

public final class TimeOut
	extends org.omg.CORBA.UserException
{
	public TimeOut()
	{
		super(org.gnu.bonobo.MonikerPackage.TimeOutHelper.id());
	}

	public TimeOut(String value)
	{
		super(value);
	}
}
