package org.gnu.bonobo.MonikerPackage;

/**
 *	Generated from IDL definition of exception "InvalidSyntax"
 *	@author JacORB IDL compiler 
 */

public final class InvalidSyntax
	extends org.omg.CORBA.UserException
{
	public InvalidSyntax()
	{
		super(org.gnu.bonobo.MonikerPackage.InvalidSyntaxHelper.id());
	}

	public InvalidSyntax(String value)
	{
		super(value);
	}
}
