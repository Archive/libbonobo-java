package org.gnu.bonobo.MonikerPackage;

/**
 *	Generated from IDL definition of exception "TimeOut"
 *	@author JacORB IDL compiler 
 */

public final class TimeOutHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.MonikerPackage.TimeOut value;

	public TimeOutHolder ()
	{
	}
	public TimeOutHolder(final org.gnu.bonobo.MonikerPackage.TimeOut initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.MonikerPackage.TimeOutHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.MonikerPackage.TimeOutHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.MonikerPackage.TimeOutHelper.write(_out, value);
	}
}
