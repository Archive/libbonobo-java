package org.gnu.bonobo.MonikerPackage;

/**
 *	Generated from IDL definition of exception "InterfaceNotFound"
 *	@author JacORB IDL compiler 
 */

public final class InterfaceNotFoundHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.MonikerPackage.InterfaceNotFound value;

	public InterfaceNotFoundHolder ()
	{
	}
	public InterfaceNotFoundHolder(final org.gnu.bonobo.MonikerPackage.InterfaceNotFound initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.MonikerPackage.InterfaceNotFoundHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.MonikerPackage.InterfaceNotFoundHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.MonikerPackage.InterfaceNotFoundHelper.write(_out, value);
	}
}
