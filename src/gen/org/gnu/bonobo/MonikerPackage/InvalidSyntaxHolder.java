package org.gnu.bonobo.MonikerPackage;

/**
 *	Generated from IDL definition of exception "InvalidSyntax"
 *	@author JacORB IDL compiler 
 */

public final class InvalidSyntaxHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.MonikerPackage.InvalidSyntax value;

	public InvalidSyntaxHolder ()
	{
	}
	public InvalidSyntaxHolder(final org.gnu.bonobo.MonikerPackage.InvalidSyntax initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.MonikerPackage.InvalidSyntaxHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.MonikerPackage.InvalidSyntaxHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.MonikerPackage.InvalidSyntaxHelper.write(_out, value);
	}
}
