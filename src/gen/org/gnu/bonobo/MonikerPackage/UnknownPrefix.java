package org.gnu.bonobo.MonikerPackage;

/**
 *	Generated from IDL definition of exception "UnknownPrefix"
 *	@author JacORB IDL compiler 
 */

public final class UnknownPrefix
	extends org.omg.CORBA.UserException
{
	public UnknownPrefix()
	{
		super(org.gnu.bonobo.MonikerPackage.UnknownPrefixHelper.id());
	}

	public UnknownPrefix(String value)
	{
		super(value);
	}
}
