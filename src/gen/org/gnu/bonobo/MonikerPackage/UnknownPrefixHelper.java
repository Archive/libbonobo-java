package org.gnu.bonobo.MonikerPackage;


/**
 *	Generated from IDL definition of exception "UnknownPrefix"
 *	@author JacORB IDL compiler 
 */

public final class UnknownPrefixHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_exception_tc(org.gnu.bonobo.MonikerPackage.UnknownPrefixHelper.id(),"UnknownPrefix",new org.omg.CORBA.StructMember[0]);
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.MonikerPackage.UnknownPrefix s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.MonikerPackage.UnknownPrefix extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Moniker/UnknownPrefix:1.0";
	}
	public static org.gnu.bonobo.MonikerPackage.UnknownPrefix read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.MonikerPackage.UnknownPrefix result = new org.gnu.bonobo.MonikerPackage.UnknownPrefix();
		if (!in.read_string().equals(id())) throw new org.omg.CORBA.MARSHAL("wrong id");
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.MonikerPackage.UnknownPrefix s)
	{
		out.write_string(id());
	}
}
