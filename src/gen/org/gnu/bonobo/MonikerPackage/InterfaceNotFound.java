package org.gnu.bonobo.MonikerPackage;

/**
 *	Generated from IDL definition of exception "InterfaceNotFound"
 *	@author JacORB IDL compiler 
 */

public final class InterfaceNotFound
	extends org.omg.CORBA.UserException
{
	public InterfaceNotFound()
	{
		super(org.gnu.bonobo.MonikerPackage.InterfaceNotFoundHelper.id());
	}

	public InterfaceNotFound(String value)
	{
		super(value);
	}
}
