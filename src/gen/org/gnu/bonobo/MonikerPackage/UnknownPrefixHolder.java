package org.gnu.bonobo.MonikerPackage;

/**
 *	Generated from IDL definition of exception "UnknownPrefix"
 *	@author JacORB IDL compiler 
 */

public final class UnknownPrefixHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.MonikerPackage.UnknownPrefix value;

	public UnknownPrefixHolder ()
	{
	}
	public UnknownPrefixHolder(final org.gnu.bonobo.MonikerPackage.UnknownPrefix initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.MonikerPackage.UnknownPrefixHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.MonikerPackage.UnknownPrefixHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.MonikerPackage.UnknownPrefixHelper.write(_out, value);
	}
}
