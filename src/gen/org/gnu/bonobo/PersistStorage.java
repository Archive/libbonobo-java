package org.gnu.bonobo;

/**
 *	Generated from IDL interface "PersistStorage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public interface PersistStorage
	extends PersistStorageOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity, org.gnu.bonobo.Persist
{
}
