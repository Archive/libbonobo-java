package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ZoomableFrame"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class ZoomableFrameHolder	implements org.omg.CORBA.portable.Streamable{
	 public ZoomableFrame value;
	public ZoomableFrameHolder()
	{
	}
	public ZoomableFrameHolder (final ZoomableFrame initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return ZoomableFrameHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ZoomableFrameHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		ZoomableFrameHelper.write (_out,value);
	}
}
