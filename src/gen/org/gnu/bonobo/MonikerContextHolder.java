package org.gnu.bonobo;

/**
 *	Generated from IDL interface "MonikerContext"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class MonikerContextHolder	implements org.omg.CORBA.portable.Streamable{
	 public MonikerContext value;
	public MonikerContextHolder()
	{
	}
	public MonikerContextHolder (final MonikerContext initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return MonikerContextHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = MonikerContextHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		MonikerContextHelper.write (_out,value);
	}
}
