package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ZoomableFrame"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface ZoomableFrameOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	void onLevelChanged(float zoom_level);
	void onParametersChanged();
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
