package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "Insensitive"
 *	@author JacORB IDL compiler 
 */

public final class Insensitive
	extends org.omg.CORBA.UserException
{
	public Insensitive()
	{
		super(org.gnu.bonobo.UIContainerPackage.InsensitiveHelper.id());
	}

	public Insensitive(String value)
	{
		super(value);
	}
}
