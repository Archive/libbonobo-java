package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "NonExistentAttr"
 *	@author JacORB IDL compiler 
 */

public final class NonExistentAttrHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.UIContainerPackage.NonExistentAttr value;

	public NonExistentAttrHolder ()
	{
	}
	public NonExistentAttrHolder(final org.gnu.bonobo.UIContainerPackage.NonExistentAttr initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.UIContainerPackage.NonExistentAttrHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.UIContainerPackage.NonExistentAttrHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.UIContainerPackage.NonExistentAttrHelper.write(_out, value);
	}
}
