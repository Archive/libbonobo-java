package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "MalformedXML"
 *	@author JacORB IDL compiler 
 */

public final class MalformedXMLHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.UIContainerPackage.MalformedXML value;

	public MalformedXMLHolder ()
	{
	}
	public MalformedXMLHolder(final org.gnu.bonobo.UIContainerPackage.MalformedXML initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.UIContainerPackage.MalformedXMLHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.UIContainerPackage.MalformedXMLHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.UIContainerPackage.MalformedXMLHelper.write(_out, value);
	}
}
