package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "Unknown"
 *	@author JacORB IDL compiler 
 */

public final class UnknownHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.UIContainerPackage.Unknown value;

	public UnknownHolder ()
	{
	}
	public UnknownHolder(final org.gnu.bonobo.UIContainerPackage.Unknown initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.UIContainerPackage.UnknownHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.UIContainerPackage.UnknownHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.UIContainerPackage.UnknownHelper.write(_out, value);
	}
}
