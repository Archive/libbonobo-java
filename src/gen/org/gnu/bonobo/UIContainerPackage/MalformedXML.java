package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "MalformedXML"
 *	@author JacORB IDL compiler 
 */

public final class MalformedXML
	extends org.omg.CORBA.UserException
{
	public MalformedXML()
	{
		super(org.gnu.bonobo.UIContainerPackage.MalformedXMLHelper.id());
	}

	public MalformedXML(String value)
	{
		super(value);
	}
}
