package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "Unknown"
 *	@author JacORB IDL compiler 
 */

public final class Unknown
	extends org.omg.CORBA.UserException
{
	public Unknown()
	{
		super(org.gnu.bonobo.UIContainerPackage.UnknownHelper.id());
	}

	public Unknown(String value)
	{
		super(value);
	}
}
