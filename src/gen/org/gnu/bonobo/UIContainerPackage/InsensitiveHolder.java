package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "Insensitive"
 *	@author JacORB IDL compiler 
 */

public final class InsensitiveHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.UIContainerPackage.Insensitive value;

	public InsensitiveHolder ()
	{
	}
	public InsensitiveHolder(final org.gnu.bonobo.UIContainerPackage.Insensitive initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.UIContainerPackage.InsensitiveHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.UIContainerPackage.InsensitiveHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.UIContainerPackage.InsensitiveHelper.write(_out, value);
	}
}
