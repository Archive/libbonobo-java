package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "InvalidPath"
 *	@author JacORB IDL compiler 
 */

public final class InvalidPath
	extends org.omg.CORBA.UserException
{
	public InvalidPath()
	{
		super(org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.id());
	}

	public InvalidPath(String value)
	{
		super(value);
	}
}
