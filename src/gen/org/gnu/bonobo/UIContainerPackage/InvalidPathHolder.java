package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "InvalidPath"
 *	@author JacORB IDL compiler 
 */

public final class InvalidPathHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.UIContainerPackage.InvalidPath value;

	public InvalidPathHolder ()
	{
	}
	public InvalidPathHolder(final org.gnu.bonobo.UIContainerPackage.InvalidPath initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.write(_out, value);
	}
}
