package org.gnu.bonobo.UIContainerPackage;

/**
 *	Generated from IDL definition of exception "NonExistentAttr"
 *	@author JacORB IDL compiler 
 */

public final class NonExistentAttr
	extends org.omg.CORBA.UserException
{
	public NonExistentAttr()
	{
		super(org.gnu.bonobo.UIContainerPackage.NonExistentAttrHelper.id());
	}

	public NonExistentAttr(String value)
	{
		super(value);
	}
}
