package org.gnu.bonobo;


/**
 *	Generated from IDL definition of struct "StorageInfo"
 *	@author JacORB IDL compiler 
 */

public final class StorageInfoHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.StorageInfoHelper.id(),"StorageInfo",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("name", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("type", org.gnu.bonobo.StorageTypeHelper.type(), null),new org.omg.CORBA.StructMember("content_type", org.gnu.bonobo.ContentTypeHelper.type(), null),new org.omg.CORBA.StructMember("size", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.StorageInfo s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.StorageInfo extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/StorageInfo:1.0";
	}
	public static org.gnu.bonobo.StorageInfo read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.StorageInfo result = new org.gnu.bonobo.StorageInfo();
		result.name=in.read_string();
		result.type=org.gnu.bonobo.StorageTypeHelper.read(in);
		result.content_type=in.read_string();
		result.size=in.read_long();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.StorageInfo s)
	{
		out.write_string(s.name);
		org.gnu.bonobo.StorageTypeHelper.write(out,s.type);
		out.write_string(s.content_type);
		out.write_long(s.size);
	}
}
