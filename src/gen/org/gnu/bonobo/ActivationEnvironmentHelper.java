package org.gnu.bonobo;

/**
 *	Generated from IDL definition of alias "ActivationEnvironment"
 *	@author JacORB IDL compiler 
 */

public final class ActivationEnvironmentHelper
{
	private static org.omg.CORBA.TypeCode _type = null;

	public static void insert (org.omg.CORBA.Any any, org.gnu.bonobo.ActivationEnvValue[] s)
	{
		any.type (type ());
		write (any.create_output_stream (), s);
	}

	public static org.gnu.bonobo.ActivationEnvValue[] extract (final org.omg.CORBA.Any any)
	{
		return read (any.create_input_stream ());
	}

	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_alias_tc(org.gnu.bonobo.ActivationEnvironmentHelper.id(), "ActivationEnvironment",org.omg.CORBA.ORB.init().create_sequence_tc(0, org.gnu.bonobo.ActivationEnvValueHelper.type()));
		}
		return _type;
	}

	public static String id()
	{
		return "IDL:Bonobo/ActivationEnvironment:1.0";
	}
	public static org.gnu.bonobo.ActivationEnvValue[] read (final org.omg.CORBA.portable.InputStream _in)
	{
		org.gnu.bonobo.ActivationEnvValue[] _result;
		int _l_result1 = _in.read_long();
		_result = new org.gnu.bonobo.ActivationEnvValue[_l_result1];
		for (int i=0;i<_result.length;i++)
		{
			_result[i]=org.gnu.bonobo.ActivationEnvValueHelper.read(_in);
		}

		return _result;
	}

	public static void write (final org.omg.CORBA.portable.OutputStream _out, org.gnu.bonobo.ActivationEnvValue[] _s)
	{
		
		_out.write_long(_s.length);
		for (int i=0; i<_s.length;i++)
		{
			org.gnu.bonobo.ActivationEnvValueHelper.write(_out,_s[i]);
		}

	}
}
