package org.gnu.bonobo;


/**
 *	Generated from IDL interface "MonikerExtender"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class _MonikerExtenderStub
	extends org.omg.CORBA.portable.ObjectImpl
	implements org.gnu.bonobo.MonikerExtender
{
	private String[] ids = {"IDL:Bonobo/MonikerExtender:1.0","IDL:Bonobo/Unknown:1.0"};
	public String[] _ids()
	{
		return ids;
	}

	public final static java.lang.Class _opsClass = org.gnu.bonobo.MonikerExtenderOperations.class;
	public void ref()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "ref", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "ref", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			MonikerExtenderOperations _localServant = (MonikerExtenderOperations)_so.servant;
			try
			{
			_localServant.ref();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void unImplemented4()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unImplemented4", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unImplemented4", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			MonikerExtenderOperations _localServant = (MonikerExtenderOperations)_so.servant;
			try
			{
			_localServant.unImplemented4();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void unImplemented1()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unImplemented1", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unImplemented1", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			MonikerExtenderOperations _localServant = (MonikerExtenderOperations)_so.servant;
			try
			{
			_localServant.unImplemented1();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void unref()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unref", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unref", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			MonikerExtenderOperations _localServant = (MonikerExtenderOperations)_so.servant;
			try
			{
			_localServant.unref();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void unImplemented3()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unImplemented3", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unImplemented3", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			MonikerExtenderOperations _localServant = (MonikerExtenderOperations)_so.servant;
			try
			{
			_localServant.unImplemented3();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void unImplemented2()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unImplemented2", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unImplemented2", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			MonikerExtenderOperations _localServant = (MonikerExtenderOperations)_so.servant;
			try
			{
			_localServant.unImplemented2();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public org.gnu.bonobo.Unknown resolve(org.gnu.bonobo.Moniker m, org.gnu.bonobo.ResolveOptions options, java.lang.String name, java.lang.String requestedInterface) throws org.gnu.bonobo.MonikerPackage.TimeOut,org.gnu.bonobo.MonikerPackage.InterfaceNotFound,org.gnu.bonobo.GeneralError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "resolve", true);
				org.gnu.bonobo.MonikerHelper.write(_os,m);
				org.gnu.bonobo.ResolveOptionsHelper.write(_os,options);
				_os.write_string(name);
				_os.write_string(requestedInterface);
				_is = _invoke(_os);
				org.gnu.bonobo.Unknown _result = org.gnu.bonobo.UnknownHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Moniker/TimeOut:1.0"))
				{
					throw org.gnu.bonobo.MonikerPackage.TimeOutHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Moniker/InterfaceNotFound:1.0"))
				{
					throw org.gnu.bonobo.MonikerPackage.InterfaceNotFoundHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/GeneralError:1.0"))
				{
					throw org.gnu.bonobo.GeneralErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "resolve", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			MonikerExtenderOperations _localServant = (MonikerExtenderOperations)_so.servant;
			org.gnu.bonobo.Unknown _result;			try
			{
			_result = _localServant.resolve(m,options,name,requestedInterface);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "queryInterface", true);
				_os.write_string(repoid);
				_is = _invoke(_os);
				org.gnu.bonobo.Unknown _result = org.gnu.bonobo.UnknownHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "queryInterface", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			MonikerExtenderOperations _localServant = (MonikerExtenderOperations)_so.servant;
			org.gnu.bonobo.Unknown _result;			try
			{
			_result = _localServant.queryInterface(repoid);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

}
