package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "IOError"
 *	@author JacORB IDL compiler 
 */

public final class IOErrorHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.IOError value;

	public IOErrorHolder ()
	{
	}
	public IOErrorHolder(final org.gnu.bonobo.IOError initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.IOErrorHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.IOErrorHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.IOErrorHelper.write(_out, value);
	}
}
