package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Stream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class StreamPOATie
	extends StreamPOA
{
	private StreamOperations _delegate;

	private POA _poa;
	public StreamPOATie(StreamOperations delegate)
	{
		_delegate = delegate;
	}
	public StreamPOATie(StreamOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Stream _this()
	{
		return org.gnu.bonobo.StreamHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Stream _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.StreamHelper.narrow(_this_object(orb));
	}
	public StreamOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(StreamOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void read(int count, org.gnu.bonobo.StreamPackage.iobufHolder buffer) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.IOError
	{
_delegate.read(count,buffer);
	}

	public void ref()
	{
_delegate.ref();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void setInfo(org.gnu.bonobo.StorageInfo info, int mask) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
_delegate.setInfo(info,mask);
	}

	public void revert() throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
_delegate.revert();
	}

	public void commit() throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
_delegate.commit();
	}

	public org.gnu.bonobo.StorageInfo getInfo(int mask) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
		return _delegate.getInfo(mask);
	}

	public int seek(int offset, org.gnu.bonobo.StreamPackage.SeekType whence) throws org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
		return _delegate.seek(offset,whence);
	}

	public void write(byte[] buffer) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.IOError
	{
_delegate.write(buffer);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public void truncate(int length) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
_delegate.truncate(length);
	}

}
