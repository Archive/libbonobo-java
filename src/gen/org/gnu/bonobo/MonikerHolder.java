package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Moniker"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class MonikerHolder	implements org.omg.CORBA.portable.Streamable{
	 public Moniker value;
	public MonikerHolder()
	{
	}
	public MonikerHolder (final Moniker initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return MonikerHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = MonikerHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		MonikerHelper.write (_out,value);
	}
}
