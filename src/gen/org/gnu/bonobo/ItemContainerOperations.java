package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ItemContainer"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface ItemContainerOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	java.lang.String[] enumObjects();
	org.gnu.bonobo.Unknown getObjectByName(java.lang.String item_name, boolean only_if_exists) throws org.gnu.bonobo.ItemContainerPackage.NotFound,org.gnu.bonobo.ItemContainerPackage.SyntaxError;
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
