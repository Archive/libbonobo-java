package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Listener"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class ListenerPOATie
	extends ListenerPOA
{
	private ListenerOperations _delegate;

	private POA _poa;
	public ListenerPOATie(ListenerOperations delegate)
	{
		_delegate = delegate;
	}
	public ListenerPOATie(ListenerOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Listener _this()
	{
		return org.gnu.bonobo.ListenerHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Listener _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.ListenerHelper.narrow(_this_object(orb));
	}
	public ListenerOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(ListenerOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void ref()
	{
_delegate.ref();
	}

	public void event(java.lang.String event_name, org.omg.CORBA.Any args)
	{
_delegate.event(event_name,args);
	}

	public void unref()
	{
_delegate.unref();
	}

	public void unImplemented()
	{
_delegate.unImplemented();
	}

	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
