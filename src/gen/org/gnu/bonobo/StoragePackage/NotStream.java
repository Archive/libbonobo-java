package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotStream"
 *	@author JacORB IDL compiler 
 */

public final class NotStream
	extends org.omg.CORBA.UserException
{
	public NotStream()
	{
		super(org.gnu.bonobo.StoragePackage.NotStreamHelper.id());
	}

	public NotStream(String value)
	{
		super(value);
	}
}
