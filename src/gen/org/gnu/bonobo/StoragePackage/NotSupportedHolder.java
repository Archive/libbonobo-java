package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotSupported"
 *	@author JacORB IDL compiler 
 */

public final class NotSupportedHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StoragePackage.NotSupported value;

	public NotSupportedHolder ()
	{
	}
	public NotSupportedHolder(final org.gnu.bonobo.StoragePackage.NotSupported initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StoragePackage.NotSupportedHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StoragePackage.NotSupportedHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StoragePackage.NotSupportedHelper.write(_out, value);
	}
}
