package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotStream"
 *	@author JacORB IDL compiler 
 */

public final class NotStreamHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StoragePackage.NotStream value;

	public NotStreamHolder ()
	{
	}
	public NotStreamHolder(final org.gnu.bonobo.StoragePackage.NotStream initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StoragePackage.NotStreamHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StoragePackage.NotStreamHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StoragePackage.NotStreamHelper.write(_out, value);
	}
}
