package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotFound"
 *	@author JacORB IDL compiler 
 */

public final class NotFound
	extends org.omg.CORBA.UserException
{
	public NotFound()
	{
		super(org.gnu.bonobo.StoragePackage.NotFoundHelper.id());
	}

	public NotFound(String value)
	{
		super(value);
	}
}
