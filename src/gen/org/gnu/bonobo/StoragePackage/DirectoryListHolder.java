package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of alias "DirectoryList"
 *	@author JacORB IDL compiler 
 */

public final class DirectoryListHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StorageInfo[] value;

	public DirectoryListHolder ()
	{
	}
	public DirectoryListHolder (final org.gnu.bonobo.StorageInfo[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return DirectoryListHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = DirectoryListHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		DirectoryListHelper.write (out,value);
	}
}
