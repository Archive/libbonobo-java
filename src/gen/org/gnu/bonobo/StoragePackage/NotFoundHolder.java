package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotFound"
 *	@author JacORB IDL compiler 
 */

public final class NotFoundHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StoragePackage.NotFound value;

	public NotFoundHolder ()
	{
	}
	public NotFoundHolder(final org.gnu.bonobo.StoragePackage.NotFound initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StoragePackage.NotFoundHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StoragePackage.NotFoundHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StoragePackage.NotFoundHelper.write(_out, value);
	}
}
