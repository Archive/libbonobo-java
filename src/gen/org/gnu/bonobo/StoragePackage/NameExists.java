package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NameExists"
 *	@author JacORB IDL compiler 
 */

public final class NameExists
	extends org.omg.CORBA.UserException
{
	public NameExists()
	{
		super(org.gnu.bonobo.StoragePackage.NameExistsHelper.id());
	}

	public NameExists(String value)
	{
		super(value);
	}
}
