package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotEmpty"
 *	@author JacORB IDL compiler 
 */

public final class NotEmptyHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StoragePackage.NotEmpty value;

	public NotEmptyHolder ()
	{
	}
	public NotEmptyHolder(final org.gnu.bonobo.StoragePackage.NotEmpty initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StoragePackage.NotEmptyHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StoragePackage.NotEmptyHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StoragePackage.NotEmptyHelper.write(_out, value);
	}
}
