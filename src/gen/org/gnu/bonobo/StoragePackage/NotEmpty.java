package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotEmpty"
 *	@author JacORB IDL compiler 
 */

public final class NotEmpty
	extends org.omg.CORBA.UserException
{
	public NotEmpty()
	{
		super(org.gnu.bonobo.StoragePackage.NotEmptyHelper.id());
	}

	public NotEmpty(String value)
	{
		super(value);
	}
}
