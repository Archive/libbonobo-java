package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NameExists"
 *	@author JacORB IDL compiler 
 */

public final class NameExistsHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StoragePackage.NameExists value;

	public NameExistsHolder ()
	{
	}
	public NameExistsHolder(final org.gnu.bonobo.StoragePackage.NameExists initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StoragePackage.NameExistsHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StoragePackage.NameExistsHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StoragePackage.NameExistsHelper.write(_out, value);
	}
}
