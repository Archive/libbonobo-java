package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotStorage"
 *	@author JacORB IDL compiler 
 */

public final class NotStorage
	extends org.omg.CORBA.UserException
{
	public NotStorage()
	{
		super(org.gnu.bonobo.StoragePackage.NotStorageHelper.id());
	}

	public NotStorage(String value)
	{
		super(value);
	}
}
