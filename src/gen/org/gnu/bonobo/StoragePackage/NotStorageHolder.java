package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NotStorage"
 *	@author JacORB IDL compiler 
 */

public final class NotStorageHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StoragePackage.NotStorage value;

	public NotStorageHolder ()
	{
	}
	public NotStorageHolder(final org.gnu.bonobo.StoragePackage.NotStorage initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StoragePackage.NotStorageHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StoragePackage.NotStorageHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StoragePackage.NotStorageHelper.write(_out, value);
	}
}
