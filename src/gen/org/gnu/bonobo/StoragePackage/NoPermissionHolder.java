package org.gnu.bonobo.StoragePackage;

/**
 *	Generated from IDL definition of exception "NoPermission"
 *	@author JacORB IDL compiler 
 */

public final class NoPermissionHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StoragePackage.NoPermission value;

	public NoPermissionHolder ()
	{
	}
	public NoPermissionHolder(final org.gnu.bonobo.StoragePackage.NoPermission initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StoragePackage.NoPermissionHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, value);
	}
}
