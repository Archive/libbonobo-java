package org.gnu.bonobo;


/**
 *	Generated from IDL definition of struct "ActivationProperty"
 *	@author JacORB IDL compiler 
 */

public final class ActivationPropertyHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.ActivationPropertyHelper.id(),"ActivationProperty",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("name", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("v", org.gnu.bonobo.ActivationPropertyValueHelper.type(), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.ActivationProperty s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.ActivationProperty extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/ActivationProperty:1.0";
	}
	public static org.gnu.bonobo.ActivationProperty read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.ActivationProperty result = new org.gnu.bonobo.ActivationProperty();
		result.name=in.read_string();
		result.v=org.gnu.bonobo.ActivationPropertyValueHelper.read(in);
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.ActivationProperty s)
	{
		out.write_string(s.name);
		org.gnu.bonobo.ActivationPropertyValueHelper.write(out,s.v);
	}
}
