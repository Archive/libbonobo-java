package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Clipboard"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface ClipboardOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	void setClipboard(org.gnu.bonobo.Moniker pasting_moniker, org.gnu.bonobo.Moniker linking_moniker);
	org.gnu.bonobo.Moniker paste();
	org.gnu.bonobo.Moniker link();
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
