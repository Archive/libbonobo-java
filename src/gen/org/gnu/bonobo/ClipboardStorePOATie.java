package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "ClipboardStore"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class ClipboardStorePOATie
	extends ClipboardStorePOA
{
	private ClipboardStoreOperations _delegate;

	private POA _poa;
	public ClipboardStorePOATie(ClipboardStoreOperations delegate)
	{
		_delegate = delegate;
	}
	public ClipboardStorePOATie(ClipboardStoreOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.ClipboardStore _this()
	{
		return org.gnu.bonobo.ClipboardStoreHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.ClipboardStore _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.ClipboardStoreHelper.narrow(_this_object(orb));
	}
	public ClipboardStoreOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(ClipboardStoreOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void ref()
	{
_delegate.ref();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void fetchStorage(org.gnu.bonobo.PersistStorage source, org.gnu.bonobo.Moniker linking_moniker)
	{
_delegate.fetchStorage(source,linking_moniker);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public void fetchStream(org.gnu.bonobo.PersistStream source, org.gnu.bonobo.Moniker linking_moniker)
	{
_delegate.fetchStream(source,linking_moniker);
	}

}
