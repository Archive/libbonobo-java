package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "StorageType"
 *	@author JacORB IDL compiler 
 */

public final class StorageTypeHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.StorageTypeHelper.id(),"StorageType",new String[]{"STORAGE_TYPE_REGULAR","STORAGE_TYPE_DIRECTORY"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.StorageType s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.StorageType extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/StorageType:1.0";
	}
	public static StorageType read (final org.omg.CORBA.portable.InputStream in)
	{
		return StorageType.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final StorageType s)
	{
		out.write_long(s.value());
	}
}
