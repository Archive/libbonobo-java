package org.gnu.bonobo.UIComponentPackage;
/**
 *	Generated from IDL definition of enum "EventType"
 *	@author JacORB IDL compiler 
 */

public final class EventType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _STATE_CHANGED = 0;
	public static final EventType STATE_CHANGED = new EventType(_STATE_CHANGED);
	public static final int _OTHER = 1;
	public static final EventType OTHER = new EventType(_OTHER);
	public int value()
	{
		return value;
	}
	public static EventType from_int(int value)
	{
		switch (value) {
			case _STATE_CHANGED: return STATE_CHANGED;
			case _OTHER: return OTHER;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected EventType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
