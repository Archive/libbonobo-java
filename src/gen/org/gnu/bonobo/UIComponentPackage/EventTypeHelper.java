package org.gnu.bonobo.UIComponentPackage;
/**
 *	Generated from IDL definition of enum "EventType"
 *	@author JacORB IDL compiler 
 */

public final class EventTypeHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.UIComponentPackage.EventTypeHelper.id(),"EventType",new String[]{"STATE_CHANGED","OTHER"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.UIComponentPackage.EventType s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.UIComponentPackage.EventType extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/UIComponent/EventType:1.0";
	}
	public static EventType read (final org.omg.CORBA.portable.InputStream in)
	{
		return EventType.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final EventType s)
	{
		out.write_long(s.value());
	}
}
