package org.gnu.bonobo.UIComponentPackage;
/**
 *	Generated from IDL definition of enum "EventType"
 *	@author JacORB IDL compiler 
 */

public final class EventTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public EventType value;

	public EventTypeHolder ()
	{
	}
	public EventTypeHolder (final EventType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return EventTypeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = EventTypeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		EventTypeHelper.write (out,value);
	}
}
