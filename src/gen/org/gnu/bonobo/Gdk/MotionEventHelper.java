package org.gnu.bonobo.Gdk;


/**
 *	Generated from IDL definition of struct "MotionEvent"
 *	@author JacORB IDL compiler 
 */

public final class MotionEventHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Gdk.MotionEventHelper.id(),"MotionEvent",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("time", org.gnu.bonobo.Gdk.TimeHelper.type(), null),new org.omg.CORBA.StructMember("x", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("y", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("x_root", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("y_root", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("pressure", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("xtilt", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("ytilt", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("state", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("is_hint", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(8)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gdk.MotionEvent s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gdk.MotionEvent extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gdk/MotionEvent:1.0";
	}
	public static org.gnu.bonobo.Gdk.MotionEvent read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Gdk.MotionEvent result = new org.gnu.bonobo.Gdk.MotionEvent();
		result.time=in.read_long();
		result.x=in.read_double();
		result.y=in.read_double();
		result.x_root=in.read_double();
		result.y_root=in.read_double();
		result.pressure=in.read_double();
		result.xtilt=in.read_double();
		result.ytilt=in.read_double();
		result.state=in.read_long();
		result.is_hint=in.read_boolean();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Gdk.MotionEvent s)
	{
		out.write_long(s.time);
		out.write_double(s.x);
		out.write_double(s.y);
		out.write_double(s.x_root);
		out.write_double(s.y_root);
		out.write_double(s.pressure);
		out.write_double(s.xtilt);
		out.write_double(s.ytilt);
		out.write_long(s.state);
		out.write_boolean(s.is_hint);
	}
}
