package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "ButtonEvent"
 *	@author JacORB IDL compiler 
 */

public final class ButtonEventHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Gdk.ButtonEvent value;

	public ButtonEventHolder ()
	{
	}
	public ButtonEventHolder(final org.gnu.bonobo.Gdk.ButtonEvent initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Gdk.ButtonEventHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Gdk.ButtonEventHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Gdk.ButtonEventHelper.write(_out, value);
	}
}
