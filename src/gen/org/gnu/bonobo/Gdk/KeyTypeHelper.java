package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "KeyType"
 *	@author JacORB IDL compiler 
 */

public final class KeyTypeHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.Gdk.KeyTypeHelper.id(),"KeyType",new String[]{"KEY_PRESS","KEY_RELEASE"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gdk.KeyType s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gdk.KeyType extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gdk/KeyType:1.0";
	}
	public static KeyType read (final org.omg.CORBA.portable.InputStream in)
	{
		return KeyType.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final KeyType s)
	{
		out.write_long(s.value());
	}
}
