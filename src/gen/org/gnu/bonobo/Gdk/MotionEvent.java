package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "MotionEvent"
 *	@author JacORB IDL compiler 
 */

public final class MotionEvent
	implements org.omg.CORBA.portable.IDLEntity
{
	public MotionEvent(){}
	public int time;
	public double x;
	public double y;
	public double x_root;
	public double y_root;
	public double pressure;
	public double xtilt;
	public double ytilt;
	public int state;
	public boolean is_hint;
	public MotionEvent(int time, double x, double y, double x_root, double y_root, double pressure, double xtilt, double ytilt, int state, boolean is_hint)
	{
		this.time = time;
		this.x = x;
		this.y = y;
		this.x_root = x_root;
		this.y_root = y_root;
		this.pressure = pressure;
		this.xtilt = xtilt;
		this.ytilt = ytilt;
		this.state = state;
		this.is_hint = is_hint;
	}
}
