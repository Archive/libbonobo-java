package org.gnu.bonobo.Gdk;


/**
 *	Generated from IDL definition of struct "KeyEvent"
 *	@author JacORB IDL compiler 
 */

public final class KeyEventHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Gdk.KeyEventHelper.id(),"KeyEvent",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("type", org.gnu.bonobo.Gdk.KeyTypeHelper.type(), null),new org.omg.CORBA.StructMember("time", org.gnu.bonobo.Gdk.TimeHelper.type(), null),new org.omg.CORBA.StructMember("state", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null),new org.omg.CORBA.StructMember("keyval", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null),new org.omg.CORBA.StructMember("length", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null),new org.omg.CORBA.StructMember("str", org.omg.CORBA.ORB.init().create_string_tc(0), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gdk.KeyEvent s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gdk.KeyEvent extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gdk/KeyEvent:1.0";
	}
	public static org.gnu.bonobo.Gdk.KeyEvent read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Gdk.KeyEvent result = new org.gnu.bonobo.Gdk.KeyEvent();
		result.type=org.gnu.bonobo.Gdk.KeyTypeHelper.read(in);
		result.time=in.read_long();
		result.state=in.read_short();
		result.keyval=in.read_short();
		result.length=in.read_short();
		result.str=in.read_string();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Gdk.KeyEvent s)
	{
		org.gnu.bonobo.Gdk.KeyTypeHelper.write(out,s.type);
		out.write_long(s.time);
		out.write_short(s.state);
		out.write_short(s.keyval);
		out.write_short(s.length);
		out.write_string(s.str);
	}
}
