package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "ButtonType"
 *	@author JacORB IDL compiler 
 */

public final class ButtonTypeHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.Gdk.ButtonTypeHelper.id(),"ButtonType",new String[]{"BUTTON_PRESS","BUTTON_2_PRESS","BUTTON_3_PRESS","BUTTON_RELEASE"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gdk.ButtonType s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gdk.ButtonType extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gdk/ButtonType:1.0";
	}
	public static ButtonType read (final org.omg.CORBA.portable.InputStream in)
	{
		return ButtonType.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final ButtonType s)
	{
		out.write_long(s.value());
	}
}
