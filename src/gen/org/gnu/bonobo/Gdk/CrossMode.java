package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "CrossMode"
 *	@author JacORB IDL compiler 
 */

public final class CrossMode
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _NORMAL = 0;
	public static final CrossMode NORMAL = new CrossMode(_NORMAL);
	public static final int _GRAB = 1;
	public static final CrossMode GRAB = new CrossMode(_GRAB);
	public static final int _UNGRAB = 2;
	public static final CrossMode UNGRAB = new CrossMode(_UNGRAB);
	public int value()
	{
		return value;
	}
	public static CrossMode from_int(int value)
	{
		switch (value) {
			case _NORMAL: return NORMAL;
			case _GRAB: return GRAB;
			case _UNGRAB: return UNGRAB;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected CrossMode(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
