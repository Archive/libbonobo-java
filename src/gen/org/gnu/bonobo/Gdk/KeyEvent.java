package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "KeyEvent"
 *	@author JacORB IDL compiler 
 */

public final class KeyEvent
	implements org.omg.CORBA.portable.IDLEntity
{
	public KeyEvent(){}
	public org.gnu.bonobo.Gdk.KeyType type;
	public int time;
	public short state;
	public short keyval;
	public short length;
	public java.lang.String str = "";
	public KeyEvent(org.gnu.bonobo.Gdk.KeyType type, int time, short state, short keyval, short length, java.lang.String str)
	{
		this.type = type;
		this.time = time;
		this.state = state;
		this.keyval = keyval;
		this.length = length;
		this.str = str;
	}
}
