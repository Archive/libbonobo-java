package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "CrossingEvent"
 *	@author JacORB IDL compiler 
 */

public final class CrossingEvent
	implements org.omg.CORBA.portable.IDLEntity
{
	public CrossingEvent(){}
	public org.gnu.bonobo.Gdk.CrossType type;
	public int time;
	public double x;
	public double y;
	public double x_root;
	public double y_root;
	public org.gnu.bonobo.Gdk.CrossMode mode;
	public boolean focus;
	public short state;
	public CrossingEvent(org.gnu.bonobo.Gdk.CrossType type, int time, double x, double y, double x_root, double y_root, org.gnu.bonobo.Gdk.CrossMode mode, boolean focus, short state)
	{
		this.type = type;
		this.time = time;
		this.x = x;
		this.y = y;
		this.x_root = x_root;
		this.y_root = y_root;
		this.mode = mode;
		this.focus = focus;
		this.state = state;
	}
}
