package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of union "Event"
 *	@author JacORB IDL compiler 
 */

public final class EventHelper
{
	private static org.omg.CORBA.TypeCode _type;
	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gdk.Event s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gdk.Event extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gdk/Event:1.0";
	}
	public static Event read (org.omg.CORBA.portable.InputStream in)
	{
		Event result = new Event ();
		org.gnu.bonobo.Gdk.EventType disc = org.gnu.bonobo.Gdk.EventType.from_int(in.read_long());
		switch (disc.value ())
		{
			case org.gnu.bonobo.Gdk.EventType._FOCUS:
			{
				org.gnu.bonobo.Gdk.FocusEvent _var;
				_var=org.gnu.bonobo.Gdk.FocusEventHelper.read(in);
				result.focus (_var);
				break;
			}
			case org.gnu.bonobo.Gdk.EventType._KEY:
			{
				org.gnu.bonobo.Gdk.KeyEvent _var;
				_var=org.gnu.bonobo.Gdk.KeyEventHelper.read(in);
				result.key (_var);
				break;
			}
			case org.gnu.bonobo.Gdk.EventType._MOTION:
			{
				org.gnu.bonobo.Gdk.MotionEvent _var;
				_var=org.gnu.bonobo.Gdk.MotionEventHelper.read(in);
				result.motion (_var);
				break;
			}
			case org.gnu.bonobo.Gdk.EventType._BUTTON:
			{
				org.gnu.bonobo.Gdk.ButtonEvent _var;
				_var=org.gnu.bonobo.Gdk.ButtonEventHelper.read(in);
				result.button (_var);
				break;
			}
			case org.gnu.bonobo.Gdk.EventType._CROSSING:
			{
				org.gnu.bonobo.Gdk.CrossingEvent _var;
				_var=org.gnu.bonobo.Gdk.CrossingEventHelper.read(in);
				result.crossing (_var);
				break;
			}
		}
		return result;
	}
	public static void write (org.omg.CORBA.portable.OutputStream out, Event s)
	{
		out.write_long (s.discriminator().value ());
		switch (s.discriminator().value ())
		{
			case org.gnu.bonobo.Gdk.EventType._FOCUS:
			{
				org.gnu.bonobo.Gdk.FocusEventHelper.write(out,s.focus ());
				break;
			}
			case org.gnu.bonobo.Gdk.EventType._KEY:
			{
				org.gnu.bonobo.Gdk.KeyEventHelper.write(out,s.key ());
				break;
			}
			case org.gnu.bonobo.Gdk.EventType._MOTION:
			{
				org.gnu.bonobo.Gdk.MotionEventHelper.write(out,s.motion ());
				break;
			}
			case org.gnu.bonobo.Gdk.EventType._BUTTON:
			{
				org.gnu.bonobo.Gdk.ButtonEventHelper.write(out,s.button ());
				break;
			}
			case org.gnu.bonobo.Gdk.EventType._CROSSING:
			{
				org.gnu.bonobo.Gdk.CrossingEventHelper.write(out,s.crossing ());
				break;
			}
		}
	}
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			org.omg.CORBA.UnionMember[] members = new org.omg.CORBA.UnionMember[5];
			org.omg.CORBA.Any label_any;
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.Gdk.EventTypeHelper.insert(label_any, org.gnu.bonobo.Gdk.EventType.FOCUS);
			members[4] = new org.omg.CORBA.UnionMember ("focus", label_any, org.gnu.bonobo.Gdk.FocusEventHelper.type(),null);
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.Gdk.EventTypeHelper.insert(label_any, org.gnu.bonobo.Gdk.EventType.KEY);
			members[3] = new org.omg.CORBA.UnionMember ("key", label_any, org.gnu.bonobo.Gdk.KeyEventHelper.type(),null);
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.Gdk.EventTypeHelper.insert(label_any, org.gnu.bonobo.Gdk.EventType.MOTION);
			members[2] = new org.omg.CORBA.UnionMember ("motion", label_any, org.gnu.bonobo.Gdk.MotionEventHelper.type(),null);
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.Gdk.EventTypeHelper.insert(label_any, org.gnu.bonobo.Gdk.EventType.BUTTON);
			members[1] = new org.omg.CORBA.UnionMember ("button", label_any, org.gnu.bonobo.Gdk.ButtonEventHelper.type(),null);
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.Gdk.EventTypeHelper.insert(label_any, org.gnu.bonobo.Gdk.EventType.CROSSING);
			members[0] = new org.omg.CORBA.UnionMember ("crossing", label_any, org.gnu.bonobo.Gdk.CrossingEventHelper.type(),null);
			 _type = org.omg.CORBA.ORB.init().create_union_tc(id(),"Event",org.gnu.bonobo.Gdk.EventTypeHelper.type(), members);
		}
		return _type;
	}
}
