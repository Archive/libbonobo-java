package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "EventType"
 *	@author JacORB IDL compiler 
 */

public final class EventType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _FOCUS = 0;
	public static final EventType FOCUS = new EventType(_FOCUS);
	public static final int _KEY = 1;
	public static final EventType KEY = new EventType(_KEY);
	public static final int _MOTION = 2;
	public static final EventType MOTION = new EventType(_MOTION);
	public static final int _BUTTON = 3;
	public static final EventType BUTTON = new EventType(_BUTTON);
	public static final int _CROSSING = 4;
	public static final EventType CROSSING = new EventType(_CROSSING);
	public int value()
	{
		return value;
	}
	public static EventType from_int(int value)
	{
		switch (value) {
			case _FOCUS: return FOCUS;
			case _KEY: return KEY;
			case _MOTION: return MOTION;
			case _BUTTON: return BUTTON;
			case _CROSSING: return CROSSING;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected EventType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
