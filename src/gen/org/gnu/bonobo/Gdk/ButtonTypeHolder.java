package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "ButtonType"
 *	@author JacORB IDL compiler 
 */

public final class ButtonTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public ButtonType value;

	public ButtonTypeHolder ()
	{
	}
	public ButtonTypeHolder (final ButtonType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ButtonTypeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ButtonTypeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ButtonTypeHelper.write (out,value);
	}
}
