package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "MotionEvent"
 *	@author JacORB IDL compiler 
 */

public final class MotionEventHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Gdk.MotionEvent value;

	public MotionEventHolder ()
	{
	}
	public MotionEventHolder(final org.gnu.bonobo.Gdk.MotionEvent initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Gdk.MotionEventHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Gdk.MotionEventHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Gdk.MotionEventHelper.write(_out, value);
	}
}
