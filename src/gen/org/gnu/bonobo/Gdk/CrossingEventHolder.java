package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "CrossingEvent"
 *	@author JacORB IDL compiler 
 */

public final class CrossingEventHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Gdk.CrossingEvent value;

	public CrossingEventHolder ()
	{
	}
	public CrossingEventHolder(final org.gnu.bonobo.Gdk.CrossingEvent initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Gdk.CrossingEventHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Gdk.CrossingEventHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Gdk.CrossingEventHelper.write(_out, value);
	}
}
