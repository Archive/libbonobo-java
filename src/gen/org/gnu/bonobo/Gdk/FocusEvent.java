package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "FocusEvent"
 *	@author JacORB IDL compiler 
 */

public final class FocusEvent
	implements org.omg.CORBA.portable.IDLEntity
{
	public FocusEvent(){}
	public boolean inside;
	public FocusEvent(boolean inside)
	{
		this.inside = inside;
	}
}
