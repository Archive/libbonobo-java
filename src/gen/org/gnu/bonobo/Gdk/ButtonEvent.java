package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "ButtonEvent"
 *	@author JacORB IDL compiler 
 */

public final class ButtonEvent
	implements org.omg.CORBA.portable.IDLEntity
{
	public ButtonEvent(){}
	public org.gnu.bonobo.Gdk.ButtonType type;
	public int time;
	public double x;
	public double y;
	public double x_root;
	public double y_root;
	public short button;
	public ButtonEvent(org.gnu.bonobo.Gdk.ButtonType type, int time, double x, double y, double x_root, double y_root, short button)
	{
		this.type = type;
		this.time = time;
		this.x = x;
		this.y = y;
		this.x_root = x_root;
		this.y_root = y_root;
		this.button = button;
	}
}
