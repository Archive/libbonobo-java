package org.gnu.bonobo.Gdk;


/**
 *	Generated from IDL definition of struct "FocusEvent"
 *	@author JacORB IDL compiler 
 */

public final class FocusEventHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Gdk.FocusEventHelper.id(),"FocusEvent",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("inside", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(8)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gdk.FocusEvent s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gdk.FocusEvent extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gdk/FocusEvent:1.0";
	}
	public static org.gnu.bonobo.Gdk.FocusEvent read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Gdk.FocusEvent result = new org.gnu.bonobo.Gdk.FocusEvent();
		result.inside=in.read_boolean();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Gdk.FocusEvent s)
	{
		out.write_boolean(s.inside);
	}
}
