package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "KeyType"
 *	@author JacORB IDL compiler 
 */

public final class KeyTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public KeyType value;

	public KeyTypeHolder ()
	{
	}
	public KeyTypeHolder (final KeyType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return KeyTypeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = KeyTypeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		KeyTypeHelper.write (out,value);
	}
}
