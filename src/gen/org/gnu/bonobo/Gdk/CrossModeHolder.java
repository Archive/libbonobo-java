package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "CrossMode"
 *	@author JacORB IDL compiler 
 */

public final class CrossModeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public CrossMode value;

	public CrossModeHolder ()
	{
	}
	public CrossModeHolder (final CrossMode initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return CrossModeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = CrossModeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		CrossModeHelper.write (out,value);
	}
}
