package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "CrossType"
 *	@author JacORB IDL compiler 
 */

public final class CrossType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _ENTER = 0;
	public static final CrossType ENTER = new CrossType(_ENTER);
	public static final int _LEAVE = 1;
	public static final CrossType LEAVE = new CrossType(_LEAVE);
	public int value()
	{
		return value;
	}
	public static CrossType from_int(int value)
	{
		switch (value) {
			case _ENTER: return ENTER;
			case _LEAVE: return LEAVE;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected CrossType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
