package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "FocusEvent"
 *	@author JacORB IDL compiler 
 */

public final class FocusEventHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Gdk.FocusEvent value;

	public FocusEventHolder ()
	{
	}
	public FocusEventHolder(final org.gnu.bonobo.Gdk.FocusEvent initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Gdk.FocusEventHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Gdk.FocusEventHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Gdk.FocusEventHelper.write(_out, value);
	}
}
