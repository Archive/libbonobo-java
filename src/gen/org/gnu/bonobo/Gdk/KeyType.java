package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "KeyType"
 *	@author JacORB IDL compiler 
 */

public final class KeyType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _KEY_PRESS = 0;
	public static final KeyType KEY_PRESS = new KeyType(_KEY_PRESS);
	public static final int _KEY_RELEASE = 1;
	public static final KeyType KEY_RELEASE = new KeyType(_KEY_RELEASE);
	public int value()
	{
		return value;
	}
	public static KeyType from_int(int value)
	{
		switch (value) {
			case _KEY_PRESS: return KEY_PRESS;
			case _KEY_RELEASE: return KEY_RELEASE;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected KeyType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
