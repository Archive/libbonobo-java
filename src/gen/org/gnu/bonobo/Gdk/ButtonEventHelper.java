package org.gnu.bonobo.Gdk;


/**
 *	Generated from IDL definition of struct "ButtonEvent"
 *	@author JacORB IDL compiler 
 */

public final class ButtonEventHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Gdk.ButtonEventHelper.id(),"ButtonEvent",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("type", org.gnu.bonobo.Gdk.ButtonTypeHelper.type(), null),new org.omg.CORBA.StructMember("time", org.gnu.bonobo.Gdk.TimeHelper.type(), null),new org.omg.CORBA.StructMember("x", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("y", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("x_root", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("y_root", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("button", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gdk.ButtonEvent s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gdk.ButtonEvent extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gdk/ButtonEvent:1.0";
	}
	public static org.gnu.bonobo.Gdk.ButtonEvent read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Gdk.ButtonEvent result = new org.gnu.bonobo.Gdk.ButtonEvent();
		result.type=org.gnu.bonobo.Gdk.ButtonTypeHelper.read(in);
		result.time=in.read_long();
		result.x=in.read_double();
		result.y=in.read_double();
		result.x_root=in.read_double();
		result.y_root=in.read_double();
		result.button=in.read_short();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Gdk.ButtonEvent s)
	{
		org.gnu.bonobo.Gdk.ButtonTypeHelper.write(out,s.type);
		out.write_long(s.time);
		out.write_double(s.x);
		out.write_double(s.y);
		out.write_double(s.x_root);
		out.write_double(s.y_root);
		out.write_short(s.button);
	}
}
