package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of union "Event"
 *	@author JacORB IDL compiler 
 */

public final class EventHolder
	implements org.omg.CORBA.portable.Streamable
{
	public Event value;

	public EventHolder ()
	{
	}
	public EventHolder (final Event initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return EventHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = EventHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		EventHelper.write (out, value);
	}
}
