package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of struct "KeyEvent"
 *	@author JacORB IDL compiler 
 */

public final class KeyEventHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Gdk.KeyEvent value;

	public KeyEventHolder ()
	{
	}
	public KeyEventHolder(final org.gnu.bonobo.Gdk.KeyEvent initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Gdk.KeyEventHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Gdk.KeyEventHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Gdk.KeyEventHelper.write(_out, value);
	}
}
