package org.gnu.bonobo.Gdk;

/**
 *	Generated from IDL definition of union "Event"
 *	@author JacORB IDL compiler 
 */

public final class Event
	implements org.omg.CORBA.portable.IDLEntity
{
	private org.gnu.bonobo.Gdk.EventType discriminator;
	private org.gnu.bonobo.Gdk.FocusEvent focus;
	private org.gnu.bonobo.Gdk.KeyEvent key;
	private org.gnu.bonobo.Gdk.MotionEvent motion;
	private org.gnu.bonobo.Gdk.ButtonEvent button;
	private org.gnu.bonobo.Gdk.CrossingEvent crossing;

	public Event ()
	{
	}

	public org.gnu.bonobo.Gdk.EventType discriminator ()
	{
		return discriminator;
	}

	public org.gnu.bonobo.Gdk.FocusEvent focus ()
	{
		if (discriminator != org.gnu.bonobo.Gdk.EventType.FOCUS)
			throw new org.omg.CORBA.BAD_OPERATION();
		return focus;
	}

	public void focus (org.gnu.bonobo.Gdk.FocusEvent _x)
	{
		discriminator = org.gnu.bonobo.Gdk.EventType.FOCUS;
		focus = _x;
	}

	public org.gnu.bonobo.Gdk.KeyEvent key ()
	{
		if (discriminator != org.gnu.bonobo.Gdk.EventType.KEY)
			throw new org.omg.CORBA.BAD_OPERATION();
		return key;
	}

	public void key (org.gnu.bonobo.Gdk.KeyEvent _x)
	{
		discriminator = org.gnu.bonobo.Gdk.EventType.KEY;
		key = _x;
	}

	public org.gnu.bonobo.Gdk.MotionEvent motion ()
	{
		if (discriminator != org.gnu.bonobo.Gdk.EventType.MOTION)
			throw new org.omg.CORBA.BAD_OPERATION();
		return motion;
	}

	public void motion (org.gnu.bonobo.Gdk.MotionEvent _x)
	{
		discriminator = org.gnu.bonobo.Gdk.EventType.MOTION;
		motion = _x;
	}

	public org.gnu.bonobo.Gdk.ButtonEvent button ()
	{
		if (discriminator != org.gnu.bonobo.Gdk.EventType.BUTTON)
			throw new org.omg.CORBA.BAD_OPERATION();
		return button;
	}

	public void button (org.gnu.bonobo.Gdk.ButtonEvent _x)
	{
		discriminator = org.gnu.bonobo.Gdk.EventType.BUTTON;
		button = _x;
	}

	public org.gnu.bonobo.Gdk.CrossingEvent crossing ()
	{
		if (discriminator != org.gnu.bonobo.Gdk.EventType.CROSSING)
			throw new org.omg.CORBA.BAD_OPERATION();
		return crossing;
	}

	public void crossing (org.gnu.bonobo.Gdk.CrossingEvent _x)
	{
		discriminator = org.gnu.bonobo.Gdk.EventType.CROSSING;
		crossing = _x;
	}

}
