package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "ButtonType"
 *	@author JacORB IDL compiler 
 */

public final class ButtonType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _BUTTON_PRESS = 0;
	public static final ButtonType BUTTON_PRESS = new ButtonType(_BUTTON_PRESS);
	public static final int _BUTTON_2_PRESS = 1;
	public static final ButtonType BUTTON_2_PRESS = new ButtonType(_BUTTON_2_PRESS);
	public static final int _BUTTON_3_PRESS = 2;
	public static final ButtonType BUTTON_3_PRESS = new ButtonType(_BUTTON_3_PRESS);
	public static final int _BUTTON_RELEASE = 3;
	public static final ButtonType BUTTON_RELEASE = new ButtonType(_BUTTON_RELEASE);
	public int value()
	{
		return value;
	}
	public static ButtonType from_int(int value)
	{
		switch (value) {
			case _BUTTON_PRESS: return BUTTON_PRESS;
			case _BUTTON_2_PRESS: return BUTTON_2_PRESS;
			case _BUTTON_3_PRESS: return BUTTON_3_PRESS;
			case _BUTTON_RELEASE: return BUTTON_RELEASE;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected ButtonType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
