package org.gnu.bonobo.Gdk;
/**
 *	Generated from IDL definition of enum "CrossType"
 *	@author JacORB IDL compiler 
 */

public final class CrossTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public CrossType value;

	public CrossTypeHolder ()
	{
	}
	public CrossTypeHolder (final CrossType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return CrossTypeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = CrossTypeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		CrossTypeHelper.write (out,value);
	}
}
