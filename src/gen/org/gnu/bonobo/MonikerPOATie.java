package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Moniker"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class MonikerPOATie
	extends MonikerPOA
{
	private MonikerOperations _delegate;

	private POA _poa;
	public MonikerPOATie(MonikerOperations delegate)
	{
		_delegate = delegate;
	}
	public MonikerPOATie(MonikerOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Moniker _this()
	{
		return org.gnu.bonobo.MonikerHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Moniker _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.MonikerHelper.narrow(_this_object(orb));
	}
	public MonikerOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(MonikerOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void ref()
	{
_delegate.ref();
	}

	public int equal(java.lang.String name)
	{
		return _delegate.equal(name);
	}

	public org.gnu.bonobo.Unknown resolve(org.gnu.bonobo.ResolveOptions options, java.lang.String requestedInterface) throws org.gnu.bonobo.MonikerPackage.TimeOut,org.gnu.bonobo.MonikerPackage.InterfaceNotFound,org.gnu.bonobo.GeneralError
	{
		return _delegate.resolve(options,requestedInterface);
	}

	public void unref()
	{
_delegate.unref();
	}

	public void setParent(org.gnu.bonobo.Moniker parent)
	{
_delegate.setParent(parent);
	}

	public void setName(java.lang.String name) throws org.gnu.bonobo.MonikerPackage.InvalidSyntax,org.gnu.bonobo.MonikerPackage.UnknownPrefix
	{
_delegate.setName(name);
	}

	public java.lang.String getName() throws org.gnu.bonobo.MonikerPackage.InvalidSyntax
	{
		return _delegate.getName();
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public org.gnu.bonobo.Moniker getParent()
	{
		return _delegate.getParent();
	}

}
