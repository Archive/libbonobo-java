package org.gnu.bonobo;


/**
 *	Generated from IDL interface "Stream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class _StreamStub
	extends org.omg.CORBA.portable.ObjectImpl
	implements org.gnu.bonobo.Stream
{
	private String[] ids = {"IDL:Bonobo/Stream:1.0","IDL:Bonobo/Unknown:1.0"};
	public String[] _ids()
	{
		return ids;
	}

	public final static java.lang.Class _opsClass = org.gnu.bonobo.StreamOperations.class;
	public void unImplemented2()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unImplemented2", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unImplemented2", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.unImplemented2();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void read(int count, org.gnu.bonobo.StreamPackage.iobufHolder buffer) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "read", true);
				_os.write_long(count);
				_is = _invoke(_os);
				buffer.value = org.gnu.bonobo.StreamPackage.iobufHelper.read(_is);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Stream/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/IOError:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "read", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.read(count,buffer);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void ref()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "ref", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "ref", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.ref();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void unref()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unref", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unref", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.unref();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void setInfo(org.gnu.bonobo.StorageInfo info, int mask) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "setInfo", true);
				org.gnu.bonobo.StorageInfoHelper.write(_os,info);
				_os.write_long(mask);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Stream/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/IOError:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "setInfo", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.setInfo(info,mask);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void revert() throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "revert", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Stream/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/IOError:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "revert", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.revert();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void commit() throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "commit", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Stream/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/IOError:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "commit", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.commit();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public org.gnu.bonobo.StorageInfo getInfo(int mask) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "getInfo", true);
				_os.write_long(mask);
				_is = _invoke(_os);
				org.gnu.bonobo.StorageInfo _result = org.gnu.bonobo.StorageInfoHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Stream/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/IOError:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "getInfo", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			org.gnu.bonobo.StorageInfo _result;			try
			{
			_result = _localServant.getInfo(mask);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

	public int seek(int offset, org.gnu.bonobo.StreamPackage.SeekType whence) throws org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "seek", true);
				_os.write_long(offset);
				org.gnu.bonobo.StreamPackage.SeekTypeHelper.write(_os,whence);
				_is = _invoke(_os);
				int _result = _is.read_long();
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Stream/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/IOError:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "seek", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			int _result;			try
			{
			_result = _localServant.seek(offset,whence);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

	public void write(byte[] buffer) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "write", true);
				org.gnu.bonobo.StreamPackage.iobufHelper.write(_os,buffer);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Stream/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/IOError:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "write", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.write(buffer);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void unImplemented1()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unImplemented1", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unImplemented1", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.unImplemented1();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "queryInterface", true);
				_os.write_string(repoid);
				_is = _invoke(_os);
				org.gnu.bonobo.Unknown _result = org.gnu.bonobo.UnknownHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "queryInterface", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			org.gnu.bonobo.Unknown _result;			try
			{
			_result = _localServant.queryInterface(repoid);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

	public void truncate(int length) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "truncate", true);
				_os.write_long(length);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Stream/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Stream/IOError:1.0"))
				{
					throw org.gnu.bonobo.StreamPackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "truncate", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StreamOperations _localServant = (StreamOperations)_so.servant;
			try
			{
			_localServant.truncate(length);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

}
