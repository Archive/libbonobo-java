package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "ActivationResultType"
 *	@author JacORB IDL compiler 
 */

public final class ActivationResultTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public ActivationResultType value;

	public ActivationResultTypeHolder ()
	{
	}
	public ActivationResultTypeHolder (final ActivationResultType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ActivationResultTypeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ActivationResultTypeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ActivationResultTypeHelper.write (out,value);
	}
}
