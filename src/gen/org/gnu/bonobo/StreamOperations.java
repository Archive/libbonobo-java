package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Stream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface StreamOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	org.gnu.bonobo.StorageInfo getInfo(int mask) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError;
	void setInfo(org.gnu.bonobo.StorageInfo info, int mask) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError;
	void read(int count, org.gnu.bonobo.StreamPackage.iobufHolder buffer) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.IOError;
	void write(byte[] buffer) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.IOError;
	int seek(int offset, org.gnu.bonobo.StreamPackage.SeekType whence) throws org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError;
	void truncate(int length) throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError;
	void commit() throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError;
	void revert() throws org.gnu.bonobo.StreamPackage.NoPermission,org.gnu.bonobo.StreamPackage.NotSupported,org.gnu.bonobo.StreamPackage.IOError;
	void unImplemented1();
	void unImplemented2();
}
