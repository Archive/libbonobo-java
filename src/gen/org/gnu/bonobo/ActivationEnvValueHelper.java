package org.gnu.bonobo;


/**
 *	Generated from IDL definition of struct "ActivationEnvValue"
 *	@author JacORB IDL compiler 
 */

public final class ActivationEnvValueHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.ActivationEnvValueHelper.id(),"ActivationEnvValue",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("name", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("value", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("flags", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.ActivationEnvValue s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.ActivationEnvValue extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/ActivationEnvValue:1.0";
	}
	public static org.gnu.bonobo.ActivationEnvValue read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.ActivationEnvValue result = new org.gnu.bonobo.ActivationEnvValue();
		result.name=in.read_string();
		result.value=in.read_string();
		result.flags=in.read_long();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.ActivationEnvValue s)
	{
		out.write_string(s.name);
		out.write_string(s.value);
		out.write_long(s.flags);
	}
}
