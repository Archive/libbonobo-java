package org.gnu.bonobo;

/**
 *	Generated from IDL interface "PersistStream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface PersistStreamOperations
	extends org.gnu.bonobo.PersistOperations
{
	/* constants */
	/* operations  */
	void load(org.gnu.bonobo.Stream stream, java.lang.String type) throws org.gnu.bonobo.PersistPackage.WrongDataType,org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported;
	void save(org.gnu.bonobo.Stream stream, java.lang.String type) throws org.gnu.bonobo.PersistPackage.WrongDataType,org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported;
	void unImplemented3();
	void unImplemented4();
}
