package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ActivationResult"
 *	@author JacORB IDL compiler 
 */

public final class ActivationResultHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ActivationResult value;

	public ActivationResultHolder ()
	{
	}
	public ActivationResultHolder(final org.gnu.bonobo.ActivationResult initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.ActivationResultHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.ActivationResultHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.ActivationResultHelper.write(_out, value);
	}
}
