package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "PrintScissor"
 *	@author JacORB IDL compiler 
 */

public final class PrintScissorHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.PrintScissor value;

	public PrintScissorHolder ()
	{
	}
	public PrintScissorHolder(final org.gnu.bonobo.PrintScissor initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.PrintScissorHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.PrintScissorHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.PrintScissorHelper.write(_out, value);
	}
}
