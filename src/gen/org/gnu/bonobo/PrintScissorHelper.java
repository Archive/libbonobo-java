package org.gnu.bonobo;


/**
 *	Generated from IDL definition of struct "PrintScissor"
 *	@author JacORB IDL compiler 
 */

public final class PrintScissorHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.PrintScissorHelper.id(),"PrintScissor",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("width_first_page", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("width_per_page", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("height_first_page", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("height_per_page", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.PrintScissor s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.PrintScissor extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/PrintScissor:1.0";
	}
	public static org.gnu.bonobo.PrintScissor read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.PrintScissor result = new org.gnu.bonobo.PrintScissor();
		result.width_first_page=in.read_double();
		result.width_per_page=in.read_double();
		result.height_first_page=in.read_double();
		result.height_per_page=in.read_double();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.PrintScissor s)
	{
		out.write_double(s.width_first_page);
		out.write_double(s.width_per_page);
		out.write_double(s.height_first_page);
		out.write_double(s.height_per_page);
	}
}
