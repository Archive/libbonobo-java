package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Storage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class StorageHolder	implements org.omg.CORBA.portable.Streamable{
	 public Storage value;
	public StorageHolder()
	{
	}
	public StorageHolder (final Storage initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return StorageHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = StorageHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		StorageHelper.write (_out,value);
	}
}
