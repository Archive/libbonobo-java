package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "MonikerContext"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class MonikerContextPOATie
	extends MonikerContextPOA
{
	private MonikerContextOperations _delegate;

	private POA _poa;
	public MonikerContextPOATie(MonikerContextOperations delegate)
	{
		_delegate = delegate;
	}
	public MonikerContextPOATie(MonikerContextOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.MonikerContext _this()
	{
		return org.gnu.bonobo.MonikerContextHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.MonikerContext _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.MonikerContextHelper.narrow(_this_object(orb));
	}
	public MonikerContextOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(MonikerContextOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void ref()
	{
_delegate.ref();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public org.gnu.bonobo.Unknown getObject(java.lang.String name, java.lang.String repoId)
	{
		return _delegate.getObject(name,repoId);
	}

	public org.gnu.bonobo.Moniker createFromName(java.lang.String name)
	{
		return _delegate.createFromName(name);
	}

	public void unImplemented5()
	{
_delegate.unImplemented5();
	}

	public org.gnu.bonobo.MonikerExtender getExtender(java.lang.String monikerPrefix, java.lang.String interfaceId)
	{
		return _delegate.getExtender(monikerPrefix,interfaceId);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public void unImplemented6()
	{
_delegate.unImplemented6();
	}

}
