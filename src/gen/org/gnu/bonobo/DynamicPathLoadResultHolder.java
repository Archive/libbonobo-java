package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "DynamicPathLoadResult"
 *	@author JacORB IDL compiler 
 */

public final class DynamicPathLoadResultHolder
	implements org.omg.CORBA.portable.Streamable
{
	public DynamicPathLoadResult value;

	public DynamicPathLoadResultHolder ()
	{
	}
	public DynamicPathLoadResultHolder (final DynamicPathLoadResult initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return DynamicPathLoadResultHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = DynamicPathLoadResultHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		DynamicPathLoadResultHelper.write (out,value);
	}
}
