package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "ActivationResultType"
 *	@author JacORB IDL compiler 
 */

public final class ActivationResultType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _ACTIVATION_RESULT_OBJECT = 0;
	public static final ActivationResultType ACTIVATION_RESULT_OBJECT = new ActivationResultType(_ACTIVATION_RESULT_OBJECT);
	public static final int _ACTIVATION_RESULT_SHLIB = 1;
	public static final ActivationResultType ACTIVATION_RESULT_SHLIB = new ActivationResultType(_ACTIVATION_RESULT_SHLIB);
	public static final int _ACTIVATION_RESULT_NONE = 2;
	public static final ActivationResultType ACTIVATION_RESULT_NONE = new ActivationResultType(_ACTIVATION_RESULT_NONE);
	public int value()
	{
		return value;
	}
	public static ActivationResultType from_int(int value)
	{
		switch (value) {
			case _ACTIVATION_RESULT_OBJECT: return ACTIVATION_RESULT_OBJECT;
			case _ACTIVATION_RESULT_SHLIB: return ACTIVATION_RESULT_SHLIB;
			case _ACTIVATION_RESULT_NONE: return ACTIVATION_RESULT_NONE;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected ActivationResultType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
