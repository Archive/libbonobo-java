package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Zoomable"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class ZoomablePOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.ZoomableOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "_get_preferredLevels", new java.lang.Integer(0));
		m_opsHash.put ( "zoomFit", new java.lang.Integer(1));
		m_opsHash.put ( "_get_hasMaxLevel", new java.lang.Integer(2));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(3));
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(4));
		m_opsHash.put ( "_get_level", new java.lang.Integer(5));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(6));
		m_opsHash.put ( "setLevel", new java.lang.Integer(7));
		m_opsHash.put ( "_get_minLevel", new java.lang.Integer(8));
		m_opsHash.put ( "_get_isContinuous", new java.lang.Integer(9));
		m_opsHash.put ( "zoomOut", new java.lang.Integer(10));
		m_opsHash.put ( "setFrame", new java.lang.Integer(11));
		m_opsHash.put ( "unref", new java.lang.Integer(12));
		m_opsHash.put ( "ref", new java.lang.Integer(13));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(14));
		m_opsHash.put ( "zoomIn", new java.lang.Integer(15));
		m_opsHash.put ( "zoomDefault", new java.lang.Integer(16));
		m_opsHash.put ( "_get_preferredLevelNames", new java.lang.Integer(17));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(18));
		m_opsHash.put ( "_get_hasMinLevel", new java.lang.Integer(19));
		m_opsHash.put ( "_get_maxLevel", new java.lang.Integer(20));
	}
	private String[] ids = {"IDL:Bonobo/Zoomable:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.Zoomable _this()
	{
		return org.gnu.bonobo.ZoomableHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Zoomable _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.ZoomableHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // _get_preferredLevels
			{
			_out = handler.createReply();
			org.gnu.bonobo.ZoomLevelListHelper.write(_out,preferredLevels());
				break;
			}
			case 1: // zoomFit
			{
				_out = handler.createReply();
				zoomFit();
				break;
			}
			case 2: // _get_hasMaxLevel
			{
			_out = handler.createReply();
			_out.write_boolean(hasMaxLevel());
				break;
			}
			case 3: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
			case 4: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 5: // _get_level
			{
			_out = handler.createReply();
			_out.write_float(level());
				break;
			}
			case 6: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 7: // setLevel
			{
				float _arg0=_input.read_float();
				_out = handler.createReply();
				setLevel(_arg0);
				break;
			}
			case 8: // _get_minLevel
			{
			_out = handler.createReply();
			_out.write_float(minLevel());
				break;
			}
			case 9: // _get_isContinuous
			{
			_out = handler.createReply();
			_out.write_boolean(isContinuous());
				break;
			}
			case 10: // zoomOut
			{
				_out = handler.createReply();
				zoomOut();
				break;
			}
			case 11: // setFrame
			{
				org.gnu.bonobo.ZoomableFrame _arg0=org.gnu.bonobo.ZoomableFrameHelper.read(_input);
				_out = handler.createReply();
				setFrame(_arg0);
				break;
			}
			case 12: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 13: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 14: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 15: // zoomIn
			{
				_out = handler.createReply();
				zoomIn();
				break;
			}
			case 16: // zoomDefault
			{
				_out = handler.createReply();
				zoomDefault();
				break;
			}
			case 17: // _get_preferredLevelNames
			{
			_out = handler.createReply();
			org.gnu.bonobo.ZoomLevelNameListHelper.write(_out,preferredLevelNames());
				break;
			}
			case 18: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 19: // _get_hasMinLevel
			{
			_out = handler.createReply();
			_out.write_boolean(hasMinLevel());
				break;
			}
			case 20: // _get_maxLevel
			{
			_out = handler.createReply();
			_out.write_float(maxLevel());
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
