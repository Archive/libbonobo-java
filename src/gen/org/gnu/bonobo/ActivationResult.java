package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ActivationResult"
 *	@author JacORB IDL compiler 
 */

public final class ActivationResult
	implements org.omg.CORBA.portable.IDLEntity
{
	public ActivationResult(){}
	public java.lang.String aid = "";
	public org.gnu.bonobo.ActivationResultData res;
	public ActivationResult(java.lang.String aid, org.gnu.bonobo.ActivationResultData res)
	{
		this.aid = aid;
		this.res = res;
	}
}
