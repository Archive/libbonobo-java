package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ActivationEnvValue"
 *	@author JacORB IDL compiler 
 */

public final class ActivationEnvValue
	implements org.omg.CORBA.portable.IDLEntity
{
	public ActivationEnvValue(){}
	public java.lang.String name = "";
	public java.lang.String value = "";
	public int flags;
	public ActivationEnvValue(java.lang.String name, java.lang.String value, int flags)
	{
		this.name = name;
		this.value = value;
		this.flags = flags;
	}
}
