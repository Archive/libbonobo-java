package org.gnu.bonobo;

/**
 *	Generated from IDL definition of alias "ActivationEnvironment"
 *	@author JacORB IDL compiler 
 */

public final class ActivationEnvironmentHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ActivationEnvValue[] value;

	public ActivationEnvironmentHolder ()
	{
	}
	public ActivationEnvironmentHolder (final org.gnu.bonobo.ActivationEnvValue[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ActivationEnvironmentHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ActivationEnvironmentHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ActivationEnvironmentHelper.write (out,value);
	}
}
