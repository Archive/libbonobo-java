package org.gnu.bonobo;

/**
 *	Generated from IDL interface "MonikerExtender"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface MonikerExtenderOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	org.gnu.bonobo.Unknown resolve(org.gnu.bonobo.Moniker m, org.gnu.bonobo.ResolveOptions options, java.lang.String name, java.lang.String requestedInterface) throws org.gnu.bonobo.MonikerPackage.TimeOut,org.gnu.bonobo.MonikerPackage.InterfaceNotFound,org.gnu.bonobo.GeneralError;
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
