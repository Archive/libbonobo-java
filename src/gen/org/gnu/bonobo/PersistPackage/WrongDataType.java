package org.gnu.bonobo.PersistPackage;

/**
 *	Generated from IDL definition of exception "WrongDataType"
 *	@author JacORB IDL compiler 
 */

public final class WrongDataType
	extends org.omg.CORBA.UserException
{
	public WrongDataType()
	{
		super(org.gnu.bonobo.PersistPackage.WrongDataTypeHelper.id());
	}

	public WrongDataType(String value)
	{
		super(value);
	}
}
