package org.gnu.bonobo.PersistPackage;

/**
 *	Generated from IDL definition of exception "FileNotFound"
 *	@author JacORB IDL compiler 
 */

public final class FileNotFoundHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.PersistPackage.FileNotFound value;

	public FileNotFoundHolder ()
	{
	}
	public FileNotFoundHolder(final org.gnu.bonobo.PersistPackage.FileNotFound initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.PersistPackage.FileNotFoundHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.PersistPackage.FileNotFoundHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.PersistPackage.FileNotFoundHelper.write(_out, value);
	}
}
