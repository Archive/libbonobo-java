package org.gnu.bonobo.PersistPackage;


/**
 *	Generated from IDL definition of exception "WrongDataType"
 *	@author JacORB IDL compiler 
 */

public final class WrongDataTypeHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_exception_tc(org.gnu.bonobo.PersistPackage.WrongDataTypeHelper.id(),"WrongDataType",new org.omg.CORBA.StructMember[0]);
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.PersistPackage.WrongDataType s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.PersistPackage.WrongDataType extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Persist/WrongDataType:1.0";
	}
	public static org.gnu.bonobo.PersistPackage.WrongDataType read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.PersistPackage.WrongDataType result = new org.gnu.bonobo.PersistPackage.WrongDataType();
		if (!in.read_string().equals(id())) throw new org.omg.CORBA.MARSHAL("wrong id");
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.PersistPackage.WrongDataType s)
	{
		out.write_string(id());
	}
}
