package org.gnu.bonobo.PersistPackage;

/**
 *	Generated from IDL definition of alias "ContentTypeList"
 *	@author JacORB IDL compiler 
 */

public final class ContentTypeListHolder
	implements org.omg.CORBA.portable.Streamable
{
	public java.lang.String[] value;

	public ContentTypeListHolder ()
	{
	}
	public ContentTypeListHolder (final java.lang.String[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ContentTypeListHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ContentTypeListHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ContentTypeListHelper.write (out,value);
	}
}
