package org.gnu.bonobo.PersistPackage;
/**
 *	Generated from IDL definition of enum "Status"
 *	@author JacORB IDL compiler 
 */

public final class Status
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _SAVE_OK = 0;
	public static final Status SAVE_OK = new Status(_SAVE_OK);
	public static final int _SAVE_CANCEL = 1;
	public static final Status SAVE_CANCEL = new Status(_SAVE_CANCEL);
	public static final int _SAVE_FAILED = 2;
	public static final Status SAVE_FAILED = new Status(_SAVE_FAILED);
	public int value()
	{
		return value;
	}
	public static Status from_int(int value)
	{
		switch (value) {
			case _SAVE_OK: return SAVE_OK;
			case _SAVE_CANCEL: return SAVE_CANCEL;
			case _SAVE_FAILED: return SAVE_FAILED;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected Status(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
