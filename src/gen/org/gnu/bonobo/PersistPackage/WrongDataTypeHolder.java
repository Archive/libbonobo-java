package org.gnu.bonobo.PersistPackage;

/**
 *	Generated from IDL definition of exception "WrongDataType"
 *	@author JacORB IDL compiler 
 */

public final class WrongDataTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.PersistPackage.WrongDataType value;

	public WrongDataTypeHolder ()
	{
	}
	public WrongDataTypeHolder(final org.gnu.bonobo.PersistPackage.WrongDataType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.PersistPackage.WrongDataTypeHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.PersistPackage.WrongDataTypeHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.PersistPackage.WrongDataTypeHelper.write(_out, value);
	}
}
