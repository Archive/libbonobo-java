package org.gnu.bonobo.PersistPackage;
/**
 *	Generated from IDL definition of enum "Status"
 *	@author JacORB IDL compiler 
 */

public final class StatusHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.PersistPackage.StatusHelper.id(),"Status",new String[]{"SAVE_OK","SAVE_CANCEL","SAVE_FAILED"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.PersistPackage.Status s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.PersistPackage.Status extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Persist/Status:1.0";
	}
	public static Status read (final org.omg.CORBA.portable.InputStream in)
	{
		return Status.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final Status s)
	{
		out.write_long(s.value());
	}
}
