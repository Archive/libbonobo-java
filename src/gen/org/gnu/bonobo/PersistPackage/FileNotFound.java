package org.gnu.bonobo.PersistPackage;

/**
 *	Generated from IDL definition of exception "FileNotFound"
 *	@author JacORB IDL compiler 
 */

public final class FileNotFound
	extends org.omg.CORBA.UserException
{
	public FileNotFound()
	{
		super(org.gnu.bonobo.PersistPackage.FileNotFoundHelper.id());
	}

	public FileNotFound(String value)
	{
		super(value);
	}
}
