package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "GeneralError"
 *	@author JacORB IDL compiler 
 */

public final class GeneralError
	extends org.omg.CORBA.UserException
{
	public GeneralError()
	{
		super(org.gnu.bonobo.GeneralErrorHelper.id());
	}

	public java.lang.String description = "";
	public GeneralError(java.lang.String _reason,java.lang.String description)
	{
		super(org.gnu.bonobo.GeneralErrorHelper.id()+ " " + _reason);
		this.description = description;
	}
	public GeneralError(java.lang.String description)
	{
		super(org.gnu.bonobo.GeneralErrorHelper.id());
		this.description = description;
	}
}
