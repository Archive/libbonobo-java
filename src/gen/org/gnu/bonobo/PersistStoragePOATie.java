package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "PersistStorage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class PersistStoragePOATie
	extends PersistStoragePOA
{
	private PersistStorageOperations _delegate;

	private POA _poa;
	public PersistStoragePOATie(PersistStorageOperations delegate)
	{
		_delegate = delegate;
	}
	public PersistStoragePOATie(PersistStorageOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.PersistStorage _this()
	{
		return org.gnu.bonobo.PersistStorageHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.PersistStorage _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.PersistStorageHelper.narrow(_this_object(orb));
	}
	public PersistStorageOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(PersistStorageOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void ref()
	{
_delegate.ref();
	}

	public void load(org.gnu.bonobo.Storage storage) throws org.gnu.bonobo.PersistPackage.WrongDataType,org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported
	{
_delegate.load(storage);
	}

	public boolean isDirty()
	{
		return _delegate.isDirty();
	}

	public void unref()
	{
_delegate.unref();
	}

	public java.lang.String[] getContentTypes()
	{
		return _delegate.getContentTypes();
	}

	public java.lang.String getIId()
	{
		return _delegate.getIId();
	}

	public void save(org.gnu.bonobo.Storage storage, boolean same_as_loaded) throws org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported
	{
_delegate.save(storage,same_as_loaded);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
