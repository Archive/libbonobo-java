package org.gnu.bonobo;
/**
 *	Generated from IDL definition of union "ActivationResultData"
 *	@author JacORB IDL compiler 
 */

public final class ActivationResultDataHolder
	implements org.omg.CORBA.portable.Streamable
{
	public ActivationResultData value;

	public ActivationResultDataHolder ()
	{
	}
	public ActivationResultDataHolder (final ActivationResultData initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ActivationResultDataHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ActivationResultDataHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ActivationResultDataHelper.write (out, value);
	}
}
