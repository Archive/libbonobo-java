package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "UIContainer"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class UIContainerPOATie
	extends UIContainerPOA
{
	private UIContainerOperations _delegate;

	private POA _poa;
	public UIContainerPOATie(UIContainerOperations delegate)
	{
		_delegate = delegate;
	}
	public UIContainerPOATie(UIContainerOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.UIContainer _this()
	{
		return org.gnu.bonobo.UIContainerHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.UIContainer _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.UIContainerHelper.narrow(_this_object(orb));
	}
	public UIContainerOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(UIContainerOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void freeze()
	{
_delegate.freeze();
	}

	public void unImplemented()
	{
_delegate.unImplemented();
	}

	public boolean exists(java.lang.String path)
	{
		return _delegate.exists(path);
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void removeNode(java.lang.String path, java.lang.String by_component_name) throws org.gnu.bonobo.UIContainerPackage.InvalidPath
	{
_delegate.removeNode(path,by_component_name);
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void execVerb(java.lang.String cname) throws org.gnu.bonobo.UIContainerPackage.Insensitive,org.gnu.bonobo.UIContainerPackage.Unknown
	{
_delegate.execVerb(cname);
	}

	public void setNode(java.lang.String path, java.lang.String xml, java.lang.String component_name) throws org.gnu.bonobo.UIContainerPackage.InvalidPath,org.gnu.bonobo.UIContainerPackage.MalformedXML
	{
_delegate.setNode(path,xml,component_name);
	}

	public void deregisterComponent(java.lang.String component_name)
	{
_delegate.deregisterComponent(component_name);
	}

	public void thaw()
	{
_delegate.thaw();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void ref()
	{
_delegate.ref();
	}

	public void uiEvent(java.lang.String id, org.gnu.bonobo.UIComponentPackage.EventType type, java.lang.String state) throws org.gnu.bonobo.UIContainerPackage.Insensitive,org.gnu.bonobo.UIContainerPackage.Unknown
	{
_delegate.uiEvent(id,type,state);
	}

	public java.lang.String getAttr(java.lang.String path, java.lang.String attr) throws org.gnu.bonobo.UIContainerPackage.NonExistentAttr,org.gnu.bonobo.UIContainerPackage.InvalidPath
	{
		return _delegate.getAttr(path,attr);
	}

	public org.gnu.bonobo.Unknown getObject(java.lang.String path) throws org.gnu.bonobo.UIContainerPackage.InvalidPath
	{
		return _delegate.getObject(path);
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void setObject(java.lang.String path, org.gnu.bonobo.Unknown control) throws org.gnu.bonobo.UIContainerPackage.InvalidPath
	{
_delegate.setObject(path,control);
	}

	public java.lang.String getNode(java.lang.String path, boolean nodeOnly) throws org.gnu.bonobo.UIContainerPackage.InvalidPath
	{
		return _delegate.getNode(path,nodeOnly);
	}

	public void setAttr(java.lang.String path, java.lang.String attr, java.lang.String value, java.lang.String component_name)
	{
_delegate.setAttr(path,attr,value,component_name);
	}

	public void registerComponent(java.lang.String component_name, org.gnu.bonobo.UIComponent component)
	{
_delegate.registerComponent(component_name,component);
	}

}
