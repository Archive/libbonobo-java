package org.gnu.bonobo;

/**
 *	Generated from IDL interface "UIContainer"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface UIContainerOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	void registerComponent(java.lang.String component_name, org.gnu.bonobo.UIComponent component);
	void deregisterComponent(java.lang.String component_name);
	void freeze();
	void thaw();
	void setAttr(java.lang.String path, java.lang.String attr, java.lang.String value, java.lang.String component_name);
	java.lang.String getAttr(java.lang.String path, java.lang.String attr) throws org.gnu.bonobo.UIContainerPackage.NonExistentAttr,org.gnu.bonobo.UIContainerPackage.InvalidPath;
	void setNode(java.lang.String path, java.lang.String xml, java.lang.String component_name) throws org.gnu.bonobo.UIContainerPackage.InvalidPath,org.gnu.bonobo.UIContainerPackage.MalformedXML;
	java.lang.String getNode(java.lang.String path, boolean nodeOnly) throws org.gnu.bonobo.UIContainerPackage.InvalidPath;
	void removeNode(java.lang.String path, java.lang.String by_component_name) throws org.gnu.bonobo.UIContainerPackage.InvalidPath;
	boolean exists(java.lang.String path);
	void execVerb(java.lang.String cname) throws org.gnu.bonobo.UIContainerPackage.Insensitive,org.gnu.bonobo.UIContainerPackage.Unknown;
	void uiEvent(java.lang.String id, org.gnu.bonobo.UIComponentPackage.EventType type, java.lang.String state) throws org.gnu.bonobo.UIContainerPackage.Insensitive,org.gnu.bonobo.UIContainerPackage.Unknown;
	void setObject(java.lang.String path, org.gnu.bonobo.Unknown control) throws org.gnu.bonobo.UIContainerPackage.InvalidPath;
	org.gnu.bonobo.Unknown getObject(java.lang.String path) throws org.gnu.bonobo.UIContainerPackage.InvalidPath;
	void unImplemented();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
