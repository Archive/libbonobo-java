package org.gnu.bonobo;

/**
 *	Generated from IDL interface "RunningContext"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface RunningContextOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	void addObject(org.omg.CORBA.Object obj);
	void removeObject(org.omg.CORBA.Object obj);
	void addKey(java.lang.String key);
	void removeKey(java.lang.String key);
	void atExitUnref(org.omg.CORBA.Object obj);
	void unImplemented1();
	void unImplemented2();
}
