package org.gnu.bonobo.Activation;

/**
 *	Generated from IDL definition of exception "AlreadyListed"
 *	@author JacORB IDL compiler 
 */

public final class AlreadyListedHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Activation.AlreadyListed value;

	public AlreadyListedHolder ()
	{
	}
	public AlreadyListedHolder(final org.gnu.bonobo.Activation.AlreadyListed initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Activation.AlreadyListedHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Activation.AlreadyListedHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Activation.AlreadyListedHelper.write(_out, value);
	}
}
