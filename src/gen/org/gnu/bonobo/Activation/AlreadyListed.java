package org.gnu.bonobo.Activation;

/**
 *	Generated from IDL definition of exception "AlreadyListed"
 *	@author JacORB IDL compiler 
 */

public final class AlreadyListed
	extends org.omg.CORBA.UserException
{
	public AlreadyListed()
	{
		super(org.gnu.bonobo.Activation.AlreadyListedHelper.id());
	}

	public AlreadyListed(String value)
	{
		super(value);
	}
}
