package org.gnu.bonobo.Activation;

/**
 *	Generated from IDL definition of exception "NotListed"
 *	@author JacORB IDL compiler 
 */

public final class NotListed
	extends org.omg.CORBA.UserException
{
	public NotListed()
	{
		super(org.gnu.bonobo.Activation.NotListedHelper.id());
	}

	public NotListed(String value)
	{
		super(value);
	}
}
