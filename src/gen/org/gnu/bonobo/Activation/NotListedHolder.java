package org.gnu.bonobo.Activation;

/**
 *	Generated from IDL definition of exception "NotListed"
 *	@author JacORB IDL compiler 
 */

public final class NotListedHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Activation.NotListed value;

	public NotListedHolder ()
	{
	}
	public NotListedHolder(final org.gnu.bonobo.Activation.NotListed initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Activation.NotListedHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Activation.NotListedHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Activation.NotListedHelper.write(_out, value);
	}
}
