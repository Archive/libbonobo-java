package org.gnu.bonobo.Activation;

/**
 *	Generated from IDL definition of exception "IncompleteContext"
 *	@author JacORB IDL compiler 
 */

public final class IncompleteContextHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Activation.IncompleteContext value;

	public IncompleteContextHolder ()
	{
	}
	public IncompleteContextHolder(final org.gnu.bonobo.Activation.IncompleteContext initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Activation.IncompleteContextHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Activation.IncompleteContextHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Activation.IncompleteContextHelper.write(_out, value);
	}
}
