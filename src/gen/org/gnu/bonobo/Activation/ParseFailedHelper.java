package org.gnu.bonobo.Activation;


/**
 *	Generated from IDL definition of exception "ParseFailed"
 *	@author JacORB IDL compiler 
 */

public final class ParseFailedHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_exception_tc(org.gnu.bonobo.Activation.ParseFailedHelper.id(),"ParseFailed",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("description", org.omg.CORBA.ORB.init().create_string_tc(0), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Activation.ParseFailed s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Activation.ParseFailed extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Activation/ParseFailed:1.0";
	}
	public static org.gnu.bonobo.Activation.ParseFailed read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Activation.ParseFailed result = new org.gnu.bonobo.Activation.ParseFailed();
		if (!in.read_string().equals(id())) throw new org.omg.CORBA.MARSHAL("wrong id");
		result.description=in.read_string();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Activation.ParseFailed s)
	{
		out.write_string(id());
		out.write_string(s.description);
	}
}
