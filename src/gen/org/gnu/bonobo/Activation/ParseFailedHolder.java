package org.gnu.bonobo.Activation;

/**
 *	Generated from IDL definition of exception "ParseFailed"
 *	@author JacORB IDL compiler 
 */

public final class ParseFailedHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Activation.ParseFailed value;

	public ParseFailedHolder ()
	{
	}
	public ParseFailedHolder(final org.gnu.bonobo.Activation.ParseFailed initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Activation.ParseFailedHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Activation.ParseFailedHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Activation.ParseFailedHelper.write(_out, value);
	}
}
