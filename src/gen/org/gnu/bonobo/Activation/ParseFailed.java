package org.gnu.bonobo.Activation;

/**
 *	Generated from IDL definition of exception "ParseFailed"
 *	@author JacORB IDL compiler 
 */

public final class ParseFailed
	extends org.omg.CORBA.UserException
{
	public ParseFailed()
	{
		super(org.gnu.bonobo.Activation.ParseFailedHelper.id());
	}

	public java.lang.String description = "";
	public ParseFailed(java.lang.String _reason,java.lang.String description)
	{
		super(org.gnu.bonobo.Activation.ParseFailedHelper.id()+ " " + _reason);
		this.description = description;
	}
	public ParseFailed(java.lang.String description)
	{
		super(org.gnu.bonobo.Activation.ParseFailedHelper.id());
		this.description = description;
	}
}
