package org.gnu.bonobo.Activation;

/**
 *	Generated from IDL definition of exception "IncompleteContext"
 *	@author JacORB IDL compiler 
 */

public final class IncompleteContext
	extends org.omg.CORBA.UserException
{
	public IncompleteContext()
	{
		super(org.gnu.bonobo.Activation.IncompleteContextHelper.id());
	}

	public IncompleteContext(String value)
	{
		super(value);
	}
}
