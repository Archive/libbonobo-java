package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Stream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class StreamPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.StreamOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(0));
		m_opsHash.put ( "read", new java.lang.Integer(1));
		m_opsHash.put ( "ref", new java.lang.Integer(2));
		m_opsHash.put ( "unref", new java.lang.Integer(3));
		m_opsHash.put ( "setInfo", new java.lang.Integer(4));
		m_opsHash.put ( "revert", new java.lang.Integer(5));
		m_opsHash.put ( "commit", new java.lang.Integer(6));
		m_opsHash.put ( "getInfo", new java.lang.Integer(7));
		m_opsHash.put ( "seek", new java.lang.Integer(8));
		m_opsHash.put ( "write", new java.lang.Integer(9));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(10));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(11));
		m_opsHash.put ( "truncate", new java.lang.Integer(12));
	}
	private String[] ids = {"IDL:Bonobo/Stream:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.Stream _this()
	{
		return org.gnu.bonobo.StreamHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Stream _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.StreamHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 1: // read
			{
			try
			{
				int _arg0=_input.read_long();
				org.gnu.bonobo.StreamPackage.iobufHolder _arg1= new org.gnu.bonobo.StreamPackage.iobufHolder();
				_out = handler.createReply();
				read(_arg0,_arg1);
				org.gnu.bonobo.StreamPackage.iobufHelper.write(_out,_arg1.value);
			}
			catch(org.gnu.bonobo.StreamPackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StreamPackage.IOError _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.IOErrorHelper.write(_out, _ex1);
			}
				break;
			}
			case 2: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 3: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 4: // setInfo
			{
			try
			{
				org.gnu.bonobo.StorageInfo _arg0=org.gnu.bonobo.StorageInfoHelper.read(_input);
				int _arg1=_input.read_long();
				_out = handler.createReply();
				setInfo(_arg0,_arg1);
			}
			catch(org.gnu.bonobo.StreamPackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StreamPackage.NotSupported _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NotSupportedHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StreamPackage.IOError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.IOErrorHelper.write(_out, _ex2);
			}
				break;
			}
			case 5: // revert
			{
			try
			{
				_out = handler.createReply();
				revert();
			}
			catch(org.gnu.bonobo.StreamPackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StreamPackage.NotSupported _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NotSupportedHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StreamPackage.IOError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.IOErrorHelper.write(_out, _ex2);
			}
				break;
			}
			case 6: // commit
			{
			try
			{
				_out = handler.createReply();
				commit();
			}
			catch(org.gnu.bonobo.StreamPackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StreamPackage.NotSupported _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NotSupportedHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StreamPackage.IOError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.IOErrorHelper.write(_out, _ex2);
			}
				break;
			}
			case 7: // getInfo
			{
			try
			{
				int _arg0=_input.read_long();
				_out = handler.createReply();
				org.gnu.bonobo.StorageInfoHelper.write(_out,getInfo(_arg0));
			}
			catch(org.gnu.bonobo.StreamPackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StreamPackage.NotSupported _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NotSupportedHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StreamPackage.IOError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.IOErrorHelper.write(_out, _ex2);
			}
				break;
			}
			case 8: // seek
			{
			try
			{
				int _arg0=_input.read_long();
				org.gnu.bonobo.StreamPackage.SeekType _arg1=org.gnu.bonobo.StreamPackage.SeekTypeHelper.read(_input);
				_out = handler.createReply();
				_out.write_long(seek(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.StreamPackage.NotSupported _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NotSupportedHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StreamPackage.IOError _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.IOErrorHelper.write(_out, _ex1);
			}
				break;
			}
			case 9: // write
			{
			try
			{
				byte[] _arg0=org.gnu.bonobo.StreamPackage.iobufHelper.read(_input);
				_out = handler.createReply();
				write(_arg0);
			}
			catch(org.gnu.bonobo.StreamPackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StreamPackage.IOError _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.IOErrorHelper.write(_out, _ex1);
			}
				break;
			}
			case 10: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 11: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
			case 12: // truncate
			{
			try
			{
				int _arg0=_input.read_long();
				_out = handler.createReply();
				truncate(_arg0);
			}
			catch(org.gnu.bonobo.StreamPackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StreamPackage.NotSupported _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.NotSupportedHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StreamPackage.IOError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StreamPackage.IOErrorHelper.write(_out, _ex2);
			}
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
