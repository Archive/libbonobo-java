package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "StorageInfo"
 *	@author JacORB IDL compiler 
 */

public final class StorageInfoHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.StorageInfo value;

	public StorageInfoHolder ()
	{
	}
	public StorageInfoHolder(final org.gnu.bonobo.StorageInfo initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.StorageInfoHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.StorageInfoHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.StorageInfoHelper.write(_out, value);
	}
}
