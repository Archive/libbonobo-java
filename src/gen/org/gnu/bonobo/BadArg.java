package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "BadArg"
 *	@author JacORB IDL compiler 
 */

public final class BadArg
	extends org.omg.CORBA.UserException
{
	public BadArg()
	{
		super(org.gnu.bonobo.BadArgHelper.id());
	}

	public BadArg(String value)
	{
		super(value);
	}
}
