package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Storage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class StoragePOATie
	extends StoragePOA
{
	private StorageOperations _delegate;

	private POA _poa;
	public StoragePOATie(StorageOperations delegate)
	{
		_delegate = delegate;
	}
	public StoragePOATie(StorageOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Storage _this()
	{
		return org.gnu.bonobo.StorageHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Storage _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.StorageHelper.narrow(_this_object(orb));
	}
	public StorageOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(StorageOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void rename(java.lang.String path_name, java.lang.String new_path_name) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError
	{
_delegate.rename(path_name,new_path_name);
	}

	public void erase(java.lang.String path) throws org.gnu.bonobo.StoragePackage.NotEmpty,org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError
	{
_delegate.erase(path);
	}

	public void ref()
	{
_delegate.ref();
	}

	public org.gnu.bonobo.StorageInfo[] listContents(java.lang.String path, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.NotStorage,org.gnu.bonobo.StoragePackage.IOError
	{
		return _delegate.listContents(path,mask);
	}

	public void unref()
	{
_delegate.unref();
	}

	public org.gnu.bonobo.Storage openStorage(java.lang.String path, int mode) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotStorage,org.gnu.bonobo.StoragePackage.IOError
	{
		return _delegate.openStorage(path,mode);
	}

	public void revert() throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError
	{
_delegate.revert();
	}

	public void commit() throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError
	{
_delegate.commit();
	}

	public void setInfo(java.lang.String path, org.gnu.bonobo.StorageInfo info, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError
	{
_delegate.setInfo(path,info,mask);
	}

	public org.gnu.bonobo.Stream openStream(java.lang.String path, int mode) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotStream,org.gnu.bonobo.StoragePackage.IOError
	{
		return _delegate.openStream(path,mode);
	}

	public void copyTo(org.gnu.bonobo.Storage target) throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError
	{
_delegate.copyTo(target);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public org.gnu.bonobo.StorageInfo getInfo(java.lang.String path, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError
	{
		return _delegate.getInfo(path,mask);
	}

}
