package org.gnu.bonobo;

/**
 *	Generated from IDL interface "MonikerExtender"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class MonikerExtenderPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.MonikerExtenderOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "ref", new java.lang.Integer(0));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(1));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(2));
		m_opsHash.put ( "unref", new java.lang.Integer(3));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(4));
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(5));
		m_opsHash.put ( "resolve", new java.lang.Integer(6));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(7));
	}
	private String[] ids = {"IDL:Bonobo/MonikerExtender:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.MonikerExtender _this()
	{
		return org.gnu.bonobo.MonikerExtenderHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.MonikerExtender _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.MonikerExtenderHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 1: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 2: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 3: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 4: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 5: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 6: // resolve
			{
			try
			{
				org.gnu.bonobo.Moniker _arg0=org.gnu.bonobo.MonikerHelper.read(_input);
				org.gnu.bonobo.ResolveOptions _arg1=org.gnu.bonobo.ResolveOptionsHelper.read(_input);
				java.lang.String _arg2=_input.read_string();
				java.lang.String _arg3=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,resolve(_arg0,_arg1,_arg2,_arg3));
			}
			catch(org.gnu.bonobo.MonikerPackage.TimeOut _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.MonikerPackage.TimeOutHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.MonikerPackage.InterfaceNotFound _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.MonikerPackage.InterfaceNotFoundHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.GeneralError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.GeneralErrorHelper.write(_out, _ex2);
			}
				break;
			}
			case 7: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
