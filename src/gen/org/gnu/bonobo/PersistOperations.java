package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Persist"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface PersistOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	java.lang.String[] getContentTypes();
	java.lang.String getIId();
	boolean isDirty();
	void unImplemented1();
	void unImplemented2();
}
