package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Zoomable"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface ZoomableOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	float level();
	float minLevel();
	float maxLevel();
	boolean hasMinLevel();
	boolean hasMaxLevel();
	boolean isContinuous();
	float[] preferredLevels();
	java.lang.String[] preferredLevelNames();
	void zoomIn();
	void zoomOut();
	void zoomFit();
	void zoomDefault();
	void setLevel(float zoom_level);
	void setFrame(org.gnu.bonobo.ZoomableFrame zoomable_frame);
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
