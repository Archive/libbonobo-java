package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "DynamicPathLoadResult"
 *	@author JacORB IDL compiler 
 */

public final class DynamicPathLoadResult
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _DYNAMIC_LOAD_SUCCESS = 0;
	public static final DynamicPathLoadResult DYNAMIC_LOAD_SUCCESS = new DynamicPathLoadResult(_DYNAMIC_LOAD_SUCCESS);
	public static final int _DYNAMIC_LOAD_ERROR = 1;
	public static final DynamicPathLoadResult DYNAMIC_LOAD_ERROR = new DynamicPathLoadResult(_DYNAMIC_LOAD_ERROR);
	public static final int _DYNAMIC_LOAD_NOT_LISTED = 2;
	public static final DynamicPathLoadResult DYNAMIC_LOAD_NOT_LISTED = new DynamicPathLoadResult(_DYNAMIC_LOAD_NOT_LISTED);
	public static final int _DYNAMIC_LOAD_ALREADY_LISTED = 3;
	public static final DynamicPathLoadResult DYNAMIC_LOAD_ALREADY_LISTED = new DynamicPathLoadResult(_DYNAMIC_LOAD_ALREADY_LISTED);
	public int value()
	{
		return value;
	}
	public static DynamicPathLoadResult from_int(int value)
	{
		switch (value) {
			case _DYNAMIC_LOAD_SUCCESS: return DYNAMIC_LOAD_SUCCESS;
			case _DYNAMIC_LOAD_ERROR: return DYNAMIC_LOAD_ERROR;
			case _DYNAMIC_LOAD_NOT_LISTED: return DYNAMIC_LOAD_NOT_LISTED;
			case _DYNAMIC_LOAD_ALREADY_LISTED: return DYNAMIC_LOAD_ALREADY_LISTED;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected DynamicPathLoadResult(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
