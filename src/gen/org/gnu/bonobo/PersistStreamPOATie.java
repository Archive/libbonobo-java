package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "PersistStream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class PersistStreamPOATie
	extends PersistStreamPOA
{
	private PersistStreamOperations _delegate;

	private POA _poa;
	public PersistStreamPOATie(PersistStreamOperations delegate)
	{
		_delegate = delegate;
	}
	public PersistStreamPOATie(PersistStreamOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.PersistStream _this()
	{
		return org.gnu.bonobo.PersistStreamHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.PersistStream _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.PersistStreamHelper.narrow(_this_object(orb));
	}
	public PersistStreamOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(PersistStreamOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void ref()
	{
_delegate.ref();
	}

	public boolean isDirty()
	{
		return _delegate.isDirty();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unref()
	{
_delegate.unref();
	}

	public java.lang.String[] getContentTypes()
	{
		return _delegate.getContentTypes();
	}

	public java.lang.String getIId()
	{
		return _delegate.getIId();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void save(org.gnu.bonobo.Stream stream, java.lang.String type) throws org.gnu.bonobo.PersistPackage.WrongDataType,org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported
	{
_delegate.save(stream,type);
	}

	public void load(org.gnu.bonobo.Stream stream, java.lang.String type) throws org.gnu.bonobo.PersistPackage.WrongDataType,org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported
	{
_delegate.load(stream,type);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
