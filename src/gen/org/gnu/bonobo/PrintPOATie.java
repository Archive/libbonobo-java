package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Print"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class PrintPOATie
	extends PrintPOA
{
	private PrintOperations _delegate;

	private POA _poa;
	public PrintPOATie(PrintOperations delegate)
	{
		_delegate = delegate;
	}
	public PrintPOATie(PrintOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Print _this()
	{
		return org.gnu.bonobo.PrintHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Print _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.PrintHelper.narrow(_this_object(orb));
	}
	public PrintOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(PrintOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void ref()
	{
_delegate.ref();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public org.gnu.bonobo.Stream render(org.gnu.bonobo.PrintDimensions pd, org.gnu.bonobo.PrintScissor scissor)
	{
		return _delegate.render(pd,scissor);
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
