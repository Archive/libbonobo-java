package org.gnu.bonobo;

/**
 *	Generated from IDL definition of alias "ServerInfoList"
 *	@author JacORB IDL compiler 
 */

public final class ServerInfoListHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ServerInfo[] value;

	public ServerInfoListHolder ()
	{
	}
	public ServerInfoListHolder (final org.gnu.bonobo.ServerInfo[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ServerInfoListHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ServerInfoListHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ServerInfoListHelper.write (out,value);
	}
}
