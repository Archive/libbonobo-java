package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Unknown"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class UnknownPOATie
	extends UnknownPOA
{
	private UnknownOperations _delegate;

	private POA _poa;
	public UnknownPOATie(UnknownOperations delegate)
	{
		_delegate = delegate;
	}
	public UnknownPOATie(UnknownOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Unknown _this()
	{
		return org.gnu.bonobo.UnknownHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Unknown _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.UnknownHelper.narrow(_this_object(orb));
	}
	public UnknownOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(UnknownOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void ref()
	{
_delegate.ref();
	}

	public void unref()
	{
_delegate.unref();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
