package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ZoomableFrame"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class ZoomableFramePOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.ZoomableFrameOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "onLevelChanged", new java.lang.Integer(0));
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(1));
		m_opsHash.put ( "ref", new java.lang.Integer(2));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(3));
		m_opsHash.put ( "unref", new java.lang.Integer(4));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(5));
		m_opsHash.put ( "onParametersChanged", new java.lang.Integer(6));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(7));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(8));
	}
	private String[] ids = {"IDL:Bonobo/ZoomableFrame:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.ZoomableFrame _this()
	{
		return org.gnu.bonobo.ZoomableFrameHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.ZoomableFrame _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.ZoomableFrameHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // onLevelChanged
			{
				float _arg0=_input.read_float();
				_out = handler.createReply();
				onLevelChanged(_arg0);
				break;
			}
			case 1: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 2: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 3: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 4: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 5: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 6: // onParametersChanged
			{
				_out = handler.createReply();
				onParametersChanged();
				break;
			}
			case 7: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 8: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
