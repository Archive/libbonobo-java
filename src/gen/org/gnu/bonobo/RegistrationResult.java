package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "RegistrationResult"
 *	@author JacORB IDL compiler 
 */

public final class RegistrationResult
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _ACTIVATION_REG_SUCCESS = 0;
	public static final RegistrationResult ACTIVATION_REG_SUCCESS = new RegistrationResult(_ACTIVATION_REG_SUCCESS);
	public static final int _ACTIVATION_REG_NOT_LISTED = 1;
	public static final RegistrationResult ACTIVATION_REG_NOT_LISTED = new RegistrationResult(_ACTIVATION_REG_NOT_LISTED);
	public static final int _ACTIVATION_REG_ALREADY_ACTIVE = 2;
	public static final RegistrationResult ACTIVATION_REG_ALREADY_ACTIVE = new RegistrationResult(_ACTIVATION_REG_ALREADY_ACTIVE);
	public static final int _ACTIVATION_REG_ERROR = 3;
	public static final RegistrationResult ACTIVATION_REG_ERROR = new RegistrationResult(_ACTIVATION_REG_ERROR);
	public int value()
	{
		return value;
	}
	public static RegistrationResult from_int(int value)
	{
		switch (value) {
			case _ACTIVATION_REG_SUCCESS: return ACTIVATION_REG_SUCCESS;
			case _ACTIVATION_REG_NOT_LISTED: return ACTIVATION_REG_NOT_LISTED;
			case _ACTIVATION_REG_ALREADY_ACTIVE: return ACTIVATION_REG_ALREADY_ACTIVE;
			case _ACTIVATION_REG_ERROR: return ACTIVATION_REG_ERROR;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected RegistrationResult(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
