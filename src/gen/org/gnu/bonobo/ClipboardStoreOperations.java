package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ClipboardStore"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface ClipboardStoreOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	void fetchStream(org.gnu.bonobo.PersistStream source, org.gnu.bonobo.Moniker linking_moniker);
	void fetchStorage(org.gnu.bonobo.PersistStorage source, org.gnu.bonobo.Moniker linking_moniker);
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
