package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Storage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public interface Storage
	extends StorageOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity, org.gnu.bonobo.Unknown
{
}
