package org.gnu.bonobo;

/**
 *	Generated from IDL definition of alias "ZoomLevelNameList"
 *	@author JacORB IDL compiler 
 */

public final class ZoomLevelNameListHolder
	implements org.omg.CORBA.portable.Streamable
{
	public java.lang.String[] value;

	public ZoomLevelNameListHolder ()
	{
	}
	public ZoomLevelNameListHolder (final java.lang.String[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ZoomLevelNameListHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ZoomLevelNameListHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ZoomLevelNameListHelper.write (out,value);
	}
}
