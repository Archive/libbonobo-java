package org.gnu.bonobo;


/**
 *	Generated from IDL interface "Stream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class StreamHelper
{
	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Stream s)
	{
			any.insert_Object(s);
	}
	public static org.gnu.bonobo.Stream extract(final org.omg.CORBA.Any any)
	{
		return narrow(any.extract_Object()) ;
	}
	public static org.omg.CORBA.TypeCode type()
	{
		return org.omg.CORBA.ORB.init().create_interface_tc("IDL:Bonobo/Stream:1.0", "Stream");
	}
	public static String id()
	{
		return "IDL:Bonobo/Stream:1.0";
	}
	public static Stream read(final org.omg.CORBA.portable.InputStream in)
	{
		return narrow(in.read_Object());
	}
	public static void write(final org.omg.CORBA.portable.OutputStream _out, final org.gnu.bonobo.Stream s)
	{
		_out.write_Object(s);
	}
	public static org.gnu.bonobo.Stream narrow(final java.lang.Object obj)
	{
		if (obj instanceof org.gnu.bonobo.Stream)
		{
			return (org.gnu.bonobo.Stream)obj;
		}
		else if (obj instanceof org.omg.CORBA.Object)
		{
			return narrow((org.omg.CORBA.Object)obj);
		}
		throw new org.omg.CORBA.BAD_PARAM("Failed to narrow in helper");
	}
	public static org.gnu.bonobo.Stream narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
			return null;
		try
		{
			return (org.gnu.bonobo.Stream)obj;
		}
		catch (ClassCastException c)
		{
			if (obj._is_a("IDL:Bonobo/Stream:1.0"))
			{
				org.gnu.bonobo._StreamStub stub;
				stub = new org.gnu.bonobo._StreamStub();
				stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
				return stub;
			}
		}
		throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
	}
	public static org.gnu.bonobo.Stream unchecked_narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
			return null;
		try
		{
			return (org.gnu.bonobo.Stream)obj;
		}
		catch (ClassCastException c)
		{
				org.gnu.bonobo._StreamStub stub;
				stub = new org.gnu.bonobo._StreamStub();
				stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
				return stub;
		}
	}
}
