package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ZoomableFrame"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public interface ZoomableFrame
	extends ZoomableFrameOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity, org.gnu.bonobo.Unknown
{
}
