package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "PrintDimensions"
 *	@author JacORB IDL compiler 
 */

public final class PrintDimensions
	implements org.omg.CORBA.portable.IDLEntity
{
	public PrintDimensions(){}
	public double width;
	public double height;
	public PrintDimensions(double width, double height)
	{
		this.width = width;
		this.height = height;
	}
}
