package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ResolveOptions"
 *	@author JacORB IDL compiler 
 */

public final class ResolveOptions
	implements org.omg.CORBA.portable.IDLEntity
{
	public ResolveOptions(){}
	public int flags;
	public int timeout;
	public ResolveOptions(int flags, int timeout)
	{
		this.flags = flags;
		this.timeout = timeout;
	}
}
