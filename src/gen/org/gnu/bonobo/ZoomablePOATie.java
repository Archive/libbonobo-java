package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Zoomable"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class ZoomablePOATie
	extends ZoomablePOA
{
	private ZoomableOperations _delegate;

	private POA _poa;
	public ZoomablePOATie(ZoomableOperations delegate)
	{
		_delegate = delegate;
	}
	public ZoomablePOATie(ZoomableOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Zoomable _this()
	{
		return org.gnu.bonobo.ZoomableHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Zoomable _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.ZoomableHelper.narrow(_this_object(orb));
	}
	public ZoomableOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(ZoomableOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public float[] preferredLevels()
	{
		return _delegate.preferredLevels();
	}

	public void zoomFit()
	{
_delegate.zoomFit();
	}

	public boolean hasMaxLevel()
	{
		return _delegate.hasMaxLevel();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public float level()
	{
		return _delegate.level();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void setLevel(float zoom_level)
	{
_delegate.setLevel(zoom_level);
	}

	public float minLevel()
	{
		return _delegate.minLevel();
	}

	public boolean isContinuous()
	{
		return _delegate.isContinuous();
	}

	public void zoomOut()
	{
_delegate.zoomOut();
	}

	public void setFrame(org.gnu.bonobo.ZoomableFrame zoomable_frame)
	{
_delegate.setFrame(zoomable_frame);
	}

	public void unref()
	{
_delegate.unref();
	}

	public void ref()
	{
_delegate.ref();
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public void zoomIn()
	{
_delegate.zoomIn();
	}

	public void zoomDefault()
	{
_delegate.zoomDefault();
	}

	public java.lang.String[] preferredLevelNames()
	{
		return _delegate.preferredLevelNames();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public boolean hasMinLevel()
	{
		return _delegate.hasMinLevel();
	}

	public float maxLevel()
	{
		return _delegate.maxLevel();
	}

}
