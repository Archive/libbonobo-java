package org.gnu.bonobo;

/**
 *	Generated from IDL interface "UIComponent"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class UIComponentHolder	implements org.omg.CORBA.portable.Streamable{
	 public UIComponent value;
	public UIComponentHolder()
	{
	}
	public UIComponentHolder (final UIComponent initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return UIComponentHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = UIComponentHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		UIComponentHelper.write (_out,value);
	}
}
