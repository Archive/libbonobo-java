package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "StorageType"
 *	@author JacORB IDL compiler 
 */

public final class StorageType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _STORAGE_TYPE_REGULAR = 0;
	public static final StorageType STORAGE_TYPE_REGULAR = new StorageType(_STORAGE_TYPE_REGULAR);
	public static final int _STORAGE_TYPE_DIRECTORY = 1;
	public static final StorageType STORAGE_TYPE_DIRECTORY = new StorageType(_STORAGE_TYPE_DIRECTORY);
	public int value()
	{
		return value;
	}
	public static StorageType from_int(int value)
	{
		switch (value) {
			case _STORAGE_TYPE_REGULAR: return STORAGE_TYPE_REGULAR;
			case _STORAGE_TYPE_DIRECTORY: return STORAGE_TYPE_DIRECTORY;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected StorageType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
