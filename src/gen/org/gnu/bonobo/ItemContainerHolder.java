package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ItemContainer"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class ItemContainerHolder	implements org.omg.CORBA.portable.Streamable{
	 public ItemContainer value;
	public ItemContainerHolder()
	{
	}
	public ItemContainerHolder (final ItemContainer initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return ItemContainerHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ItemContainerHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		ItemContainerHelper.write (_out,value);
	}
}
