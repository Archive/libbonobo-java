package org.gnu.bonobo;

/**
 *	Generated from IDL interface "PersistStream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class PersistStreamHolder	implements org.omg.CORBA.portable.Streamable{
	 public PersistStream value;
	public PersistStreamHolder()
	{
	}
	public PersistStreamHolder (final PersistStream initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return PersistStreamHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = PersistStreamHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		PersistStreamHelper.write (_out,value);
	}
}
