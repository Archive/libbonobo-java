package org.gnu.bonobo;

/**
 *	Generated from IDL interface "EventSource"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class EventSourceHolder	implements org.omg.CORBA.portable.Streamable{
	 public EventSource value;
	public EventSourceHolder()
	{
	}
	public EventSourceHolder (final EventSource initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return EventSourceHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = EventSourceHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		EventSourceHelper.write (_out,value);
	}
}
