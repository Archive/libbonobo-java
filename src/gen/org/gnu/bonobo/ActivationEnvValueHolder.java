package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ActivationEnvValue"
 *	@author JacORB IDL compiler 
 */

public final class ActivationEnvValueHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ActivationEnvValue value;

	public ActivationEnvValueHolder ()
	{
	}
	public ActivationEnvValueHolder(final org.gnu.bonobo.ActivationEnvValue initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.ActivationEnvValueHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.ActivationEnvValueHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.ActivationEnvValueHelper.write(_out, value);
	}
}
