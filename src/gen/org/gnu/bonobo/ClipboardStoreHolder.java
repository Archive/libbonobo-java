package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ClipboardStore"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class ClipboardStoreHolder	implements org.omg.CORBA.portable.Streamable{
	 public ClipboardStore value;
	public ClipboardStoreHolder()
	{
	}
	public ClipboardStoreHolder (final ClipboardStore initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return ClipboardStoreHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ClipboardStoreHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		ClipboardStoreHelper.write (_out,value);
	}
}
