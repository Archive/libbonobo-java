package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ServerInfo"
 *	@author JacORB IDL compiler 
 */

public final class ServerInfoHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ServerInfo value;

	public ServerInfoHolder ()
	{
	}
	public ServerInfoHolder(final org.gnu.bonobo.ServerInfo initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.ServerInfoHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.ServerInfoHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.ServerInfoHelper.write(_out, value);
	}
}
