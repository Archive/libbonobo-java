package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "ActivationPropertyType"
 *	@author JacORB IDL compiler 
 */

public final class ActivationPropertyTypeHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.ActivationPropertyTypeHelper.id(),"ActivationPropertyType",new String[]{"ACTIVATION_P_STRING","ACTIVATION_P_NUMBER","ACTIVATION_P_BOOLEAN","ACTIVATION_P_STRINGV"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.ActivationPropertyType s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.ActivationPropertyType extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/ActivationPropertyType:1.0";
	}
	public static ActivationPropertyType read (final org.omg.CORBA.portable.InputStream in)
	{
		return ActivationPropertyType.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final ActivationPropertyType s)
	{
		out.write_long(s.value());
	}
}
