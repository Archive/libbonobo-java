package org.gnu.bonobo;


/**
 *	Generated from IDL definition of struct "ActivationResult"
 *	@author JacORB IDL compiler 
 */

public final class ActivationResultHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.ActivationResultHelper.id(),"ActivationResult",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("aid", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("res", org.gnu.bonobo.ActivationResultDataHelper.type(), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.ActivationResult s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.ActivationResult extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/ActivationResult:1.0";
	}
	public static org.gnu.bonobo.ActivationResult read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.ActivationResult result = new org.gnu.bonobo.ActivationResult();
		result.aid=in.read_string();
		result.res=org.gnu.bonobo.ActivationResultDataHelper.read(in);
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.ActivationResult s)
	{
		out.write_string(s.aid);
		org.gnu.bonobo.ActivationResultDataHelper.write(out,s.res);
	}
}
