package org.gnu.bonobo;


/**
 *	Generated from IDL definition of exception "BadArg"
 *	@author JacORB IDL compiler 
 */

public final class BadArgHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_exception_tc(org.gnu.bonobo.BadArgHelper.id(),"BadArg",new org.omg.CORBA.StructMember[0]);
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.BadArg s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.BadArg extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/BadArg:1.0";
	}
	public static org.gnu.bonobo.BadArg read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.BadArg result = new org.gnu.bonobo.BadArg();
		if (!in.read_string().equals(id())) throw new org.omg.CORBA.MARSHAL("wrong id");
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.BadArg s)
	{
		out.write_string(id());
	}
}
