package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Persist"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class PersistPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.PersistOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "ref", new java.lang.Integer(0));
		m_opsHash.put ( "getIId", new java.lang.Integer(1));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(2));
		m_opsHash.put ( "unref", new java.lang.Integer(3));
		m_opsHash.put ( "getContentTypes", new java.lang.Integer(4));
		m_opsHash.put ( "isDirty", new java.lang.Integer(5));
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(6));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(7));
	}
	private String[] ids = {"IDL:Bonobo/Persist:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.Persist _this()
	{
		return org.gnu.bonobo.PersistHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Persist _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.PersistHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 1: // getIId
			{
				_out = handler.createReply();
				_out.write_string(getIId());
				break;
			}
			case 2: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 3: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 4: // getContentTypes
			{
				_out = handler.createReply();
				org.gnu.bonobo.PersistPackage.ContentTypeListHelper.write(_out,getContentTypes());
				break;
			}
			case 5: // isDirty
			{
				_out = handler.createReply();
				_out.write_boolean(isDirty());
				break;
			}
			case 6: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 7: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
