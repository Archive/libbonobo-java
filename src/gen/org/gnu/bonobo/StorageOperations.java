package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Storage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface StorageOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	int READ = 1;
	int WRITE = 2;
	int CREATE = 4;
	int FAILIFEXIST = 8;
	int COMPRESSED = 16;
	int TRANSACTED = 32;
	/* operations  */
	org.gnu.bonobo.StorageInfo getInfo(java.lang.String path, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError;
	void setInfo(java.lang.String path, org.gnu.bonobo.StorageInfo info, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError;
	org.gnu.bonobo.Stream openStream(java.lang.String path, int mode) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotStream,org.gnu.bonobo.StoragePackage.IOError;
	org.gnu.bonobo.Storage openStorage(java.lang.String path, int mode) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotStorage,org.gnu.bonobo.StoragePackage.IOError;
	void copyTo(org.gnu.bonobo.Storage target) throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError;
	org.gnu.bonobo.StorageInfo[] listContents(java.lang.String path, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.NotStorage,org.gnu.bonobo.StoragePackage.IOError;
	void erase(java.lang.String path) throws org.gnu.bonobo.StoragePackage.NotEmpty,org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError;
	void rename(java.lang.String path_name, java.lang.String new_path_name) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError;
	void commit() throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError;
	void revert() throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError;
	void unImplemented1();
	void unImplemented2();
}
