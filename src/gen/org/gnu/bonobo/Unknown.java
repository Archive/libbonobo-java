package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Unknown"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public interface Unknown
	extends UnknownOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
