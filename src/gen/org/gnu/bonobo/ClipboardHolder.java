package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Clipboard"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class ClipboardHolder	implements org.omg.CORBA.portable.Streamable{
	 public Clipboard value;
	public ClipboardHolder()
	{
	}
	public ClipboardHolder (final Clipboard initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return ClipboardHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ClipboardHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		ClipboardHelper.write (_out,value);
	}
}
