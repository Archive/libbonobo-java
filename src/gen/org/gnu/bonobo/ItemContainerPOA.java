package org.gnu.bonobo;

/**
 *	Generated from IDL interface "ItemContainer"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class ItemContainerPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.ItemContainerOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(0));
		m_opsHash.put ( "ref", new java.lang.Integer(1));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(2));
		m_opsHash.put ( "unref", new java.lang.Integer(3));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(4));
		m_opsHash.put ( "getObjectByName", new java.lang.Integer(5));
		m_opsHash.put ( "enumObjects", new java.lang.Integer(6));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(7));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(8));
	}
	private String[] ids = {"IDL:Bonobo/ItemContainer:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.ItemContainer _this()
	{
		return org.gnu.bonobo.ItemContainerHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.ItemContainer _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.ItemContainerHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 1: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 2: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 3: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 4: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 5: // getObjectByName
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				boolean _arg1=_input.read_boolean();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,getObjectByName(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.ItemContainerPackage.NotFound _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.ItemContainerPackage.NotFoundHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.ItemContainerPackage.SyntaxError _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.ItemContainerPackage.SyntaxErrorHelper.write(_out, _ex1);
			}
				break;
			}
			case 6: // enumObjects
			{
				_out = handler.createReply();
				org.gnu.bonobo.ItemContainerPackage.ObjectNamesHelper.write(_out,enumObjects());
				break;
			}
			case 7: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 8: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
