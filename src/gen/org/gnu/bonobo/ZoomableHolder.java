package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Zoomable"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class ZoomableHolder	implements org.omg.CORBA.portable.Streamable{
	 public Zoomable value;
	public ZoomableHolder()
	{
	}
	public ZoomableHolder (final Zoomable initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return ZoomableHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ZoomableHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		ZoomableHelper.write (_out,value);
	}
}
