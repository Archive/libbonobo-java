package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Persist"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public interface Persist
	extends PersistOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity, org.gnu.bonobo.Unknown
{
}
