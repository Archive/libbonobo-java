package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Print"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface PrintOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	org.gnu.bonobo.Stream render(org.gnu.bonobo.PrintDimensions pd, org.gnu.bonobo.PrintScissor scissor);
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
}
