package org.gnu.bonobo;

/**
 *	Generated from IDL interface "UIContainer"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class UIContainerPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.UIContainerOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "freeze", new java.lang.Integer(0));
		m_opsHash.put ( "unImplemented", new java.lang.Integer(1));
		m_opsHash.put ( "exists", new java.lang.Integer(2));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(3));
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(4));
		m_opsHash.put ( "removeNode", new java.lang.Integer(5));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(6));
		m_opsHash.put ( "execVerb", new java.lang.Integer(7));
		m_opsHash.put ( "setNode", new java.lang.Integer(8));
		m_opsHash.put ( "deregisterComponent", new java.lang.Integer(9));
		m_opsHash.put ( "thaw", new java.lang.Integer(10));
		m_opsHash.put ( "unref", new java.lang.Integer(11));
		m_opsHash.put ( "ref", new java.lang.Integer(12));
		m_opsHash.put ( "uiEvent", new java.lang.Integer(13));
		m_opsHash.put ( "getAttr", new java.lang.Integer(14));
		m_opsHash.put ( "getObject", new java.lang.Integer(15));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(16));
		m_opsHash.put ( "setObject", new java.lang.Integer(17));
		m_opsHash.put ( "getNode", new java.lang.Integer(18));
		m_opsHash.put ( "setAttr", new java.lang.Integer(19));
		m_opsHash.put ( "registerComponent", new java.lang.Integer(20));
	}
	private String[] ids = {"IDL:Bonobo/UIContainer:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.UIContainer _this()
	{
		return org.gnu.bonobo.UIContainerHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.UIContainer _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.UIContainerHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // freeze
			{
				_out = handler.createReply();
				freeze();
				break;
			}
			case 1: // unImplemented
			{
				_out = handler.createReply();
				unImplemented();
				break;
			}
			case 2: // exists
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				_out.write_boolean(exists(_arg0));
				break;
			}
			case 3: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
			case 4: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 5: // removeNode
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				java.lang.String _arg1=_input.read_string();
				_out = handler.createReply();
				removeNode(_arg0,_arg1);
			}
			catch(org.gnu.bonobo.UIContainerPackage.InvalidPath _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.write(_out, _ex0);
			}
				break;
			}
			case 6: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 7: // execVerb
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				execVerb(_arg0);
			}
			catch(org.gnu.bonobo.UIContainerPackage.Insensitive _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.InsensitiveHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.UIContainerPackage.Unknown _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.UnknownHelper.write(_out, _ex1);
			}
				break;
			}
			case 8: // setNode
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				java.lang.String _arg1=_input.read_string();
				java.lang.String _arg2=_input.read_string();
				_out = handler.createReply();
				setNode(_arg0,_arg1,_arg2);
			}
			catch(org.gnu.bonobo.UIContainerPackage.InvalidPath _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.UIContainerPackage.MalformedXML _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.MalformedXMLHelper.write(_out, _ex1);
			}
				break;
			}
			case 9: // deregisterComponent
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				deregisterComponent(_arg0);
				break;
			}
			case 10: // thaw
			{
				_out = handler.createReply();
				thaw();
				break;
			}
			case 11: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 12: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 13: // uiEvent
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				org.gnu.bonobo.UIComponentPackage.EventType _arg1=org.gnu.bonobo.UIComponentPackage.EventTypeHelper.read(_input);
				java.lang.String _arg2=_input.read_string();
				_out = handler.createReply();
				uiEvent(_arg0,_arg1,_arg2);
			}
			catch(org.gnu.bonobo.UIContainerPackage.Insensitive _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.InsensitiveHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.UIContainerPackage.Unknown _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.UnknownHelper.write(_out, _ex1);
			}
				break;
			}
			case 14: // getAttr
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				java.lang.String _arg1=_input.read_string();
				_out = handler.createReply();
				_out.write_string(getAttr(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.UIContainerPackage.NonExistentAttr _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.NonExistentAttrHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.UIContainerPackage.InvalidPath _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.write(_out, _ex1);
			}
				break;
			}
			case 15: // getObject
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,getObject(_arg0));
			}
			catch(org.gnu.bonobo.UIContainerPackage.InvalidPath _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.write(_out, _ex0);
			}
				break;
			}
			case 16: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 17: // setObject
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				org.gnu.bonobo.Unknown _arg1=org.gnu.bonobo.UnknownHelper.read(_input);
				_out = handler.createReply();
				setObject(_arg0,_arg1);
			}
			catch(org.gnu.bonobo.UIContainerPackage.InvalidPath _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.write(_out, _ex0);
			}
				break;
			}
			case 18: // getNode
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				boolean _arg1=_input.read_boolean();
				_out = handler.createReply();
				_out.write_string(getNode(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.UIContainerPackage.InvalidPath _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.UIContainerPackage.InvalidPathHelper.write(_out, _ex0);
			}
				break;
			}
			case 19: // setAttr
			{
				java.lang.String _arg0=_input.read_string();
				java.lang.String _arg1=_input.read_string();
				java.lang.String _arg2=_input.read_string();
				java.lang.String _arg3=_input.read_string();
				_out = handler.createReply();
				setAttr(_arg0,_arg1,_arg2,_arg3);
				break;
			}
			case 20: // registerComponent
			{
				java.lang.String _arg0=_input.read_string();
				org.gnu.bonobo.UIComponent _arg1=org.gnu.bonobo.UIComponentHelper.read(_input);
				_out = handler.createReply();
				registerComponent(_arg0,_arg1);
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
