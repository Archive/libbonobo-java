package org.gnu.bonobo;

/**
 *	Generated from IDL definition of union "ActivationPropertyValue"
 *	@author JacORB IDL compiler 
 */

public final class ActivationPropertyValueHelper
{
	private static org.omg.CORBA.TypeCode _type;
	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.ActivationPropertyValue s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.ActivationPropertyValue extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/ActivationPropertyValue:1.0";
	}
	public static ActivationPropertyValue read (org.omg.CORBA.portable.InputStream in)
	{
		ActivationPropertyValue result = new ActivationPropertyValue ();
		org.gnu.bonobo.ActivationPropertyType disc = org.gnu.bonobo.ActivationPropertyType.from_int(in.read_long());
		switch (disc.value ())
		{
			case org.gnu.bonobo.ActivationPropertyType._ACTIVATION_P_STRING:
			{
				java.lang.String _var;
				_var=in.read_string();
				result.value_string (_var);
				break;
			}
			case org.gnu.bonobo.ActivationPropertyType._ACTIVATION_P_NUMBER:
			{
				double _var;
				_var=in.read_double();
				result.value_number (_var);
				break;
			}
			case org.gnu.bonobo.ActivationPropertyType._ACTIVATION_P_BOOLEAN:
			{
				boolean _var;
				_var=in.read_boolean();
				result.value_boolean (_var);
				break;
			}
			case org.gnu.bonobo.ActivationPropertyType._ACTIVATION_P_STRINGV:
			{
				java.lang.String[] _var;
				_var = org.gnu.bonobo.StringListHelper.read(in);
				result.value_stringv (_var);
				break;
			}
		}
		return result;
	}
	public static void write (org.omg.CORBA.portable.OutputStream out, ActivationPropertyValue s)
	{
		out.write_long (s.discriminator().value ());
		switch (s.discriminator().value ())
		{
			case org.gnu.bonobo.ActivationPropertyType._ACTIVATION_P_STRING:
			{
				out.write_string(s.value_string ());
				break;
			}
			case org.gnu.bonobo.ActivationPropertyType._ACTIVATION_P_NUMBER:
			{
				out.write_double(s.value_number ());
				break;
			}
			case org.gnu.bonobo.ActivationPropertyType._ACTIVATION_P_BOOLEAN:
			{
				out.write_boolean(s.value_boolean ());
				break;
			}
			case org.gnu.bonobo.ActivationPropertyType._ACTIVATION_P_STRINGV:
			{
				org.gnu.bonobo.StringListHelper.write(out,s.value_stringv ());
				break;
			}
		}
	}
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			org.omg.CORBA.UnionMember[] members = new org.omg.CORBA.UnionMember[4];
			org.omg.CORBA.Any label_any;
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.ActivationPropertyTypeHelper.insert(label_any, org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_STRING);
			members[3] = new org.omg.CORBA.UnionMember ("value_string", label_any, org.omg.CORBA.ORB.init().create_string_tc(0),null);
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.ActivationPropertyTypeHelper.insert(label_any, org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_NUMBER);
			members[2] = new org.omg.CORBA.UnionMember ("value_number", label_any, org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)),null);
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.ActivationPropertyTypeHelper.insert(label_any, org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_BOOLEAN);
			members[1] = new org.omg.CORBA.UnionMember ("value_boolean", label_any, org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(8)),null);
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.ActivationPropertyTypeHelper.insert(label_any, org.gnu.bonobo.ActivationPropertyType.ACTIVATION_P_STRINGV);
			members[0] = new org.omg.CORBA.UnionMember ("value_stringv", label_any, org.omg.CORBA.ORB.init().create_sequence_tc(0, org.omg.CORBA.ORB.init().create_string_tc(0)),null);
			 _type = org.omg.CORBA.ORB.init().create_union_tc(id(),"ActivationPropertyValue",org.gnu.bonobo.ActivationPropertyTypeHelper.type(), members);
		}
		return _type;
	}
}
