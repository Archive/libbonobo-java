package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "NotSupported"
 *	@author JacORB IDL compiler 
 */

public final class NotSupportedHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.NotSupported value;

	public NotSupportedHolder ()
	{
	}
	public NotSupportedHolder(final org.gnu.bonobo.NotSupported initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.NotSupportedHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.NotSupportedHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.NotSupportedHelper.write(_out, value);
	}
}
