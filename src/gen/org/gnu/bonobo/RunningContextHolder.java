package org.gnu.bonobo;

/**
 *	Generated from IDL interface "RunningContext"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class RunningContextHolder	implements org.omg.CORBA.portable.Streamable{
	 public RunningContext value;
	public RunningContextHolder()
	{
	}
	public RunningContextHolder (final RunningContext initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return RunningContextHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = RunningContextHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		RunningContextHelper.write (_out,value);
	}
}
