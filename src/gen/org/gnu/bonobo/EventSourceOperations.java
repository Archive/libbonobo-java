package org.gnu.bonobo;

/**
 *	Generated from IDL interface "EventSource"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface EventSourceOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	void addListener(org.gnu.bonobo.Listener l);
	void addListenerWithMask(org.gnu.bonobo.Listener l, java.lang.String event_mask);
	void removeListener(org.gnu.bonobo.Listener l) throws org.gnu.bonobo.EventSourcePackage.UnknownListener;
	void unImplemented();
	void unImplemented2();
}
