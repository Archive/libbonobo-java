package org.gnu.bonobo;


/**
 *	Generated from IDL definition of struct "PrintDimensions"
 *	@author JacORB IDL compiler 
 */

public final class PrintDimensionsHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.PrintDimensionsHelper.id(),"PrintDimensions",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("width", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("height", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.PrintDimensions s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.PrintDimensions extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/PrintDimensions:1.0";
	}
	public static org.gnu.bonobo.PrintDimensions read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.PrintDimensions result = new org.gnu.bonobo.PrintDimensions();
		result.width=in.read_double();
		result.height=in.read_double();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.PrintDimensions s)
	{
		out.write_double(s.width);
		out.write_double(s.height);
	}
}
