package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "NotSupported"
 *	@author JacORB IDL compiler 
 */

public final class NotSupported
	extends org.omg.CORBA.UserException
{
	public NotSupported()
	{
		super(org.gnu.bonobo.NotSupportedHelper.id());
	}

	public NotSupported(String value)
	{
		super(value);
	}
}
