package org.gnu.bonobo.PersistFilePackage;

/**
 *	Generated from IDL definition of exception "NoCurrentName"
 *	@author JacORB IDL compiler 
 */

public final class NoCurrentNameHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.PersistFilePackage.NoCurrentName value;

	public NoCurrentNameHolder ()
	{
	}
	public NoCurrentNameHolder(final org.gnu.bonobo.PersistFilePackage.NoCurrentName initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.PersistFilePackage.NoCurrentNameHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.PersistFilePackage.NoCurrentNameHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.PersistFilePackage.NoCurrentNameHelper.write(_out, value);
	}
}
