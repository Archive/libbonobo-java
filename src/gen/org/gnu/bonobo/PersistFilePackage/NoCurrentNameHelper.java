package org.gnu.bonobo.PersistFilePackage;


/**
 *	Generated from IDL definition of exception "NoCurrentName"
 *	@author JacORB IDL compiler 
 */

public final class NoCurrentNameHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_exception_tc(org.gnu.bonobo.PersistFilePackage.NoCurrentNameHelper.id(),"NoCurrentName",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("extension", org.omg.CORBA.ORB.init().create_string_tc(0), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.PersistFilePackage.NoCurrentName s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.PersistFilePackage.NoCurrentName extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/PersistFile/NoCurrentName:1.0";
	}
	public static org.gnu.bonobo.PersistFilePackage.NoCurrentName read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.PersistFilePackage.NoCurrentName result = new org.gnu.bonobo.PersistFilePackage.NoCurrentName();
		if (!in.read_string().equals(id())) throw new org.omg.CORBA.MARSHAL("wrong id");
		result.extension=in.read_string();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.PersistFilePackage.NoCurrentName s)
	{
		out.write_string(id());
		out.write_string(s.extension);
	}
}
