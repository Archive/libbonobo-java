package org.gnu.bonobo.PersistFilePackage;

/**
 *	Generated from IDL definition of exception "NoCurrentName"
 *	@author JacORB IDL compiler 
 */

public final class NoCurrentName
	extends org.omg.CORBA.UserException
{
	public NoCurrentName()
	{
		super(org.gnu.bonobo.PersistFilePackage.NoCurrentNameHelper.id());
	}

	public java.lang.String extension = "";
	public NoCurrentName(java.lang.String _reason,java.lang.String extension)
	{
		super(org.gnu.bonobo.PersistFilePackage.NoCurrentNameHelper.id()+ " " + _reason);
		this.extension = extension;
	}
	public NoCurrentName(java.lang.String extension)
	{
		super(org.gnu.bonobo.PersistFilePackage.NoCurrentNameHelper.id());
		this.extension = extension;
	}
}
