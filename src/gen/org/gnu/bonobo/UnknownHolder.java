package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Unknown"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class UnknownHolder	implements org.omg.CORBA.portable.Streamable{
	 public Unknown value;
	public UnknownHolder()
	{
	}
	public UnknownHolder (final Unknown initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return UnknownHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = UnknownHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		UnknownHelper.write (_out,value);
	}
}
