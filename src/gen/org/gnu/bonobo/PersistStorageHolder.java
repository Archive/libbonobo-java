package org.gnu.bonobo;

/**
 *	Generated from IDL interface "PersistStorage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class PersistStorageHolder	implements org.omg.CORBA.portable.Streamable{
	 public PersistStorage value;
	public PersistStorageHolder()
	{
	}
	public PersistStorageHolder (final PersistStorage initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return PersistStorageHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = PersistStorageHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		PersistStorageHelper.write (_out,value);
	}
}
