package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "RegistrationResult"
 *	@author JacORB IDL compiler 
 */

public final class RegistrationResultHolder
	implements org.omg.CORBA.portable.Streamable
{
	public RegistrationResult value;

	public RegistrationResultHolder ()
	{
	}
	public RegistrationResultHolder (final RegistrationResult initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return RegistrationResultHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = RegistrationResultHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		RegistrationResultHelper.write (out,value);
	}
}
