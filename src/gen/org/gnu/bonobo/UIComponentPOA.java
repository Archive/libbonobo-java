package org.gnu.bonobo;

/**
 *	Generated from IDL interface "UIComponent"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class UIComponentPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.UIComponentOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(0));
		m_opsHash.put ( "unImplemented", new java.lang.Integer(1));
		m_opsHash.put ( "_get_name", new java.lang.Integer(2));
		m_opsHash.put ( "ref", new java.lang.Integer(3));
		m_opsHash.put ( "describeVerbs", new java.lang.Integer(4));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(5));
		m_opsHash.put ( "unref", new java.lang.Integer(6));
		m_opsHash.put ( "unsetContainer", new java.lang.Integer(7));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(8));
		m_opsHash.put ( "execVerb", new java.lang.Integer(9));
		m_opsHash.put ( "setContainer", new java.lang.Integer(10));
		m_opsHash.put ( "uiEvent", new java.lang.Integer(11));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(12));
	}
	private String[] ids = {"IDL:Bonobo/UIComponent:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.UIComponent _this()
	{
		return org.gnu.bonobo.UIComponentHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.UIComponent _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.UIComponentHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 1: // unImplemented
			{
				_out = handler.createReply();
				unImplemented();
				break;
			}
			case 2: // _get_name
			{
			_out = handler.createReply();
			_out.write_string(name());
				break;
			}
			case 3: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 4: // describeVerbs
			{
				_out = handler.createReply();
				_out.write_string(describeVerbs());
				break;
			}
			case 5: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 6: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 7: // unsetContainer
			{
				_out = handler.createReply();
				unsetContainer();
				break;
			}
			case 8: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 9: // execVerb
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				execVerb(_arg0);
				break;
			}
			case 10: // setContainer
			{
				org.gnu.bonobo.UIContainer _arg0=org.gnu.bonobo.UIContainerHelper.read(_input);
				_out = handler.createReply();
				setContainer(_arg0);
				break;
			}
			case 11: // uiEvent
			{
				java.lang.String _arg0=_input.read_string();
				org.gnu.bonobo.UIComponentPackage.EventType _arg1=org.gnu.bonobo.UIComponentPackage.EventTypeHelper.read(_input);
				java.lang.String _arg2=_input.read_string();
				_out = handler.createReply();
				uiEvent(_arg0,_arg1,_arg2);
				break;
			}
			case 12: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
