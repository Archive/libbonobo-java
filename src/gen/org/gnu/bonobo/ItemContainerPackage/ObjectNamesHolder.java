package org.gnu.bonobo.ItemContainerPackage;

/**
 *	Generated from IDL definition of alias "ObjectNames"
 *	@author JacORB IDL compiler 
 */

public final class ObjectNamesHolder
	implements org.omg.CORBA.portable.Streamable
{
	public java.lang.String[] value;

	public ObjectNamesHolder ()
	{
	}
	public ObjectNamesHolder (final java.lang.String[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ObjectNamesHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ObjectNamesHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ObjectNamesHelper.write (out,value);
	}
}
