package org.gnu.bonobo.ItemContainerPackage;

/**
 *	Generated from IDL definition of exception "SyntaxError"
 *	@author JacORB IDL compiler 
 */

public final class SyntaxErrorHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ItemContainerPackage.SyntaxError value;

	public SyntaxErrorHolder ()
	{
	}
	public SyntaxErrorHolder(final org.gnu.bonobo.ItemContainerPackage.SyntaxError initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.ItemContainerPackage.SyntaxErrorHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.ItemContainerPackage.SyntaxErrorHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.ItemContainerPackage.SyntaxErrorHelper.write(_out, value);
	}
}
