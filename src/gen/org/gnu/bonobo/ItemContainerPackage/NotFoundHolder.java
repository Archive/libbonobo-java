package org.gnu.bonobo.ItemContainerPackage;

/**
 *	Generated from IDL definition of exception "NotFound"
 *	@author JacORB IDL compiler 
 */

public final class NotFoundHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ItemContainerPackage.NotFound value;

	public NotFoundHolder ()
	{
	}
	public NotFoundHolder(final org.gnu.bonobo.ItemContainerPackage.NotFound initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.ItemContainerPackage.NotFoundHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.ItemContainerPackage.NotFoundHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.ItemContainerPackage.NotFoundHelper.write(_out, value);
	}
}
