package org.gnu.bonobo.ItemContainerPackage;

/**
 *	Generated from IDL definition of exception "SyntaxError"
 *	@author JacORB IDL compiler 
 */

public final class SyntaxError
	extends org.omg.CORBA.UserException
{
	public SyntaxError()
	{
		super(org.gnu.bonobo.ItemContainerPackage.SyntaxErrorHelper.id());
	}

	public SyntaxError(String value)
	{
		super(value);
	}
}
