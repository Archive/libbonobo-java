package org.gnu.bonobo.ItemContainerPackage;

/**
 *	Generated from IDL definition of exception "NotFound"
 *	@author JacORB IDL compiler 
 */

public final class NotFound
	extends org.omg.CORBA.UserException
{
	public NotFound()
	{
		super(org.gnu.bonobo.ItemContainerPackage.NotFoundHelper.id());
	}

	public NotFound(String value)
	{
		super(value);
	}
}
