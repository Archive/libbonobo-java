package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "ActivationPropertyType"
 *	@author JacORB IDL compiler 
 */

public final class ActivationPropertyType
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _ACTIVATION_P_STRING = 0;
	public static final ActivationPropertyType ACTIVATION_P_STRING = new ActivationPropertyType(_ACTIVATION_P_STRING);
	public static final int _ACTIVATION_P_NUMBER = 1;
	public static final ActivationPropertyType ACTIVATION_P_NUMBER = new ActivationPropertyType(_ACTIVATION_P_NUMBER);
	public static final int _ACTIVATION_P_BOOLEAN = 2;
	public static final ActivationPropertyType ACTIVATION_P_BOOLEAN = new ActivationPropertyType(_ACTIVATION_P_BOOLEAN);
	public static final int _ACTIVATION_P_STRINGV = 3;
	public static final ActivationPropertyType ACTIVATION_P_STRINGV = new ActivationPropertyType(_ACTIVATION_P_STRINGV);
	public int value()
	{
		return value;
	}
	public static ActivationPropertyType from_int(int value)
	{
		switch (value) {
			case _ACTIVATION_P_STRING: return ACTIVATION_P_STRING;
			case _ACTIVATION_P_NUMBER: return ACTIVATION_P_NUMBER;
			case _ACTIVATION_P_BOOLEAN: return ACTIVATION_P_BOOLEAN;
			case _ACTIVATION_P_STRINGV: return ACTIVATION_P_STRINGV;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected ActivationPropertyType(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
