package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "MonikerExtender"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class MonikerExtenderPOATie
	extends MonikerExtenderPOA
{
	private MonikerExtenderOperations _delegate;

	private POA _poa;
	public MonikerExtenderPOATie(MonikerExtenderOperations delegate)
	{
		_delegate = delegate;
	}
	public MonikerExtenderPOATie(MonikerExtenderOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.MonikerExtender _this()
	{
		return org.gnu.bonobo.MonikerExtenderHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.MonikerExtender _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.MonikerExtenderHelper.narrow(_this_object(orb));
	}
	public MonikerExtenderOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(MonikerExtenderOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void ref()
	{
_delegate.ref();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public org.gnu.bonobo.Unknown resolve(org.gnu.bonobo.Moniker m, org.gnu.bonobo.ResolveOptions options, java.lang.String name, java.lang.String requestedInterface) throws org.gnu.bonobo.MonikerPackage.TimeOut,org.gnu.bonobo.MonikerPackage.InterfaceNotFound,org.gnu.bonobo.GeneralError
	{
		return _delegate.resolve(m,options,name,requestedInterface);
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
