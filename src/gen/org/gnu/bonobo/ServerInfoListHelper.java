package org.gnu.bonobo;

/**
 *	Generated from IDL definition of alias "ServerInfoList"
 *	@author JacORB IDL compiler 
 */

public final class ServerInfoListHelper
{
	private static org.omg.CORBA.TypeCode _type = null;

	public static void insert (org.omg.CORBA.Any any, org.gnu.bonobo.ServerInfo[] s)
	{
		any.type (type ());
		write (any.create_output_stream (), s);
	}

	public static org.gnu.bonobo.ServerInfo[] extract (final org.omg.CORBA.Any any)
	{
		return read (any.create_input_stream ());
	}

	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_alias_tc(org.gnu.bonobo.ServerInfoListHelper.id(), "ServerInfoList",org.omg.CORBA.ORB.init().create_sequence_tc(0, org.gnu.bonobo.ServerInfoHelper.type()));
		}
		return _type;
	}

	public static String id()
	{
		return "IDL:Bonobo/ServerInfoList:1.0";
	}
	public static org.gnu.bonobo.ServerInfo[] read (final org.omg.CORBA.portable.InputStream _in)
	{
		org.gnu.bonobo.ServerInfo[] _result;
		int _l_result3 = _in.read_long();
		_result = new org.gnu.bonobo.ServerInfo[_l_result3];
		for (int i=0;i<_result.length;i++)
		{
			_result[i]=org.gnu.bonobo.ServerInfoHelper.read(_in);
		}

		return _result;
	}

	public static void write (final org.omg.CORBA.portable.OutputStream _out, org.gnu.bonobo.ServerInfo[] _s)
	{
		
		_out.write_long(_s.length);
		for (int i=0; i<_s.length;i++)
		{
			org.gnu.bonobo.ServerInfoHelper.write(_out,_s[i]);
		}

	}
}
