package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "PrintDimensions"
 *	@author JacORB IDL compiler 
 */

public final class PrintDimensionsHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.PrintDimensions value;

	public PrintDimensionsHolder ()
	{
	}
	public PrintDimensionsHolder(final org.gnu.bonobo.PrintDimensions initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.PrintDimensionsHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.PrintDimensionsHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.PrintDimensionsHelper.write(_out, value);
	}
}
