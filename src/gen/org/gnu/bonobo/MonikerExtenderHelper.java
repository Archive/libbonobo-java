package org.gnu.bonobo;


/**
 *	Generated from IDL interface "MonikerExtender"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class MonikerExtenderHelper
{
	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.MonikerExtender s)
	{
			any.insert_Object(s);
	}
	public static org.gnu.bonobo.MonikerExtender extract(final org.omg.CORBA.Any any)
	{
		return narrow(any.extract_Object()) ;
	}
	public static org.omg.CORBA.TypeCode type()
	{
		return org.omg.CORBA.ORB.init().create_interface_tc("IDL:Bonobo/MonikerExtender:1.0", "MonikerExtender");
	}
	public static String id()
	{
		return "IDL:Bonobo/MonikerExtender:1.0";
	}
	public static MonikerExtender read(final org.omg.CORBA.portable.InputStream in)
	{
		return narrow(in.read_Object());
	}
	public static void write(final org.omg.CORBA.portable.OutputStream _out, final org.gnu.bonobo.MonikerExtender s)
	{
		_out.write_Object(s);
	}
	public static org.gnu.bonobo.MonikerExtender narrow(final java.lang.Object obj)
	{
		if (obj instanceof org.gnu.bonobo.MonikerExtender)
		{
			return (org.gnu.bonobo.MonikerExtender)obj;
		}
		else if (obj instanceof org.omg.CORBA.Object)
		{
			return narrow((org.omg.CORBA.Object)obj);
		}
		throw new org.omg.CORBA.BAD_PARAM("Failed to narrow in helper");
	}
	public static org.gnu.bonobo.MonikerExtender narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
			return null;
		try
		{
			return (org.gnu.bonobo.MonikerExtender)obj;
		}
		catch (ClassCastException c)
		{
			if (obj._is_a("IDL:Bonobo/MonikerExtender:1.0"))
			{
				org.gnu.bonobo._MonikerExtenderStub stub;
				stub = new org.gnu.bonobo._MonikerExtenderStub();
				stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
				return stub;
			}
		}
		throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
	}
	public static org.gnu.bonobo.MonikerExtender unchecked_narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
			return null;
		try
		{
			return (org.gnu.bonobo.MonikerExtender)obj;
		}
		catch (ClassCastException c)
		{
				org.gnu.bonobo._MonikerExtenderStub stub;
				stub = new org.gnu.bonobo._MonikerExtenderStub();
				stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
				return stub;
		}
	}
}
