package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "ActivationPropertyType"
 *	@author JacORB IDL compiler 
 */

public final class ActivationPropertyTypeHolder
	implements org.omg.CORBA.portable.Streamable
{
	public ActivationPropertyType value;

	public ActivationPropertyTypeHolder ()
	{
	}
	public ActivationPropertyTypeHolder (final ActivationPropertyType initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ActivationPropertyTypeHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ActivationPropertyTypeHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ActivationPropertyTypeHelper.write (out,value);
	}
}
