package org.gnu.bonobo;

/**
 *	Generated from IDL definition of union "ActivationResultData"
 *	@author JacORB IDL compiler 
 */

public final class ActivationResultDataHelper
{
	private static org.omg.CORBA.TypeCode _type;
	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.ActivationResultData s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.ActivationResultData extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/ActivationResultData:1.0";
	}
	public static ActivationResultData read (org.omg.CORBA.portable.InputStream in)
	{
		ActivationResultData result = new ActivationResultData ();
		org.gnu.bonobo.ActivationResultType disc = org.gnu.bonobo.ActivationResultType.from_int(in.read_long());
		switch (disc.value ())
		{
			case org.gnu.bonobo.ActivationResultType._ACTIVATION_RESULT_OBJECT:
			{
				org.omg.CORBA.Object _var;
				_var=in.read_Object();
				result.res_object (_var);
				break;
			}
			case org.gnu.bonobo.ActivationResultType._ACTIVATION_RESULT_SHLIB:
			{
				java.lang.String[] _var;
				_var = org.gnu.bonobo.StringListHelper.read(in);
				result.res_shlib (_var);
				break;
			}
			default: result.__default (disc);
		}
		return result;
	}
	public static void write (org.omg.CORBA.portable.OutputStream out, ActivationResultData s)
	{
		out.write_long (s.discriminator().value ());
		switch (s.discriminator().value ())
		{
			case org.gnu.bonobo.ActivationResultType._ACTIVATION_RESULT_OBJECT:
			{
				out.write_Object(s.res_object ());
				break;
			}
			case org.gnu.bonobo.ActivationResultType._ACTIVATION_RESULT_SHLIB:
			{
				org.gnu.bonobo.StringListHelper.write(out,s.res_shlib ());
				break;
			}
		}
	}
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			org.omg.CORBA.UnionMember[] members = new org.omg.CORBA.UnionMember[2];
			org.omg.CORBA.Any label_any;
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.ActivationResultTypeHelper.insert(label_any, org.gnu.bonobo.ActivationResultType.ACTIVATION_RESULT_OBJECT);
			members[1] = new org.omg.CORBA.UnionMember ("res_object", label_any, org.omg.CORBA.ORB.init().create_interface_tc("IDL:omg.org/CORBA/Object:1.0","Object"),null);
			label_any = org.omg.CORBA.ORB.init().create_any ();
			org.gnu.bonobo.ActivationResultTypeHelper.insert(label_any, org.gnu.bonobo.ActivationResultType.ACTIVATION_RESULT_SHLIB);
			members[0] = new org.omg.CORBA.UnionMember ("res_shlib", label_any, org.omg.CORBA.ORB.init().create_sequence_tc(0, org.omg.CORBA.ORB.init().create_string_tc(0)),null);
			 _type = org.omg.CORBA.ORB.init().create_union_tc(id(),"ActivationResultData",org.gnu.bonobo.ActivationResultTypeHelper.type(), members);
		}
		return _type;
	}
}
