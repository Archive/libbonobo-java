package org.gnu.bonobo;

/**
 *	Generated from IDL definition of union "ActivationResultData"
 *	@author JacORB IDL compiler 
 */

public final class ActivationResultData
	implements org.omg.CORBA.portable.IDLEntity
{
	private org.gnu.bonobo.ActivationResultType discriminator;
	private org.omg.CORBA.Object res_object;
	private java.lang.String[] res_shlib;

	public ActivationResultData ()
	{
	}

	public org.gnu.bonobo.ActivationResultType discriminator ()
	{
		return discriminator;
	}

	public org.omg.CORBA.Object res_object ()
	{
		if (discriminator != org.gnu.bonobo.ActivationResultType.ACTIVATION_RESULT_OBJECT)
			throw new org.omg.CORBA.BAD_OPERATION();
		return res_object;
	}

	public void res_object (org.omg.CORBA.Object _x)
	{
		discriminator = org.gnu.bonobo.ActivationResultType.ACTIVATION_RESULT_OBJECT;
		res_object = _x;
	}

	public java.lang.String[] res_shlib ()
	{
		if (discriminator != org.gnu.bonobo.ActivationResultType.ACTIVATION_RESULT_SHLIB)
			throw new org.omg.CORBA.BAD_OPERATION();
		return res_shlib;
	}

	public void res_shlib (java.lang.String[] _x)
	{
		discriminator = org.gnu.bonobo.ActivationResultType.ACTIVATION_RESULT_SHLIB;
		res_shlib = _x;
	}

	public void __default ()
	{
		discriminator = org.gnu.bonobo.ActivationResultType.ACTIVATION_RESULT_NONE;
	}
	public void __default (org.gnu.bonobo.ActivationResultType _discriminator)
	{
		discriminator = _discriminator;
	}
}
