package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ResolveOptions"
 *	@author JacORB IDL compiler 
 */

public final class ResolveOptionsHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ResolveOptions value;

	public ResolveOptionsHolder ()
	{
	}
	public ResolveOptionsHolder(final org.gnu.bonobo.ResolveOptions initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.ResolveOptionsHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.ResolveOptionsHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.ResolveOptionsHelper.write(_out, value);
	}
}
