package org.gnu.bonobo;

/**
 *	Generated from IDL interface "PersistStream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class PersistStreamPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.PersistStreamOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(0));
		m_opsHash.put ( "ref", new java.lang.Integer(1));
		m_opsHash.put ( "isDirty", new java.lang.Integer(2));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(3));
		m_opsHash.put ( "unref", new java.lang.Integer(4));
		m_opsHash.put ( "getContentTypes", new java.lang.Integer(5));
		m_opsHash.put ( "getIId", new java.lang.Integer(6));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(7));
		m_opsHash.put ( "save", new java.lang.Integer(8));
		m_opsHash.put ( "load", new java.lang.Integer(9));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(10));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(11));
	}
	private String[] ids = {"IDL:Bonobo/PersistStream:1.0","IDL:Bonobo/Persist:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.PersistStream _this()
	{
		return org.gnu.bonobo.PersistStreamHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.PersistStream _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.PersistStreamHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 1: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 2: // isDirty
			{
				_out = handler.createReply();
				_out.write_boolean(isDirty());
				break;
			}
			case 3: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 4: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 5: // getContentTypes
			{
				_out = handler.createReply();
				org.gnu.bonobo.PersistPackage.ContentTypeListHelper.write(_out,getContentTypes());
				break;
			}
			case 6: // getIId
			{
				_out = handler.createReply();
				_out.write_string(getIId());
				break;
			}
			case 7: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 8: // save
			{
			try
			{
				org.gnu.bonobo.Stream _arg0=org.gnu.bonobo.StreamHelper.read(_input);
				java.lang.String _arg1=_input.read_string();
				_out = handler.createReply();
				save(_arg0,_arg1);
			}
			catch(org.gnu.bonobo.PersistPackage.WrongDataType _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.PersistPackage.WrongDataTypeHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.IOError _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.IOErrorHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.NotSupported _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.NotSupportedHelper.write(_out, _ex2);
			}
				break;
			}
			case 9: // load
			{
			try
			{
				org.gnu.bonobo.Stream _arg0=org.gnu.bonobo.StreamHelper.read(_input);
				java.lang.String _arg1=_input.read_string();
				_out = handler.createReply();
				load(_arg0,_arg1);
			}
			catch(org.gnu.bonobo.PersistPackage.WrongDataType _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.PersistPackage.WrongDataTypeHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.IOError _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.IOErrorHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.NotSupported _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.NotSupportedHelper.write(_out, _ex2);
			}
				break;
			}
			case 10: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 11: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
