package org.gnu.bonobo;

/**
 *	Generated from IDL interface "PersistFile"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class PersistFileHolder	implements org.omg.CORBA.portable.Streamable{
	 public PersistFile value;
	public PersistFileHolder()
	{
	}
	public PersistFileHolder (final PersistFile initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return PersistFileHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = PersistFileHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		PersistFileHelper.write (_out,value);
	}
}
