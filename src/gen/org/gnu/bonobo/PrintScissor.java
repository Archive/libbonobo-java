package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "PrintScissor"
 *	@author JacORB IDL compiler 
 */

public final class PrintScissor
	implements org.omg.CORBA.portable.IDLEntity
{
	public PrintScissor(){}
	public double width_first_page;
	public double width_per_page;
	public double height_first_page;
	public double height_per_page;
	public PrintScissor(double width_first_page, double width_per_page, double height_first_page, double height_per_page)
	{
		this.width_first_page = width_first_page;
		this.width_per_page = width_per_page;
		this.height_first_page = height_first_page;
		this.height_per_page = height_per_page;
	}
}
