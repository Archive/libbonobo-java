package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "NotImplemented"
 *	@author JacORB IDL compiler 
 */

public final class NotImplementedHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.NotImplemented value;

	public NotImplementedHolder ()
	{
	}
	public NotImplementedHolder(final org.gnu.bonobo.NotImplemented initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.NotImplementedHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.NotImplementedHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.NotImplementedHelper.write(_out, value);
	}
}
