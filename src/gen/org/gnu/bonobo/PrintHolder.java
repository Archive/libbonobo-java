package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Print"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class PrintHolder	implements org.omg.CORBA.portable.Streamable{
	 public Print value;
	public PrintHolder()
	{
	}
	public PrintHolder (final Print initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return PrintHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = PrintHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		PrintHelper.write (_out,value);
	}
}
