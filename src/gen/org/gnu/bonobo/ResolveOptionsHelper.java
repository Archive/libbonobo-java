package org.gnu.bonobo;


/**
 *	Generated from IDL definition of struct "ResolveOptions"
 *	@author JacORB IDL compiler 
 */

public final class ResolveOptionsHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.ResolveOptionsHelper.id(),"ResolveOptions",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("flags", org.gnu.bonobo.ResolveFlagHelper.type(), null),new org.omg.CORBA.StructMember("timeout", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.ResolveOptions s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.ResolveOptions extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/ResolveOptions:1.0";
	}
	public static org.gnu.bonobo.ResolveOptions read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.ResolveOptions result = new org.gnu.bonobo.ResolveOptions();
		result.flags=in.read_long();
		result.timeout=in.read_long();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.ResolveOptions s)
	{
		out.write_long(s.flags);
		out.write_long(s.timeout);
	}
}
