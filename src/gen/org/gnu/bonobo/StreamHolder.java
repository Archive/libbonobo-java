package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Stream"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class StreamHolder	implements org.omg.CORBA.portable.Streamable{
	 public Stream value;
	public StreamHolder()
	{
	}
	public StreamHolder (final Stream initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return StreamHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = StreamHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		StreamHelper.write (_out,value);
	}
}
