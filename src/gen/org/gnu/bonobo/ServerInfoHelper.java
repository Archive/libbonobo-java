package org.gnu.bonobo;


/**
 *	Generated from IDL definition of struct "ServerInfo"
 *	@author JacORB IDL compiler 
 */

public final class ServerInfoHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.ServerInfoHelper.id(),"ServerInfo",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("iid", org.gnu.bonobo.ImplementationIDHelper.type(), null),new org.omg.CORBA.StructMember("server_type", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("location_info", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("username", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("hostname", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("domain", org.omg.CORBA.ORB.init().create_string_tc(0), null),new org.omg.CORBA.StructMember("props", org.omg.CORBA.ORB.init().create_sequence_tc(0, org.gnu.bonobo.ActivationPropertyHelper.type()), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.ServerInfo s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.ServerInfo extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/ServerInfo:1.0";
	}
	public static org.gnu.bonobo.ServerInfo read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.ServerInfo result = new org.gnu.bonobo.ServerInfo();
		result.iid=in.read_string();
		result.server_type=in.read_string();
		result.location_info=in.read_string();
		result.username=in.read_string();
		result.hostname=in.read_string();
		result.domain=in.read_string();
		int _lresult_props2 = in.read_long();
		result.props = new org.gnu.bonobo.ActivationProperty[_lresult_props2];
		for (int i=0;i<result.props.length;i++)
		{
			result.props[i]=org.gnu.bonobo.ActivationPropertyHelper.read(in);
		}

		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.ServerInfo s)
	{
		out.write_string(s.iid);
		out.write_string(s.server_type);
		out.write_string(s.location_info);
		out.write_string(s.username);
		out.write_string(s.hostname);
		out.write_string(s.domain);
		
		out.write_long(s.props.length);
		for (int i=0; i<s.props.length;i++)
		{
			org.gnu.bonobo.ActivationPropertyHelper.write(out,s.props[i]);
		}

	}
}
