package org.gnu.bonobo;

/**
 *	Generated from IDL definition of alias "ZoomLevelList"
 *	@author JacORB IDL compiler 
 */

public final class ZoomLevelListHolder
	implements org.omg.CORBA.portable.Streamable
{
	public float[] value;

	public ZoomLevelListHolder ()
	{
	}
	public ZoomLevelListHolder (final float[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ZoomLevelListHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ZoomLevelListHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ZoomLevelListHelper.write (out,value);
	}
}
