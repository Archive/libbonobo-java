package org.gnu.bonobo;
/**
 *	Generated from IDL definition of enum "DynamicPathLoadResult"
 *	@author JacORB IDL compiler 
 */

public final class DynamicPathLoadResultHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.DynamicPathLoadResultHelper.id(),"DynamicPathLoadResult",new String[]{"DYNAMIC_LOAD_SUCCESS","DYNAMIC_LOAD_ERROR","DYNAMIC_LOAD_NOT_LISTED","DYNAMIC_LOAD_ALREADY_LISTED"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.DynamicPathLoadResult s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.DynamicPathLoadResult extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/DynamicPathLoadResult:1.0";
	}
	public static DynamicPathLoadResult read (final org.omg.CORBA.portable.InputStream in)
	{
		return DynamicPathLoadResult.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final DynamicPathLoadResult s)
	{
		out.write_long(s.value());
	}
}
