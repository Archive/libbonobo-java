package org.gnu.bonobo;


/**
 *	Generated from IDL interface "UIComponent"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class UIComponentHelper
{
	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.UIComponent s)
	{
			any.insert_Object(s);
	}
	public static org.gnu.bonobo.UIComponent extract(final org.omg.CORBA.Any any)
	{
		return narrow(any.extract_Object()) ;
	}
	public static org.omg.CORBA.TypeCode type()
	{
		return org.omg.CORBA.ORB.init().create_interface_tc("IDL:Bonobo/UIComponent:1.0", "UIComponent");
	}
	public static String id()
	{
		return "IDL:Bonobo/UIComponent:1.0";
	}
	public static UIComponent read(final org.omg.CORBA.portable.InputStream in)
	{
		return narrow(in.read_Object());
	}
	public static void write(final org.omg.CORBA.portable.OutputStream _out, final org.gnu.bonobo.UIComponent s)
	{
		_out.write_Object(s);
	}
	public static org.gnu.bonobo.UIComponent narrow(final java.lang.Object obj)
	{
		if (obj instanceof org.gnu.bonobo.UIComponent)
		{
			return (org.gnu.bonobo.UIComponent)obj;
		}
		else if (obj instanceof org.omg.CORBA.Object)
		{
			return narrow((org.omg.CORBA.Object)obj);
		}
		throw new org.omg.CORBA.BAD_PARAM("Failed to narrow in helper");
	}
	public static org.gnu.bonobo.UIComponent narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
			return null;
		try
		{
			return (org.gnu.bonobo.UIComponent)obj;
		}
		catch (ClassCastException c)
		{
			if (obj._is_a("IDL:Bonobo/UIComponent:1.0"))
			{
				org.gnu.bonobo._UIComponentStub stub;
				stub = new org.gnu.bonobo._UIComponentStub();
				stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
				return stub;
			}
		}
		throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
	}
	public static org.gnu.bonobo.UIComponent unchecked_narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
			return null;
		try
		{
			return (org.gnu.bonobo.UIComponent)obj;
		}
		catch (ClassCastException c)
		{
				org.gnu.bonobo._UIComponentStub stub;
				stub = new org.gnu.bonobo._UIComponentStub();
				stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
				return stub;
		}
	}
}
