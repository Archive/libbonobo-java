package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "NotImplemented"
 *	@author JacORB IDL compiler 
 */

public final class NotImplemented
	extends org.omg.CORBA.UserException
{
	public NotImplemented()
	{
		super(org.gnu.bonobo.NotImplementedHelper.id());
	}

	public NotImplemented(String value)
	{
		super(value);
	}
}
