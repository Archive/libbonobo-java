package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "StorageInfo"
 *	@author JacORB IDL compiler 
 */

public final class StorageInfo
	implements org.omg.CORBA.portable.IDLEntity
{
	public StorageInfo(){}
	public java.lang.String name = "";
	public org.gnu.bonobo.StorageType type;
	public java.lang.String content_type;
	public int size;
	public StorageInfo(java.lang.String name, org.gnu.bonobo.StorageType type, java.lang.String content_type, int size)
	{
		this.name = name;
		this.type = type;
		this.content_type = content_type;
		this.size = size;
	}
}
