package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "GenericFactory"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class GenericFactoryPOATie
	extends GenericFactoryPOA
{
	private GenericFactoryOperations _delegate;

	private POA _poa;
	public GenericFactoryPOATie(GenericFactoryOperations delegate)
	{
		_delegate = delegate;
	}
	public GenericFactoryPOATie(GenericFactoryOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.GenericFactory _this()
	{
		return org.gnu.bonobo.GenericFactoryHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.GenericFactory _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.GenericFactoryHelper.narrow(_this_object(orb));
	}
	public GenericFactoryOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(GenericFactoryOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void ref()
	{
_delegate.ref();
	}

	public void unref()
	{
_delegate.unref();
	}

	public org.omg.CORBA.Object createObject(java.lang.String iid) throws org.gnu.bonobo.GenericFactoryPackage.CannotActivate
	{
		return _delegate.createObject(iid);
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
