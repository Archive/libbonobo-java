package org.gnu.bonobo;
/**
 *	Generated from IDL definition of union "ActivationPropertyValue"
 *	@author JacORB IDL compiler 
 */

public final class ActivationPropertyValueHolder
	implements org.omg.CORBA.portable.Streamable
{
	public ActivationPropertyValue value;

	public ActivationPropertyValueHolder ()
	{
	}
	public ActivationPropertyValueHolder (final ActivationPropertyValue initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return ActivationPropertyValueHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ActivationPropertyValueHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		ActivationPropertyValueHelper.write (out, value);
	}
}
