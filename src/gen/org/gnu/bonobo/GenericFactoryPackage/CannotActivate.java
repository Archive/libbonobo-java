package org.gnu.bonobo.GenericFactoryPackage;

/**
 *	Generated from IDL definition of exception "CannotActivate"
 *	@author JacORB IDL compiler 
 */

public final class CannotActivate
	extends org.omg.CORBA.UserException
{
	public CannotActivate()
	{
		super(org.gnu.bonobo.GenericFactoryPackage.CannotActivateHelper.id());
	}

	public CannotActivate(String value)
	{
		super(value);
	}
}
