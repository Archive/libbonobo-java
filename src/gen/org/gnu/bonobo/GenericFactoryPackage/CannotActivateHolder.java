package org.gnu.bonobo.GenericFactoryPackage;

/**
 *	Generated from IDL definition of exception "CannotActivate"
 *	@author JacORB IDL compiler 
 */

public final class CannotActivateHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.GenericFactoryPackage.CannotActivate value;

	public CannotActivateHolder ()
	{
	}
	public CannotActivateHolder(final org.gnu.bonobo.GenericFactoryPackage.CannotActivate initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.GenericFactoryPackage.CannotActivateHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.GenericFactoryPackage.CannotActivateHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.GenericFactoryPackage.CannotActivateHelper.write(_out, value);
	}
}
