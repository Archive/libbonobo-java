package org.gnu.bonobo.Gtk;

/**
 *	Generated from IDL definition of struct "Requisition"
 *	@author JacORB IDL compiler 
 */

public final class Requisition
	implements org.omg.CORBA.portable.IDLEntity
{
	public Requisition(){}
	public int width;
	public int height;
	public Requisition(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
}
