package org.gnu.bonobo.Gtk;
/**
 *	Generated from IDL definition of enum "State"
 *	@author JacORB IDL compiler 
 */

public final class StateHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.Gtk.StateHelper.id(),"State",new String[]{"StateNormal","StateActive","StatePrelight","StateSelected","StateInsensitive"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gtk.State s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gtk.State extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gtk/State:1.0";
	}
	public static State read (final org.omg.CORBA.portable.InputStream in)
	{
		return State.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final State s)
	{
		out.write_long(s.value());
	}
}
