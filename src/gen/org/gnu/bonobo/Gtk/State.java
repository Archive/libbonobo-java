package org.gnu.bonobo.Gtk;
/**
 *	Generated from IDL definition of enum "State"
 *	@author JacORB IDL compiler 
 */

public final class State
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _StateNormal = 0;
	public static final State StateNormal = new State(_StateNormal);
	public static final int _StateActive = 1;
	public static final State StateActive = new State(_StateActive);
	public static final int _StatePrelight = 2;
	public static final State StatePrelight = new State(_StatePrelight);
	public static final int _StateSelected = 3;
	public static final State StateSelected = new State(_StateSelected);
	public static final int _StateInsensitive = 4;
	public static final State StateInsensitive = new State(_StateInsensitive);
	public int value()
	{
		return value;
	}
	public static State from_int(int value)
	{
		switch (value) {
			case _StateNormal: return StateNormal;
			case _StateActive: return StateActive;
			case _StatePrelight: return StatePrelight;
			case _StateSelected: return StateSelected;
			case _StateInsensitive: return StateInsensitive;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected State(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
