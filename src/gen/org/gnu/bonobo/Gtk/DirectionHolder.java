package org.gnu.bonobo.Gtk;
/**
 *	Generated from IDL definition of enum "Direction"
 *	@author JacORB IDL compiler 
 */

public final class DirectionHolder
	implements org.omg.CORBA.portable.Streamable
{
	public Direction value;

	public DirectionHolder ()
	{
	}
	public DirectionHolder (final Direction initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return DirectionHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = DirectionHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		DirectionHelper.write (out,value);
	}
}
