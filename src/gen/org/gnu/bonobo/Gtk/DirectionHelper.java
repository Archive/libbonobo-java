package org.gnu.bonobo.Gtk;
/**
 *	Generated from IDL definition of enum "Direction"
 *	@author JacORB IDL compiler 
 */

public final class DirectionHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_enum_tc(org.gnu.bonobo.Gtk.DirectionHelper.id(),"Direction",new String[]{"DirectionTabForward","DirectionTabBackward","DirectionUp","DirectionDown","DirectionLeft","DirectionRight"});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gtk.Direction s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gtk.Direction extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gtk/Direction:1.0";
	}
	public static Direction read (final org.omg.CORBA.portable.InputStream in)
	{
		return Direction.from_int(in.read_long());
	}

	public static void write (final org.omg.CORBA.portable.OutputStream out, final Direction s)
	{
		out.write_long(s.value());
	}
}
