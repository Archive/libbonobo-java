package org.gnu.bonobo.Gtk;

/**
 *	Generated from IDL definition of struct "Requisition"
 *	@author JacORB IDL compiler 
 */

public final class RequisitionHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Gtk.Requisition value;

	public RequisitionHolder ()
	{
	}
	public RequisitionHolder(final org.gnu.bonobo.Gtk.Requisition initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Gtk.RequisitionHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Gtk.RequisitionHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Gtk.RequisitionHelper.write(_out, value);
	}
}
