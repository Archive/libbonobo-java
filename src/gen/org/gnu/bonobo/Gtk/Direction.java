package org.gnu.bonobo.Gtk;
/**
 *	Generated from IDL definition of enum "Direction"
 *	@author JacORB IDL compiler 
 */

public final class Direction
	implements org.omg.CORBA.portable.IDLEntity
{
	private int value = -1;
	public static final int _DirectionTabForward = 0;
	public static final Direction DirectionTabForward = new Direction(_DirectionTabForward);
	public static final int _DirectionTabBackward = 1;
	public static final Direction DirectionTabBackward = new Direction(_DirectionTabBackward);
	public static final int _DirectionUp = 2;
	public static final Direction DirectionUp = new Direction(_DirectionUp);
	public static final int _DirectionDown = 3;
	public static final Direction DirectionDown = new Direction(_DirectionDown);
	public static final int _DirectionLeft = 4;
	public static final Direction DirectionLeft = new Direction(_DirectionLeft);
	public static final int _DirectionRight = 5;
	public static final Direction DirectionRight = new Direction(_DirectionRight);
	public int value()
	{
		return value;
	}
	public static Direction from_int(int value)
	{
		switch (value) {
			case _DirectionTabForward: return DirectionTabForward;
			case _DirectionTabBackward: return DirectionTabBackward;
			case _DirectionUp: return DirectionUp;
			case _DirectionDown: return DirectionDown;
			case _DirectionLeft: return DirectionLeft;
			case _DirectionRight: return DirectionRight;
			default: throw new org.omg.CORBA.BAD_PARAM();
		}
	}
	protected Direction(int i)
	{
		value = i;
	}
	java.lang.Object readResolve()
	throws java.io.ObjectStreamException
	{
		return from_int(value());
	}
}
