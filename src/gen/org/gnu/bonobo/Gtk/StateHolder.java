package org.gnu.bonobo.Gtk;
/**
 *	Generated from IDL definition of enum "State"
 *	@author JacORB IDL compiler 
 */

public final class StateHolder
	implements org.omg.CORBA.portable.Streamable
{
	public State value;

	public StateHolder ()
	{
	}
	public StateHolder (final State initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return StateHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = StateHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		StateHelper.write (out,value);
	}
}
