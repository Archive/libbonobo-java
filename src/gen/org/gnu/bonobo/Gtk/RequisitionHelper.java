package org.gnu.bonobo.Gtk;


/**
 *	Generated from IDL definition of struct "Requisition"
 *	@author JacORB IDL compiler 
 */

public final class RequisitionHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Gtk.RequisitionHelper.id(),"Requisition",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("width", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("height", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Gtk.Requisition s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Gtk.Requisition extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Gtk/Requisition:1.0";
	}
	public static org.gnu.bonobo.Gtk.Requisition read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Gtk.Requisition result = new org.gnu.bonobo.Gtk.Requisition();
		result.width=in.read_long();
		result.height=in.read_long();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Gtk.Requisition s)
	{
		out.write_long(s.width);
		out.write_long(s.height);
	}
}
