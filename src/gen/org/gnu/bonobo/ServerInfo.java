package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ServerInfo"
 *	@author JacORB IDL compiler 
 */

public final class ServerInfo
	implements org.omg.CORBA.portable.IDLEntity
{
	public ServerInfo(){}
	public java.lang.String iid;
	public java.lang.String server_type = "";
	public java.lang.String location_info = "";
	public java.lang.String username = "";
	public java.lang.String hostname = "";
	public java.lang.String domain = "";
	public org.gnu.bonobo.ActivationProperty[] props;
	public ServerInfo(java.lang.String iid, java.lang.String server_type, java.lang.String location_info, java.lang.String username, java.lang.String hostname, java.lang.String domain, org.gnu.bonobo.ActivationProperty[] props)
	{
		this.iid = iid;
		this.server_type = server_type;
		this.location_info = location_info;
		this.username = username;
		this.hostname = hostname;
		this.domain = domain;
		this.props = props;
	}
}
