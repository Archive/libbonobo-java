package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Storage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class StoragePOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.StorageOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(0));
		m_opsHash.put ( "rename", new java.lang.Integer(1));
		m_opsHash.put ( "erase", new java.lang.Integer(2));
		m_opsHash.put ( "ref", new java.lang.Integer(3));
		m_opsHash.put ( "listContents", new java.lang.Integer(4));
		m_opsHash.put ( "unref", new java.lang.Integer(5));
		m_opsHash.put ( "openStorage", new java.lang.Integer(6));
		m_opsHash.put ( "revert", new java.lang.Integer(7));
		m_opsHash.put ( "commit", new java.lang.Integer(8));
		m_opsHash.put ( "setInfo", new java.lang.Integer(9));
		m_opsHash.put ( "openStream", new java.lang.Integer(10));
		m_opsHash.put ( "copyTo", new java.lang.Integer(11));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(12));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(13));
		m_opsHash.put ( "getInfo", new java.lang.Integer(14));
	}
	private String[] ids = {"IDL:Bonobo/Storage:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.Storage _this()
	{
		return org.gnu.bonobo.StorageHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Storage _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.StorageHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 1: // rename
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				java.lang.String _arg1=_input.read_string();
				_out = handler.createReply();
				rename(_arg0,_arg1);
			}
			catch(org.gnu.bonobo.StoragePackage.NotFound _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotFoundHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NameExists _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NameExistsHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex2);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex3)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex3);
			}
				break;
			}
			case 2: // erase
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				erase(_arg0);
			}
			catch(org.gnu.bonobo.StoragePackage.NotEmpty _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotEmptyHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NotFound _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotFoundHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex2);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex3)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex3);
			}
				break;
			}
			case 3: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 4: // listContents
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				int _arg1=_input.read_long();
				_out = handler.createReply();
				org.gnu.bonobo.StoragePackage.DirectoryListHelper.write(_out,listContents(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.StoragePackage.NotFound _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotFoundHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NotSupported _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotSupportedHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.NotStorage _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotStorageHelper.write(_out, _ex2);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex3)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex3);
			}
				break;
			}
			case 5: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 6: // openStorage
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				int _arg1=_input.read_long();
				_out = handler.createReply();
				org.gnu.bonobo.StorageHelper.write(_out,openStorage(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.StoragePackage.NotFound _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotFoundHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NameExists _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NameExistsHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex2);
			}
			catch(org.gnu.bonobo.StoragePackage.NotStorage _ex3)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotStorageHelper.write(_out, _ex3);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex4)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex4);
			}
				break;
			}
			case 7: // revert
			{
			try
			{
				_out = handler.createReply();
				revert();
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NotSupported _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotSupportedHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex2);
			}
				break;
			}
			case 8: // commit
			{
			try
			{
				_out = handler.createReply();
				commit();
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NotSupported _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotSupportedHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex2);
			}
				break;
			}
			case 9: // setInfo
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				org.gnu.bonobo.StorageInfo _arg1=org.gnu.bonobo.StorageInfoHelper.read(_input);
				int _arg2=_input.read_long();
				_out = handler.createReply();
				setInfo(_arg0,_arg1,_arg2);
			}
			catch(org.gnu.bonobo.StoragePackage.NotFound _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotFoundHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.NotSupported _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotSupportedHelper.write(_out, _ex2);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex3)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex3);
			}
				break;
			}
			case 10: // openStream
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				int _arg1=_input.read_long();
				_out = handler.createReply();
				org.gnu.bonobo.StreamHelper.write(_out,openStream(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.StoragePackage.NotFound _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotFoundHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NameExists _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NameExistsHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex2);
			}
			catch(org.gnu.bonobo.StoragePackage.NotStream _ex3)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotStreamHelper.write(_out, _ex3);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex4)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex4);
			}
				break;
			}
			case 11: // copyTo
			{
			try
			{
				org.gnu.bonobo.Storage _arg0=org.gnu.bonobo.StorageHelper.read(_input);
				_out = handler.createReply();
				copyTo(_arg0);
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex1);
			}
				break;
			}
			case 12: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 13: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
			case 14: // getInfo
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				int _arg1=_input.read_long();
				_out = handler.createReply();
				org.gnu.bonobo.StorageInfoHelper.write(_out,getInfo(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.StoragePackage.NotFound _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotFoundHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.StoragePackage.NoPermission _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NoPermissionHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.StoragePackage.NotSupported _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.NotSupportedHelper.write(_out, _ex2);
			}
			catch(org.gnu.bonobo.StoragePackage.IOError _ex3)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.StoragePackage.IOErrorHelper.write(_out, _ex3);
			}
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
