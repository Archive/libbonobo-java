package org.gnu.bonobo;


/**
 *	Generated from IDL interface "Storage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class _StorageStub
	extends org.omg.CORBA.portable.ObjectImpl
	implements org.gnu.bonobo.Storage
{
	private String[] ids = {"IDL:Bonobo/Storage:1.0","IDL:Bonobo/Unknown:1.0"};
	public String[] _ids()
	{
		return ids;
	}

	public final static java.lang.Class _opsClass = org.gnu.bonobo.StorageOperations.class;
	public void unImplemented2()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unImplemented2", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unImplemented2", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.unImplemented2();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void rename(java.lang.String path_name, java.lang.String new_path_name) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "rename", true);
				_os.write_string(path_name);
				_os.write_string(new_path_name);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NotFound:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotFoundHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NameExists:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NameExistsHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "rename", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.rename(path_name,new_path_name);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void erase(java.lang.String path) throws org.gnu.bonobo.StoragePackage.NotEmpty,org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "erase", true);
				_os.write_string(path);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NotEmpty:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotEmptyHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotFound:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotFoundHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "erase", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.erase(path);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void ref()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "ref", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "ref", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.ref();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public org.gnu.bonobo.StorageInfo[] listContents(java.lang.String path, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.NotStorage,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "listContents", true);
				_os.write_string(path);
				_os.write_long(mask);
				_is = _invoke(_os);
				org.gnu.bonobo.StorageInfo[] _result = org.gnu.bonobo.StoragePackage.DirectoryListHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NotFound:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotFoundHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotStorage:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotStorageHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "listContents", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			org.gnu.bonobo.StorageInfo[] _result;			try
			{
			_result = _localServant.listContents(path,mask);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

	public void unref()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unref", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unref", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.unref();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public org.gnu.bonobo.Storage openStorage(java.lang.String path, int mode) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotStorage,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "openStorage", true);
				_os.write_string(path);
				_os.write_long(mode);
				_is = _invoke(_os);
				org.gnu.bonobo.Storage _result = org.gnu.bonobo.StorageHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NotFound:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotFoundHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NameExists:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NameExistsHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotStorage:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotStorageHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "openStorage", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			org.gnu.bonobo.Storage _result;			try
			{
			_result = _localServant.openStorage(path,mode);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

	public void revert() throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "revert", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "revert", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.revert();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void commit() throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "commit", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "commit", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.commit();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void setInfo(java.lang.String path, org.gnu.bonobo.StorageInfo info, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "setInfo", true);
				_os.write_string(path);
				org.gnu.bonobo.StorageInfoHelper.write(_os,info);
				_os.write_long(mask);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NotFound:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotFoundHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "setInfo", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.setInfo(path,info,mask);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public org.gnu.bonobo.Stream openStream(java.lang.String path, int mode) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NameExists,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotStream,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "openStream", true);
				_os.write_string(path);
				_os.write_long(mode);
				_is = _invoke(_os);
				org.gnu.bonobo.Stream _result = org.gnu.bonobo.StreamHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NotFound:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotFoundHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NameExists:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NameExistsHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotStream:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotStreamHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "openStream", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			org.gnu.bonobo.Stream _result;			try
			{
			_result = _localServant.openStream(path,mode);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

	public void copyTo(org.gnu.bonobo.Storage target) throws org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "copyTo", true);
				org.gnu.bonobo.StorageHelper.write(_os,target);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "copyTo", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.copyTo(target);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public void unImplemented1()
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "unImplemented1", true);
				_is = _invoke(_os);
				return;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "unImplemented1", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			try
			{
			_localServant.unImplemented1();
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return;
		}

		}

	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "queryInterface", true);
				_os.write_string(repoid);
				_is = _invoke(_os);
				org.gnu.bonobo.Unknown _result = org.gnu.bonobo.UnknownHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "queryInterface", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			org.gnu.bonobo.Unknown _result;			try
			{
			_result = _localServant.queryInterface(repoid);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

	public org.gnu.bonobo.StorageInfo getInfo(java.lang.String path, int mask) throws org.gnu.bonobo.StoragePackage.NotFound,org.gnu.bonobo.StoragePackage.NoPermission,org.gnu.bonobo.StoragePackage.NotSupported,org.gnu.bonobo.StoragePackage.IOError
	{
		while(true)
		{
		if(! this._is_local())
		{
			org.omg.CORBA.portable.InputStream _is = null;
			try
			{
				org.omg.CORBA.portable.OutputStream _os = _request( "getInfo", true);
				_os.write_string(path);
				_os.write_long(mask);
				_is = _invoke(_os);
				org.gnu.bonobo.StorageInfo _result = org.gnu.bonobo.StorageInfoHelper.read(_is);
				return _result;
			}
			catch( org.omg.CORBA.portable.RemarshalException _rx ){}
			catch( org.omg.CORBA.portable.ApplicationException _ax )
			{
				String _id = _ax.getId();
				if( _id.equals("IDL:Bonobo/Storage/NotFound:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotFoundHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NoPermission:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NoPermissionHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/NotSupported:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.NotSupportedHelper.read(_ax.getInputStream());
				}
				else if( _id.equals("IDL:Bonobo/Storage/IOError:1.0"))
				{
					throw org.gnu.bonobo.StoragePackage.IOErrorHelper.read(_ax.getInputStream());
				}
				else 
					throw new RuntimeException("Unexpected exception " + _id );
			}
			finally
			{
				this._releaseReply(_is);
			}
		}
		else
		{
			org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke( "getInfo", _opsClass );
			if( _so == null )
				throw new org.omg.CORBA.UNKNOWN("local invocations not supported!");
			StorageOperations _localServant = (StorageOperations)_so.servant;
			org.gnu.bonobo.StorageInfo _result;			try
			{
			_result = _localServant.getInfo(path,mask);
			}
			finally
			{
				_servant_postinvoke(_so);
			}
			return _result;
		}

		}

	}

}
