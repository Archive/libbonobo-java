package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "EventSource"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class EventSourcePOATie
	extends EventSourcePOA
{
	private EventSourceOperations _delegate;

	private POA _poa;
	public EventSourcePOATie(EventSourceOperations delegate)
	{
		_delegate = delegate;
	}
	public EventSourcePOATie(EventSourceOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.EventSource _this()
	{
		return org.gnu.bonobo.EventSourceHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.EventSource _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.EventSourceHelper.narrow(_this_object(orb));
	}
	public EventSourceOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(EventSourceOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void ref()
	{
_delegate.ref();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void unImplemented()
	{
_delegate.unImplemented();
	}

	public void addListenerWithMask(org.gnu.bonobo.Listener l, java.lang.String event_mask)
	{
_delegate.addListenerWithMask(l,event_mask);
	}

	public void addListener(org.gnu.bonobo.Listener l)
	{
_delegate.addListener(l);
	}

	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void removeListener(org.gnu.bonobo.Listener l) throws org.gnu.bonobo.EventSourcePackage.UnknownListener
	{
_delegate.removeListener(l);
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
