package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL interface "ComponentProxy"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class ComponentProxyHolder	implements org.omg.CORBA.portable.Streamable{
	 public ComponentProxy value;
	public ComponentProxyHolder()
	{
	}
	public ComponentProxyHolder (final ComponentProxy initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return ComponentProxyHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ComponentProxyHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		ComponentProxyHelper.write (_out,value);
	}
}
