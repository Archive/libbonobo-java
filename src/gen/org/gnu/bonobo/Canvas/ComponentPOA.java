package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL interface "Component"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class ComponentPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.Canvas.ComponentOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "render", new java.lang.Integer(0));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(1));
		m_opsHash.put ( "contains", new java.lang.Integer(2));
		m_opsHash.put ( "update", new java.lang.Integer(3));
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(4));
		m_opsHash.put ( "unrealize", new java.lang.Integer(5));
		m_opsHash.put ( "realize", new java.lang.Integer(6));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(7));
		m_opsHash.put ( "setBounds", new java.lang.Integer(8));
		m_opsHash.put ( "bounds", new java.lang.Integer(9));
		m_opsHash.put ( "setCanvasSize", new java.lang.Integer(10));
		m_opsHash.put ( "unImplemented6", new java.lang.Integer(11));
		m_opsHash.put ( "unmap", new java.lang.Integer(12));
		m_opsHash.put ( "map", new java.lang.Integer(13));
		m_opsHash.put ( "unImplemented8", new java.lang.Integer(14));
		m_opsHash.put ( "unref", new java.lang.Integer(15));
		m_opsHash.put ( "ref", new java.lang.Integer(16));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(17));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(18));
		m_opsHash.put ( "unImplemented5", new java.lang.Integer(19));
		m_opsHash.put ( "unImplemented7", new java.lang.Integer(20));
		m_opsHash.put ( "event", new java.lang.Integer(21));
		m_opsHash.put ( "draw", new java.lang.Integer(22));
	}
	private String[] ids = {"IDL:Bonobo/Canvas/Component:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.Canvas.Component _this()
	{
		return org.gnu.bonobo.Canvas.ComponentHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Canvas.Component _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.Canvas.ComponentHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // render
			{
				org.gnu.bonobo.Canvas.BufHolder _arg0= new org.gnu.bonobo.Canvas.BufHolder();
				_arg0._read (_input);
				_out = handler.createReply();
				render(_arg0);
				org.gnu.bonobo.Canvas.BufHelper.write(_out,_arg0.value);
				break;
			}
			case 1: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
			case 2: // contains
			{
				double _arg0=_input.read_double();
				double _arg1=_input.read_double();
				_out = handler.createReply();
				_out.write_boolean(contains(_arg0,_arg1));
				break;
			}
			case 3: // update
			{
				org.gnu.bonobo.Canvas.State _arg0=org.gnu.bonobo.Canvas.StateHelper.read(_input);
				double[] _arg1=org.gnu.bonobo.Canvas.affineHelper.read(_input);
				org.gnu.bonobo.Canvas.SVPSegment[] _arg2=org.gnu.bonobo.Canvas.SVPHelper.read(_input);
				int _arg3=_input.read_long();
				org.omg.CORBA.DoubleHolder _arg4= new org.omg.CORBA.DoubleHolder();
				org.omg.CORBA.DoubleHolder _arg5= new org.omg.CORBA.DoubleHolder();
				org.omg.CORBA.DoubleHolder _arg6= new org.omg.CORBA.DoubleHolder();
				org.omg.CORBA.DoubleHolder _arg7= new org.omg.CORBA.DoubleHolder();
				_out = handler.createReply();
				org.gnu.bonobo.Canvas.ArtUTAHelper.write(_out,update(_arg0,_arg1,_arg2,_arg3,_arg4,_arg5,_arg6,_arg7));
				_out.write_double(_arg4.value);
				_out.write_double(_arg5.value);
				_out.write_double(_arg6.value);
				_out.write_double(_arg7.value);
				break;
			}
			case 4: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 5: // unrealize
			{
				_out = handler.createReply();
				unrealize();
				break;
			}
			case 6: // realize
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				realize(_arg0);
				break;
			}
			case 7: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 8: // setBounds
			{
				org.gnu.bonobo.Canvas.DRect _arg0=org.gnu.bonobo.Canvas.DRectHelper.read(_input);
				_out = handler.createReply();
				setBounds(_arg0);
				break;
			}
			case 9: // bounds
			{
				org.gnu.bonobo.Canvas.State _arg0=org.gnu.bonobo.Canvas.StateHelper.read(_input);
				org.omg.CORBA.DoubleHolder _arg1= new org.omg.CORBA.DoubleHolder();
				org.omg.CORBA.DoubleHolder _arg2= new org.omg.CORBA.DoubleHolder();
				org.omg.CORBA.DoubleHolder _arg3= new org.omg.CORBA.DoubleHolder();
				org.omg.CORBA.DoubleHolder _arg4= new org.omg.CORBA.DoubleHolder();
				_out = handler.createReply();
				bounds(_arg0,_arg1,_arg2,_arg3,_arg4);
				_out.write_double(_arg1.value);
				_out.write_double(_arg2.value);
				_out.write_double(_arg3.value);
				_out.write_double(_arg4.value);
				break;
			}
			case 10: // setCanvasSize
			{
				short _arg0=_input.read_short();
				short _arg1=_input.read_short();
				short _arg2=_input.read_short();
				short _arg3=_input.read_short();
				_out = handler.createReply();
				setCanvasSize(_arg0,_arg1,_arg2,_arg3);
				break;
			}
			case 11: // unImplemented6
			{
				_out = handler.createReply();
				unImplemented6();
				break;
			}
			case 12: // unmap
			{
				_out = handler.createReply();
				unmap();
				break;
			}
			case 13: // map
			{
				_out = handler.createReply();
				map();
				break;
			}
			case 14: // unImplemented8
			{
				_out = handler.createReply();
				unImplemented8();
				break;
			}
			case 15: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 16: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 17: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 18: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 19: // unImplemented5
			{
				_out = handler.createReply();
				unImplemented5();
				break;
			}
			case 20: // unImplemented7
			{
				_out = handler.createReply();
				unImplemented7();
				break;
			}
			case 21: // event
			{
				org.gnu.bonobo.Canvas.State _arg0=org.gnu.bonobo.Canvas.StateHelper.read(_input);
				org.gnu.bonobo.Gdk.Event _arg1=org.gnu.bonobo.Gdk.EventHelper.read(_input);
				_out = handler.createReply();
				_out.write_boolean(event(_arg0,_arg1));
				break;
			}
			case 22: // draw
			{
				org.gnu.bonobo.Canvas.State _arg0=org.gnu.bonobo.Canvas.StateHelper.read(_input);
				java.lang.String _arg1=_input.read_string();
				short _arg2=_input.read_short();
				short _arg3=_input.read_short();
				short _arg4=_input.read_short();
				short _arg5=_input.read_short();
				_out = handler.createReply();
				draw(_arg0,_arg1,_arg2,_arg3,_arg4,_arg5);
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
