package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "State"
 *	@author JacORB IDL compiler 
 */

public final class StateHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Canvas.State value;

	public StateHolder ()
	{
	}
	public StateHolder(final org.gnu.bonobo.Canvas.State initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Canvas.StateHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Canvas.StateHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Canvas.StateHelper.write(_out, value);
	}
}
