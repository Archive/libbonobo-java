package org.gnu.bonobo.Canvas;


/**
 *	Generated from IDL definition of struct "Point"
 *	@author JacORB IDL compiler 
 */

public final class PointHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Canvas.PointHelper.id(),"Point",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("x", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("y", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Canvas.Point s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Canvas.Point extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Canvas/Point:1.0";
	}
	public static org.gnu.bonobo.Canvas.Point read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Canvas.Point result = new org.gnu.bonobo.Canvas.Point();
		result.x=in.read_double();
		result.y=in.read_double();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Canvas.Point s)
	{
		out.write_double(s.x);
		out.write_double(s.y);
	}
}
