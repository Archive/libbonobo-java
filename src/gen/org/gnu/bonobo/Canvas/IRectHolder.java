package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "IRect"
 *	@author JacORB IDL compiler 
 */

public final class IRectHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Canvas.IRect value;

	public IRectHolder ()
	{
	}
	public IRectHolder(final org.gnu.bonobo.Canvas.IRect initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Canvas.IRectHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Canvas.IRectHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Canvas.IRectHelper.write(_out, value);
	}
}
