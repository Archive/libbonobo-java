package org.gnu.bonobo.Canvas;


/**
 *	Generated from IDL definition of struct "SVPSegment"
 *	@author JacORB IDL compiler 
 */

public final class SVPSegmentHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Canvas.SVPSegmentHelper.id(),"SVPSegment",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("up", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(8)), null),new org.omg.CORBA.StructMember("bbox", org.gnu.bonobo.Canvas.DRectHelper.type(), null),new org.omg.CORBA.StructMember("points", org.gnu.bonobo.Canvas.PointsHelper.type(), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Canvas.SVPSegment s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Canvas.SVPSegment extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Canvas/SVPSegment:1.0";
	}
	public static org.gnu.bonobo.Canvas.SVPSegment read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Canvas.SVPSegment result = new org.gnu.bonobo.Canvas.SVPSegment();
		result.up=in.read_boolean();
		result.bbox=org.gnu.bonobo.Canvas.DRectHelper.read(in);
		result.points = org.gnu.bonobo.Canvas.PointsHelper.read(in);
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Canvas.SVPSegment s)
	{
		out.write_boolean(s.up);
		org.gnu.bonobo.Canvas.DRectHelper.write(out,s.bbox);
		org.gnu.bonobo.Canvas.PointsHelper.write(out,s.points);
	}
}
