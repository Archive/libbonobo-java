package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "DRect"
 *	@author JacORB IDL compiler 
 */

public final class DRectHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Canvas.DRect value;

	public DRectHolder ()
	{
	}
	public DRectHolder(final org.gnu.bonobo.Canvas.DRect initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Canvas.DRectHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Canvas.DRectHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Canvas.DRectHelper.write(_out, value);
	}
}
