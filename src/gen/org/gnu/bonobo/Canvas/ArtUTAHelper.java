package org.gnu.bonobo.Canvas;


/**
 *	Generated from IDL definition of struct "ArtUTA"
 *	@author JacORB IDL compiler 
 */

public final class ArtUTAHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Canvas.ArtUTAHelper.id(),"ArtUTA",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("x0", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null),new org.omg.CORBA.StructMember("y0", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null),new org.omg.CORBA.StructMember("width", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null),new org.omg.CORBA.StructMember("height", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null),new org.omg.CORBA.StructMember("utiles", org.omg.CORBA.ORB.init().create_sequence_tc(0, org.gnu.bonobo.Canvas.int32Helper.type()), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Canvas.ArtUTA s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Canvas.ArtUTA extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Canvas/ArtUTA:1.0";
	}
	public static org.gnu.bonobo.Canvas.ArtUTA read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Canvas.ArtUTA result = new org.gnu.bonobo.Canvas.ArtUTA();
		result.x0=in.read_short();
		result.y0=in.read_short();
		result.width=in.read_short();
		result.height=in.read_short();
		int _lresult_utiles3 = in.read_long();
		result.utiles = new int[_lresult_utiles3];
		for (int i=0;i<result.utiles.length;i++)
		{
			result.utiles[i]=in.read_long();
		}

		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Canvas.ArtUTA s)
	{
		out.write_short(s.x0);
		out.write_short(s.y0);
		out.write_short(s.width);
		out.write_short(s.height);
		
		out.write_long(s.utiles.length);
		for (int i=0; i<s.utiles.length;i++)
		{
			out.write_long(s.utiles[i]);
		}

	}
}
