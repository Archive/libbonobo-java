package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "Buf"
 *	@author JacORB IDL compiler 
 */

public final class Buf
	implements org.omg.CORBA.portable.IDLEntity
{
	public Buf(){}
	public byte[] rgb_buf;
	public int row_stride;
	public org.gnu.bonobo.Canvas.IRect rect;
	public int bg_color;
	public short flags;
	public Buf(byte[] rgb_buf, int row_stride, org.gnu.bonobo.Canvas.IRect rect, int bg_color, short flags)
	{
		this.rgb_buf = rgb_buf;
		this.row_stride = row_stride;
		this.rect = rect;
		this.bg_color = bg_color;
		this.flags = flags;
	}
}
