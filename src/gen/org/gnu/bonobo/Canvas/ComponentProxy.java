package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL interface "ComponentProxy"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public interface ComponentProxy
	extends ComponentProxyOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
