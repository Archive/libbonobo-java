package org.gnu.bonobo.Canvas;


/**
 *	Generated from IDL definition of struct "IRect"
 *	@author JacORB IDL compiler 
 */

public final class IRectHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Canvas.IRectHelper.id(),"IRect",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("x0", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("y0", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("x1", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("y1", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Canvas.IRect s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Canvas.IRect extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Canvas/IRect:1.0";
	}
	public static org.gnu.bonobo.Canvas.IRect read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Canvas.IRect result = new org.gnu.bonobo.Canvas.IRect();
		result.x0=in.read_long();
		result.y0=in.read_long();
		result.x1=in.read_long();
		result.y1=in.read_long();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Canvas.IRect s)
	{
		out.write_long(s.x0);
		out.write_long(s.y0);
		out.write_long(s.x1);
		out.write_long(s.y1);
	}
}
