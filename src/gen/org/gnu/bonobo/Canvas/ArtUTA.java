package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "ArtUTA"
 *	@author JacORB IDL compiler 
 */

public final class ArtUTA
	implements org.omg.CORBA.portable.IDLEntity
{
	public ArtUTA(){}
	public short x0;
	public short y0;
	public short width;
	public short height;
	public int[] utiles;
	public ArtUTA(short x0, short y0, short width, short height, int[] utiles)
	{
		this.x0 = x0;
		this.y0 = y0;
		this.width = width;
		this.height = height;
		this.utiles = utiles;
	}
}
