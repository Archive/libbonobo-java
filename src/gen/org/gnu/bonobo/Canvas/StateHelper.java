package org.gnu.bonobo.Canvas;


/**
 *	Generated from IDL definition of struct "State"
 *	@author JacORB IDL compiler 
 */

public final class StateHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Canvas.StateHelper.id(),"State",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("item_aff", org.gnu.bonobo.Canvas.affineHelper.type(), null),new org.omg.CORBA.StructMember("pixels_per_unit", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("canvas_scroll_x1", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("canvas_scroll_y1", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)), null),new org.omg.CORBA.StructMember("zoom_xofs", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("zoom_yofs", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("xoffset", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("yoffset", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Canvas.State s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Canvas.State extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Canvas/State:1.0";
	}
	public static org.gnu.bonobo.Canvas.State read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Canvas.State result = new org.gnu.bonobo.Canvas.State();
		result.item_aff = org.gnu.bonobo.Canvas.affineHelper.read(in);
		result.pixels_per_unit=in.read_double();
		result.canvas_scroll_x1=in.read_double();
		result.canvas_scroll_y1=in.read_double();
		result.zoom_xofs=in.read_long();
		result.zoom_yofs=in.read_long();
		result.xoffset=in.read_long();
		result.yoffset=in.read_long();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Canvas.State s)
	{
		org.gnu.bonobo.Canvas.affineHelper.write(out,s.item_aff);
		out.write_double(s.pixels_per_unit);
		out.write_double(s.canvas_scroll_x1);
		out.write_double(s.canvas_scroll_y1);
		out.write_long(s.zoom_xofs);
		out.write_long(s.zoom_yofs);
		out.write_long(s.xoffset);
		out.write_long(s.yoffset);
	}
}
