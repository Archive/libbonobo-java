package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "Point"
 *	@author JacORB IDL compiler 
 */

public final class PointHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Canvas.Point value;

	public PointHolder ()
	{
	}
	public PointHolder(final org.gnu.bonobo.Canvas.Point initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Canvas.PointHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Canvas.PointHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Canvas.PointHelper.write(_out, value);
	}
}
