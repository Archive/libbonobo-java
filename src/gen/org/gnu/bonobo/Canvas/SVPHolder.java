package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of alias "SVP"
 *	@author JacORB IDL compiler 
 */

public final class SVPHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Canvas.SVPSegment[] value;

	public SVPHolder ()
	{
	}
	public SVPHolder (final org.gnu.bonobo.Canvas.SVPSegment[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return SVPHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = SVPHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		SVPHelper.write (out,value);
	}
}
