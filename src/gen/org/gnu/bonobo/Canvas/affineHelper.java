package org.gnu.bonobo.Canvas;
public final class affineHelper
{
	private static org.omg.CORBA.TypeCode _type = org.omg.CORBA.ORB.init().create_array_tc(6,org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(7)));
	public static void insert (final org.omg.CORBA.Any any, final double[] s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static double[] extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static org.omg.CORBA.TypeCode type()
	{
		return _type;
	}
	public static String id()
	{
		return "IDL:Bonobo/Canvas/affine:1.0";
	}
	public static double[] read (final org.omg.CORBA.portable.InputStream _in)
	{
		double[] result = new double[6]; // double[]
		_in.read_double_array(result,0,6);
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final double[] s)
	{
		if (s.length != 6)
			throw new org.omg.CORBA.MARSHAL("Incorrect array size");
		out.write_double_array(s,0,6);
	}
}
