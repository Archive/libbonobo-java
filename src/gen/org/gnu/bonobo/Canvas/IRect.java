package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "IRect"
 *	@author JacORB IDL compiler 
 */

public final class IRect
	implements org.omg.CORBA.portable.IDLEntity
{
	public IRect(){}
	public int x0;
	public int y0;
	public int x1;
	public int y1;
	public IRect(int x0, int y0, int x1, int y1)
	{
		this.x0 = x0;
		this.y0 = y0;
		this.x1 = x1;
		this.y1 = y1;
	}
}
