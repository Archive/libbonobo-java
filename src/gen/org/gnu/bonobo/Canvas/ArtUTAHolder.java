package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "ArtUTA"
 *	@author JacORB IDL compiler 
 */

public final class ArtUTAHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Canvas.ArtUTA value;

	public ArtUTAHolder ()
	{
	}
	public ArtUTAHolder(final org.gnu.bonobo.Canvas.ArtUTA initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.Canvas.ArtUTAHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.Canvas.ArtUTAHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.Canvas.ArtUTAHelper.write(_out, value);
	}
}
