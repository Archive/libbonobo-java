package org.gnu.bonobo.Canvas;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "Component"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class ComponentPOATie
	extends ComponentPOA
{
	private ComponentOperations _delegate;

	private POA _poa;
	public ComponentPOATie(ComponentOperations delegate)
	{
		_delegate = delegate;
	}
	public ComponentPOATie(ComponentOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Canvas.Component _this()
	{
		return org.gnu.bonobo.Canvas.ComponentHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Canvas.Component _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.Canvas.ComponentHelper.narrow(_this_object(orb));
	}
	public ComponentOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(ComponentOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void render(org.gnu.bonobo.Canvas.BufHolder buf)
	{
_delegate.render(buf);
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

	public boolean contains(double x, double y)
	{
		return _delegate.contains(x,y);
	}

	public org.gnu.bonobo.Canvas.ArtUTA update(org.gnu.bonobo.Canvas.State state, double[] aff, org.gnu.bonobo.Canvas.SVPSegment[] clip_path, int flags, org.omg.CORBA.DoubleHolder x1, org.omg.CORBA.DoubleHolder y1, org.omg.CORBA.DoubleHolder x2, org.omg.CORBA.DoubleHolder y2)
	{
		return _delegate.update(state,aff,clip_path,flags,x1,y1,x2,y2);
	}

	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void unrealize()
	{
_delegate.unrealize();
	}

	public void realize(java.lang.String drawable)
	{
_delegate.realize(drawable);
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void setBounds(org.gnu.bonobo.Canvas.DRect bbox)
	{
_delegate.setBounds(bbox);
	}

	public void bounds(org.gnu.bonobo.Canvas.State state, org.omg.CORBA.DoubleHolder x1, org.omg.CORBA.DoubleHolder x2, org.omg.CORBA.DoubleHolder y1, org.omg.CORBA.DoubleHolder y2)
	{
_delegate.bounds(state,x1,x2,y1,y2);
	}

	public void setCanvasSize(short x, short y, short width, short height)
	{
_delegate.setCanvasSize(x,y,width,height);
	}

	public void unImplemented6()
	{
_delegate.unImplemented6();
	}

	public void unmap()
	{
_delegate.unmap();
	}

	public void map()
	{
_delegate.map();
	}

	public void unImplemented8()
	{
_delegate.unImplemented8();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void ref()
	{
_delegate.ref();
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unImplemented5()
	{
_delegate.unImplemented5();
	}

	public void unImplemented7()
	{
_delegate.unImplemented7();
	}

	public boolean event(org.gnu.bonobo.Canvas.State state, org.gnu.bonobo.Gdk.Event event)
	{
		return _delegate.event(state,event);
	}

	public void draw(org.gnu.bonobo.Canvas.State state, java.lang.String drawable, short x, short y, short width, short height)
	{
_delegate.draw(state,drawable,x,y,width,height);
	}

}
