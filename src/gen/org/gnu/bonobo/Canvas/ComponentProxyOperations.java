package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL interface "ComponentProxy"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface ComponentProxyOperations
{
	/* constants */
	/* operations  */
	void requestUpdate();
	void grabFocus(int mask, int cursor, int time);
	void ungrabFocus(int time);
	org.gnu.bonobo.UIContainer getUIContainer();
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
	void unImplemented5();
	void unImplemented6();
	void unImplemented7();
	void unImplemented8();
}
