package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "Point"
 *	@author JacORB IDL compiler 
 */

public final class Point
	implements org.omg.CORBA.portable.IDLEntity
{
	public Point(){}
	public double x;
	public double y;
	public Point(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
}
