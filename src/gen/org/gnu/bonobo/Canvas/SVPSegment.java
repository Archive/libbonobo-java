package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "SVPSegment"
 *	@author JacORB IDL compiler 
 */

public final class SVPSegment
	implements org.omg.CORBA.portable.IDLEntity
{
	public SVPSegment(){}
	public boolean up;
	public org.gnu.bonobo.Canvas.DRect bbox;
	public org.gnu.bonobo.Canvas.Point[] points;
	public SVPSegment(boolean up, org.gnu.bonobo.Canvas.DRect bbox, org.gnu.bonobo.Canvas.Point[] points)
	{
		this.up = up;
		this.bbox = bbox;
		this.points = points;
	}
}
