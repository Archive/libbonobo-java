package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL interface "Component"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class ComponentHolder	implements org.omg.CORBA.portable.Streamable{
	 public Component value;
	public ComponentHolder()
	{
	}
	public ComponentHolder (final Component initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type()
	{
		return ComponentHelper.type();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = ComponentHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		ComponentHelper.write (_out,value);
	}
}
