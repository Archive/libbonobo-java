package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL interface "Component"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface ComponentOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	org.gnu.bonobo.Canvas.ArtUTA update(org.gnu.bonobo.Canvas.State state, double[] aff, org.gnu.bonobo.Canvas.SVPSegment[] clip_path, int flags, org.omg.CORBA.DoubleHolder x1, org.omg.CORBA.DoubleHolder y1, org.omg.CORBA.DoubleHolder x2, org.omg.CORBA.DoubleHolder y2);
	void realize(java.lang.String drawable);
	void unrealize();
	void map();
	void unmap();
	void draw(org.gnu.bonobo.Canvas.State state, java.lang.String drawable, short x, short y, short width, short height);
	void render(org.gnu.bonobo.Canvas.BufHolder buf);
	boolean contains(double x, double y);
	void bounds(org.gnu.bonobo.Canvas.State state, org.omg.CORBA.DoubleHolder x1, org.omg.CORBA.DoubleHolder x2, org.omg.CORBA.DoubleHolder y1, org.omg.CORBA.DoubleHolder y2);
	boolean event(org.gnu.bonobo.Canvas.State state, org.gnu.bonobo.Gdk.Event event);
	void setCanvasSize(short x, short y, short width, short height);
	void setBounds(org.gnu.bonobo.Canvas.DRect bbox);
	void unImplemented1();
	void unImplemented2();
	void unImplemented3();
	void unImplemented4();
	void unImplemented5();
	void unImplemented6();
	void unImplemented7();
	void unImplemented8();
}
