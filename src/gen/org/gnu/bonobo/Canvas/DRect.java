package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "DRect"
 *	@author JacORB IDL compiler 
 */

public final class DRect
	implements org.omg.CORBA.portable.IDLEntity
{
	public DRect(){}
	public double x0;
	public double y0;
	public double x1;
	public double y1;
	public DRect(double x0, double y0, double x1, double y1)
	{
		this.x0 = x0;
		this.y0 = y0;
		this.x1 = x1;
		this.y1 = y1;
	}
}
