package org.gnu.bonobo.Canvas;

public final class affineHolder
	implements org.omg.CORBA.portable.Streamable
{
	public double[] value;
	public affineHolder ()
	{
	}
	public affineHolder (final double[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return affineHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream _in)
	{
		value = affineHelper.read (_in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream _out)
	{
		affineHelper.write (_out,value);
	}
}
