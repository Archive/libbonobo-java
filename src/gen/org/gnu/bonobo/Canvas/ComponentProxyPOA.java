package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL interface "ComponentProxy"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class ComponentProxyPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.Canvas.ComponentProxyOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(0));
		m_opsHash.put ( "unImplemented7", new java.lang.Integer(1));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(2));
		m_opsHash.put ( "unImplemented8", new java.lang.Integer(3));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(4));
		m_opsHash.put ( "unImplemented5", new java.lang.Integer(5));
		m_opsHash.put ( "grabFocus", new java.lang.Integer(6));
		m_opsHash.put ( "getUIContainer", new java.lang.Integer(7));
		m_opsHash.put ( "requestUpdate", new java.lang.Integer(8));
		m_opsHash.put ( "ungrabFocus", new java.lang.Integer(9));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(10));
		m_opsHash.put ( "unImplemented6", new java.lang.Integer(11));
	}
	private String[] ids = {"IDL:Bonobo/Canvas/ComponentProxy:1.0"};
	public org.gnu.bonobo.Canvas.ComponentProxy _this()
	{
		return org.gnu.bonobo.Canvas.ComponentProxyHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Canvas.ComponentProxy _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.Canvas.ComponentProxyHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 1: // unImplemented7
			{
				_out = handler.createReply();
				unImplemented7();
				break;
			}
			case 2: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 3: // unImplemented8
			{
				_out = handler.createReply();
				unImplemented8();
				break;
			}
			case 4: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 5: // unImplemented5
			{
				_out = handler.createReply();
				unImplemented5();
				break;
			}
			case 6: // grabFocus
			{
				int _arg0=_input.read_ulong();
				int _arg1=_input.read_long();
				int _arg2=_input.read_ulong();
				_out = handler.createReply();
				grabFocus(_arg0,_arg1,_arg2);
				break;
			}
			case 7: // getUIContainer
			{
				_out = handler.createReply();
				org.gnu.bonobo.UIContainerHelper.write(_out,getUIContainer());
				break;
			}
			case 8: // requestUpdate
			{
				_out = handler.createReply();
				requestUpdate();
				break;
			}
			case 9: // ungrabFocus
			{
				int _arg0=_input.read_ulong();
				_out = handler.createReply();
				ungrabFocus(_arg0);
				break;
			}
			case 10: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 11: // unImplemented6
			{
				_out = handler.createReply();
				unImplemented6();
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
