package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of alias "Points"
 *	@author JacORB IDL compiler 
 */

public final class PointsHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.Canvas.Point[] value;

	public PointsHolder ()
	{
	}
	public PointsHolder (final org.gnu.bonobo.Canvas.Point[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return PointsHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = PointsHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		PointsHelper.write (out,value);
	}
}
