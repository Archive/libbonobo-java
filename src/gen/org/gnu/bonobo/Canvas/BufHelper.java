package org.gnu.bonobo.Canvas;


/**
 *	Generated from IDL definition of struct "Buf"
 *	@author JacORB IDL compiler 
 */

public final class BufHelper
{
	private static org.omg.CORBA.TypeCode _type = null;
	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_struct_tc(org.gnu.bonobo.Canvas.BufHelper.id(),"Buf",new org.omg.CORBA.StructMember[]{new org.omg.CORBA.StructMember("rgb_buf", org.gnu.bonobo.Canvas.pixbufHelper.type(), null),new org.omg.CORBA.StructMember("row_stride", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(3)), null),new org.omg.CORBA.StructMember("rect", org.gnu.bonobo.Canvas.IRectHelper.type(), null),new org.omg.CORBA.StructMember("bg_color", org.gnu.bonobo.Canvas.int32Helper.type(), null),new org.omg.CORBA.StructMember("flags", org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.from_int(2)), null)});
		}
		return _type;
	}

	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.Canvas.Buf s)
	{
		any.type(type());
		write( any.create_output_stream(),s);
	}

	public static org.gnu.bonobo.Canvas.Buf extract (final org.omg.CORBA.Any any)
	{
		return read(any.create_input_stream());
	}

	public static String id()
	{
		return "IDL:Bonobo/Canvas/Buf:1.0";
	}
	public static org.gnu.bonobo.Canvas.Buf read (final org.omg.CORBA.portable.InputStream in)
	{
		org.gnu.bonobo.Canvas.Buf result = new org.gnu.bonobo.Canvas.Buf();
		result.rgb_buf = org.gnu.bonobo.Canvas.pixbufHelper.read(in);
		result.row_stride=in.read_long();
		result.rect=org.gnu.bonobo.Canvas.IRectHelper.read(in);
		result.bg_color=in.read_long();
		result.flags=in.read_short();
		return result;
	}
	public static void write (final org.omg.CORBA.portable.OutputStream out, final org.gnu.bonobo.Canvas.Buf s)
	{
		org.gnu.bonobo.Canvas.pixbufHelper.write(out,s.rgb_buf);
		out.write_long(s.row_stride);
		org.gnu.bonobo.Canvas.IRectHelper.write(out,s.rect);
		out.write_long(s.bg_color);
		out.write_short(s.flags);
	}
}
