package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of struct "State"
 *	@author JacORB IDL compiler 
 */

public final class State
	implements org.omg.CORBA.portable.IDLEntity
{
	public State(){}
	public double[] item_aff;
	public double pixels_per_unit;
	public double canvas_scroll_x1;
	public double canvas_scroll_y1;
	public int zoom_xofs;
	public int zoom_yofs;
	public int xoffset;
	public int yoffset;
	public State(double[] item_aff, double pixels_per_unit, double canvas_scroll_x1, double canvas_scroll_y1, int zoom_xofs, int zoom_yofs, int xoffset, int yoffset)
	{
		this.item_aff = item_aff;
		this.pixels_per_unit = pixels_per_unit;
		this.canvas_scroll_x1 = canvas_scroll_x1;
		this.canvas_scroll_y1 = canvas_scroll_y1;
		this.zoom_xofs = zoom_xofs;
		this.zoom_yofs = zoom_yofs;
		this.xoffset = xoffset;
		this.yoffset = yoffset;
	}
}
