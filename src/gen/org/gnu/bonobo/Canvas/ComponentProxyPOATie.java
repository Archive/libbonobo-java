package org.gnu.bonobo.Canvas;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "ComponentProxy"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class ComponentProxyPOATie
	extends ComponentProxyPOA
{
	private ComponentProxyOperations _delegate;

	private POA _poa;
	public ComponentProxyPOATie(ComponentProxyOperations delegate)
	{
		_delegate = delegate;
	}
	public ComponentProxyPOATie(ComponentProxyOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.Canvas.ComponentProxy _this()
	{
		return org.gnu.bonobo.Canvas.ComponentProxyHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Canvas.ComponentProxy _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.Canvas.ComponentProxyHelper.narrow(_this_object(orb));
	}
	public ComponentProxyOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(ComponentProxyOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void unImplemented7()
	{
_delegate.unImplemented7();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unImplemented8()
	{
_delegate.unImplemented8();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void unImplemented5()
	{
_delegate.unImplemented5();
	}

	public void grabFocus(int mask, int cursor, int time)
	{
_delegate.grabFocus(mask,cursor,time);
	}

	public org.gnu.bonobo.UIContainer getUIContainer()
	{
		return _delegate.getUIContainer();
	}

	public void requestUpdate()
	{
_delegate.requestUpdate();
	}

	public void ungrabFocus(int time)
	{
_delegate.ungrabFocus(time);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public void unImplemented6()
	{
_delegate.unImplemented6();
	}

}
