package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of alias "SVP"
 *	@author JacORB IDL compiler 
 */

public final class SVPHelper
{
	private static org.omg.CORBA.TypeCode _type = null;

	public static void insert (org.omg.CORBA.Any any, org.gnu.bonobo.Canvas.SVPSegment[] s)
	{
		any.type (type ());
		write (any.create_output_stream (), s);
	}

	public static org.gnu.bonobo.Canvas.SVPSegment[] extract (final org.omg.CORBA.Any any)
	{
		return read (any.create_input_stream ());
	}

	public static org.omg.CORBA.TypeCode type ()
	{
		if (_type == null)
		{
			_type = org.omg.CORBA.ORB.init().create_alias_tc(org.gnu.bonobo.Canvas.SVPHelper.id(), "SVP",org.omg.CORBA.ORB.init().create_sequence_tc(0, org.gnu.bonobo.Canvas.SVPSegmentHelper.type()));
		}
		return _type;
	}

	public static String id()
	{
		return "IDL:Bonobo/Canvas/SVP:1.0";
	}
	public static org.gnu.bonobo.Canvas.SVPSegment[] read (final org.omg.CORBA.portable.InputStream _in)
	{
		org.gnu.bonobo.Canvas.SVPSegment[] _result;
		int _l_result2 = _in.read_long();
		_result = new org.gnu.bonobo.Canvas.SVPSegment[_l_result2];
		for (int i=0;i<_result.length;i++)
		{
			_result[i]=org.gnu.bonobo.Canvas.SVPSegmentHelper.read(_in);
		}

		return _result;
	}

	public static void write (final org.omg.CORBA.portable.OutputStream _out, org.gnu.bonobo.Canvas.SVPSegment[] _s)
	{
		
		_out.write_long(_s.length);
		for (int i=0; i<_s.length;i++)
		{
			org.gnu.bonobo.Canvas.SVPSegmentHelper.write(_out,_s[i]);
		}

	}
}
