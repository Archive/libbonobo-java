package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL interface "Component"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public interface Component
	extends ComponentOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity, org.gnu.bonobo.Unknown
{
}
