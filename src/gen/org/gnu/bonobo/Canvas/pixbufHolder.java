package org.gnu.bonobo.Canvas;

/**
 *	Generated from IDL definition of alias "pixbuf"
 *	@author JacORB IDL compiler 
 */

public final class pixbufHolder
	implements org.omg.CORBA.portable.Streamable
{
	public byte[] value;

	public pixbufHolder ()
	{
	}
	public pixbufHolder (final byte[] initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return pixbufHelper.type ();
	}
	public void _read (final org.omg.CORBA.portable.InputStream in)
	{
		value = pixbufHelper.read (in);
	}
	public void _write (final org.omg.CORBA.portable.OutputStream out)
	{
		pixbufHelper.write (out,value);
	}
}
