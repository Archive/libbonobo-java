package org.gnu.bonobo;


/**
 *	Generated from IDL interface "PersistFile"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public final class PersistFileHelper
{
	public static void insert (final org.omg.CORBA.Any any, final org.gnu.bonobo.PersistFile s)
	{
			any.insert_Object(s);
	}
	public static org.gnu.bonobo.PersistFile extract(final org.omg.CORBA.Any any)
	{
		return narrow(any.extract_Object()) ;
	}
	public static org.omg.CORBA.TypeCode type()
	{
		return org.omg.CORBA.ORB.init().create_interface_tc("IDL:Bonobo/PersistFile:1.0", "PersistFile");
	}
	public static String id()
	{
		return "IDL:Bonobo/PersistFile:1.0";
	}
	public static PersistFile read(final org.omg.CORBA.portable.InputStream in)
	{
		return narrow(in.read_Object());
	}
	public static void write(final org.omg.CORBA.portable.OutputStream _out, final org.gnu.bonobo.PersistFile s)
	{
		_out.write_Object(s);
	}
	public static org.gnu.bonobo.PersistFile narrow(final java.lang.Object obj)
	{
		if (obj instanceof org.gnu.bonobo.PersistFile)
		{
			return (org.gnu.bonobo.PersistFile)obj;
		}
		else if (obj instanceof org.omg.CORBA.Object)
		{
			return narrow((org.omg.CORBA.Object)obj);
		}
		throw new org.omg.CORBA.BAD_PARAM("Failed to narrow in helper");
	}
	public static org.gnu.bonobo.PersistFile narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
			return null;
		try
		{
			return (org.gnu.bonobo.PersistFile)obj;
		}
		catch (ClassCastException c)
		{
			if (obj._is_a("IDL:Bonobo/PersistFile:1.0"))
			{
				org.gnu.bonobo._PersistFileStub stub;
				stub = new org.gnu.bonobo._PersistFileStub();
				stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
				return stub;
			}
		}
		throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
	}
	public static org.gnu.bonobo.PersistFile unchecked_narrow(final org.omg.CORBA.Object obj)
	{
		if (obj == null)
			return null;
		try
		{
			return (org.gnu.bonobo.PersistFile)obj;
		}
		catch (ClassCastException c)
		{
				org.gnu.bonobo._PersistFileStub stub;
				stub = new org.gnu.bonobo._PersistFileStub();
				stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
				return stub;
		}
	}
}
