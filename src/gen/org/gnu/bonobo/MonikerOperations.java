package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Moniker"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface MonikerOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	org.gnu.bonobo.Moniker getParent();
	void setParent(org.gnu.bonobo.Moniker parent);
	java.lang.String getName() throws org.gnu.bonobo.MonikerPackage.InvalidSyntax;
	void setName(java.lang.String name) throws org.gnu.bonobo.MonikerPackage.InvalidSyntax,org.gnu.bonobo.MonikerPackage.UnknownPrefix;
	org.gnu.bonobo.Unknown resolve(org.gnu.bonobo.ResolveOptions options, java.lang.String requestedInterface) throws org.gnu.bonobo.MonikerPackage.TimeOut,org.gnu.bonobo.MonikerPackage.InterfaceNotFound,org.gnu.bonobo.GeneralError;
	int equal(java.lang.String name);
	void unImplemented1();
	void unImplemented2();
}
