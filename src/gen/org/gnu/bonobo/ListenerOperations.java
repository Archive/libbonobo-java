package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Listener"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface ListenerOperations
	extends org.gnu.bonobo.UnknownOperations
{
	/* constants */
	/* operations  */
	void event(java.lang.String event_name, org.omg.CORBA.Any args);
	void unImplemented();
	void unImplemented2();
}
