package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "UIComponent"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class UIComponentPOATie
	extends UIComponentPOA
{
	private UIComponentOperations _delegate;

	private POA _poa;
	public UIComponentPOATie(UIComponentOperations delegate)
	{
		_delegate = delegate;
	}
	public UIComponentPOATie(UIComponentOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.UIComponent _this()
	{
		return org.gnu.bonobo.UIComponentHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.UIComponent _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.UIComponentHelper.narrow(_this_object(orb));
	}
	public UIComponentOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(UIComponentOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void unImplemented()
	{
_delegate.unImplemented();
	}

	public java.lang.String name()
	{
		return _delegate.name();
	}

	public void ref()
	{
_delegate.ref();
	}

	public java.lang.String describeVerbs()
	{
		return _delegate.describeVerbs();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void unsetContainer()
	{
_delegate.unsetContainer();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void execVerb(java.lang.String cname)
	{
_delegate.execVerb(cname);
	}

	public void setContainer(org.gnu.bonobo.UIContainer container)
	{
_delegate.setContainer(container);
	}

	public void uiEvent(java.lang.String id, org.gnu.bonobo.UIComponentPackage.EventType type, java.lang.String state)
	{
_delegate.uiEvent(id,type,state);
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
