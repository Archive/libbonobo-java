package org.gnu.bonobo;

/**
 *	Generated from IDL interface "RunningContext"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class RunningContextPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.RunningContextOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(0));
		m_opsHash.put ( "ref", new java.lang.Integer(1));
		m_opsHash.put ( "unref", new java.lang.Integer(2));
		m_opsHash.put ( "removeKey", new java.lang.Integer(3));
		m_opsHash.put ( "atExitUnref", new java.lang.Integer(4));
		m_opsHash.put ( "removeObject", new java.lang.Integer(5));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(6));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(7));
		m_opsHash.put ( "addKey", new java.lang.Integer(8));
		m_opsHash.put ( "addObject", new java.lang.Integer(9));
	}
	private String[] ids = {"IDL:Bonobo/RunningContext:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.RunningContext _this()
	{
		return org.gnu.bonobo.RunningContextHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.RunningContext _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.RunningContextHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 1: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 2: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 3: // removeKey
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				removeKey(_arg0);
				break;
			}
			case 4: // atExitUnref
			{
				org.omg.CORBA.Object _arg0=_input.read_Object();
				_out = handler.createReply();
				atExitUnref(_arg0);
				break;
			}
			case 5: // removeObject
			{
				org.omg.CORBA.Object _arg0=_input.read_Object();
				_out = handler.createReply();
				removeObject(_arg0);
				break;
			}
			case 6: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 7: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
			case 8: // addKey
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				addKey(_arg0);
				break;
			}
			case 9: // addObject
			{
				org.omg.CORBA.Object _arg0=_input.read_Object();
				_out = handler.createReply();
				addObject(_arg0);
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
