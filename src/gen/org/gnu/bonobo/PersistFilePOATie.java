package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "PersistFile"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class PersistFilePOATie
	extends PersistFilePOA
{
	private PersistFileOperations _delegate;

	private POA _poa;
	public PersistFilePOATie(PersistFileOperations delegate)
	{
		_delegate = delegate;
	}
	public PersistFilePOATie(PersistFileOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.PersistFile _this()
	{
		return org.gnu.bonobo.PersistFileHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.PersistFile _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.PersistFileHelper.narrow(_this_object(orb));
	}
	public PersistFileOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(PersistFileOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void ref()
	{
_delegate.ref();
	}

	public boolean isDirty()
	{
		return _delegate.isDirty();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public java.lang.String getCurrentFile() throws org.gnu.bonobo.PersistFilePackage.NoCurrentName
	{
		return _delegate.getCurrentFile();
	}

	public void unref()
	{
_delegate.unref();
	}

	public java.lang.String[] getContentTypes()
	{
		return _delegate.getContentTypes();
	}

	public java.lang.String getIId()
	{
		return _delegate.getIId();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public void load(java.lang.String uri) throws org.gnu.bonobo.PersistPackage.WrongDataType,org.gnu.bonobo.IOError,org.gnu.bonobo.PersistPackage.FileNotFound,org.gnu.bonobo.NotSupported
	{
_delegate.load(uri);
	}

	public void save(java.lang.String uri) throws org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported
	{
_delegate.save(uri);
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
