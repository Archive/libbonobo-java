package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ActivationProperty"
 *	@author JacORB IDL compiler 
 */

public final class ActivationPropertyHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.ActivationProperty value;

	public ActivationPropertyHolder ()
	{
	}
	public ActivationPropertyHolder(final org.gnu.bonobo.ActivationProperty initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.ActivationPropertyHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.ActivationPropertyHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.ActivationPropertyHelper.write(_out, value);
	}
}
