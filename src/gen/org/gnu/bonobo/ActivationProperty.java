package org.gnu.bonobo;

/**
 *	Generated from IDL definition of struct "ActivationProperty"
 *	@author JacORB IDL compiler 
 */

public final class ActivationProperty
	implements org.omg.CORBA.portable.IDLEntity
{
	public ActivationProperty(){}
	public java.lang.String name = "";
	public org.gnu.bonobo.ActivationPropertyValue v;
	public ActivationProperty(java.lang.String name, org.gnu.bonobo.ActivationPropertyValue v)
	{
		this.name = name;
		this.v = v;
	}
}
