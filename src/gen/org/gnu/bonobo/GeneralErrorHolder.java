package org.gnu.bonobo;

/**
 *	Generated from IDL definition of exception "GeneralError"
 *	@author JacORB IDL compiler 
 */

public final class GeneralErrorHolder
	implements org.omg.CORBA.portable.Streamable
{
	public org.gnu.bonobo.GeneralError value;

	public GeneralErrorHolder ()
	{
	}
	public GeneralErrorHolder(final org.gnu.bonobo.GeneralError initial)
	{
		value = initial;
	}
	public org.omg.CORBA.TypeCode _type ()
	{
		return org.gnu.bonobo.GeneralErrorHelper.type ();
	}
	public void _read(final org.omg.CORBA.portable.InputStream _in)
	{
		value = org.gnu.bonobo.GeneralErrorHelper.read(_in);
	}
	public void _write(final org.omg.CORBA.portable.OutputStream _out)
	{
		org.gnu.bonobo.GeneralErrorHelper.write(_out, value);
	}
}
