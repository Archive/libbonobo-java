package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Unknown"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface UnknownOperations
{
	/* constants */
	/* operations  */
	void ref();
	void unref();
	org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid);
}
