package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Print"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class PrintPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.PrintOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "ref", new java.lang.Integer(0));
		m_opsHash.put ( "unImplemented4", new java.lang.Integer(1));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(2));
		m_opsHash.put ( "unref", new java.lang.Integer(3));
		m_opsHash.put ( "unImplemented3", new java.lang.Integer(4));
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(5));
		m_opsHash.put ( "render", new java.lang.Integer(6));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(7));
	}
	private String[] ids = {"IDL:Bonobo/Print:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.Print _this()
	{
		return org.gnu.bonobo.PrintHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Print _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.PrintHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 1: // unImplemented4
			{
				_out = handler.createReply();
				unImplemented4();
				break;
			}
			case 2: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 3: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 4: // unImplemented3
			{
				_out = handler.createReply();
				unImplemented3();
				break;
			}
			case 5: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 6: // render
			{
				org.gnu.bonobo.PrintDimensions _arg0=org.gnu.bonobo.PrintDimensionsHelper.read(_input);
				org.gnu.bonobo.PrintScissor _arg1=org.gnu.bonobo.PrintScissorHelper.read(_input);
				_out = handler.createReply();
				org.gnu.bonobo.StreamHelper.write(_out,render(_arg0,_arg1));
				break;
			}
			case 7: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
