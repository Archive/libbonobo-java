package org.gnu.bonobo;

import org.omg.PortableServer.POA;

/**
 *	Generated from IDL interface "ItemContainer"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */

public class ItemContainerPOATie
	extends ItemContainerPOA
{
	private ItemContainerOperations _delegate;

	private POA _poa;
	public ItemContainerPOATie(ItemContainerOperations delegate)
	{
		_delegate = delegate;
	}
	public ItemContainerPOATie(ItemContainerOperations delegate, POA poa)
	{
		_delegate = delegate;
		_poa = poa;
	}
	public org.gnu.bonobo.ItemContainer _this()
	{
		return org.gnu.bonobo.ItemContainerHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.ItemContainer _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.ItemContainerHelper.narrow(_this_object(orb));
	}
	public ItemContainerOperations _delegate()
	{
		return _delegate;
	}
	public void _delegate(ItemContainerOperations delegate)
	{
		_delegate = delegate;
	}
	public POA _default_POA()
	{
		if (_poa != null)
		{
			return _poa;
		}
		else
		{
			return super._default_POA();
		}
	}
	public void unImplemented2()
	{
_delegate.unImplemented2();
	}

	public void ref()
	{
_delegate.ref();
	}

	public void unImplemented3()
	{
_delegate.unImplemented3();
	}

	public void unref()
	{
_delegate.unref();
	}

	public void unImplemented4()
	{
_delegate.unImplemented4();
	}

	public org.gnu.bonobo.Unknown getObjectByName(java.lang.String item_name, boolean only_if_exists) throws org.gnu.bonobo.ItemContainerPackage.NotFound,org.gnu.bonobo.ItemContainerPackage.SyntaxError
	{
		return _delegate.getObjectByName(item_name,only_if_exists);
	}

	public java.lang.String[] enumObjects()
	{
		return _delegate.enumObjects();
	}

	public void unImplemented1()
	{
_delegate.unImplemented1();
	}

	public org.gnu.bonobo.Unknown queryInterface(java.lang.String repoid)
	{
		return _delegate.queryInterface(repoid);
	}

}
