package org.gnu.bonobo;

/**
 *	Generated from IDL interface "Moniker"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public abstract class MonikerPOA
	extends org.omg.PortableServer.Servant
	implements org.omg.CORBA.portable.InvokeHandler, org.gnu.bonobo.MonikerOperations
{
	static private final java.util.Hashtable m_opsHash = new java.util.Hashtable();
	static
	{
		m_opsHash.put ( "unImplemented2", new java.lang.Integer(0));
		m_opsHash.put ( "ref", new java.lang.Integer(1));
		m_opsHash.put ( "equal", new java.lang.Integer(2));
		m_opsHash.put ( "resolve", new java.lang.Integer(3));
		m_opsHash.put ( "unref", new java.lang.Integer(4));
		m_opsHash.put ( "setParent", new java.lang.Integer(5));
		m_opsHash.put ( "setName", new java.lang.Integer(6));
		m_opsHash.put ( "getName", new java.lang.Integer(7));
		m_opsHash.put ( "unImplemented1", new java.lang.Integer(8));
		m_opsHash.put ( "queryInterface", new java.lang.Integer(9));
		m_opsHash.put ( "getParent", new java.lang.Integer(10));
	}
	private String[] ids = {"IDL:Bonobo/Moniker:1.0","IDL:Bonobo/Unknown:1.0"};
	public org.gnu.bonobo.Moniker _this()
	{
		return org.gnu.bonobo.MonikerHelper.narrow(_this_object());
	}
	public org.gnu.bonobo.Moniker _this(org.omg.CORBA.ORB orb)
	{
		return org.gnu.bonobo.MonikerHelper.narrow(_this_object(orb));
	}
	public org.omg.CORBA.portable.OutputStream _invoke(String method, org.omg.CORBA.portable.InputStream _input, org.omg.CORBA.portable.ResponseHandler handler)
		throws org.omg.CORBA.SystemException
	{
		org.omg.CORBA.portable.OutputStream _out = null;
		// do something
		// quick lookup of operation
		java.lang.Integer opsIndex = (java.lang.Integer)m_opsHash.get ( method );
		if ( null == opsIndex )
			throw new org.omg.CORBA.BAD_OPERATION(method + " not found");
		switch ( opsIndex.intValue() )
		{
			case 0: // unImplemented2
			{
				_out = handler.createReply();
				unImplemented2();
				break;
			}
			case 1: // ref
			{
				_out = handler.createReply();
				ref();
				break;
			}
			case 2: // equal
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				_out.write_long(equal(_arg0));
				break;
			}
			case 3: // resolve
			{
			try
			{
				org.gnu.bonobo.ResolveOptions _arg0=org.gnu.bonobo.ResolveOptionsHelper.read(_input);
				java.lang.String _arg1=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,resolve(_arg0,_arg1));
			}
			catch(org.gnu.bonobo.MonikerPackage.TimeOut _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.MonikerPackage.TimeOutHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.MonikerPackage.InterfaceNotFound _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.MonikerPackage.InterfaceNotFoundHelper.write(_out, _ex1);
			}
			catch(org.gnu.bonobo.GeneralError _ex2)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.GeneralErrorHelper.write(_out, _ex2);
			}
				break;
			}
			case 4: // unref
			{
				_out = handler.createReply();
				unref();
				break;
			}
			case 5: // setParent
			{
				org.gnu.bonobo.Moniker _arg0=org.gnu.bonobo.MonikerHelper.read(_input);
				_out = handler.createReply();
				setParent(_arg0);
				break;
			}
			case 6: // setName
			{
			try
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				setName(_arg0);
			}
			catch(org.gnu.bonobo.MonikerPackage.InvalidSyntax _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.MonikerPackage.InvalidSyntaxHelper.write(_out, _ex0);
			}
			catch(org.gnu.bonobo.MonikerPackage.UnknownPrefix _ex1)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.MonikerPackage.UnknownPrefixHelper.write(_out, _ex1);
			}
				break;
			}
			case 7: // getName
			{
			try
			{
				_out = handler.createReply();
				_out.write_string(getName());
			}
			catch(org.gnu.bonobo.MonikerPackage.InvalidSyntax _ex0)
			{
				_out = handler.createExceptionReply();
				org.gnu.bonobo.MonikerPackage.InvalidSyntaxHelper.write(_out, _ex0);
			}
				break;
			}
			case 8: // unImplemented1
			{
				_out = handler.createReply();
				unImplemented1();
				break;
			}
			case 9: // queryInterface
			{
				java.lang.String _arg0=_input.read_string();
				_out = handler.createReply();
				org.gnu.bonobo.UnknownHelper.write(_out,queryInterface(_arg0));
				break;
			}
			case 10: // getParent
			{
				_out = handler.createReply();
				org.gnu.bonobo.MonikerHelper.write(_out,getParent());
				break;
			}
		}
		return _out;
	}

	public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] obj_id)
	{
		return ids;
	}
}
