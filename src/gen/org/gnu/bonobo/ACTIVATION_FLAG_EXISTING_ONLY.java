package org.gnu.bonobo;
/**
 * Automatically generated from IDL const definition 
 * @author JacORB IDL compiler 
 */

public interface ACTIVATION_FLAG_EXISTING_ONLY
{
	int value = 1<<2;
}
