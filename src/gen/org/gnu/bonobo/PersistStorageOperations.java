package org.gnu.bonobo;

/**
 *	Generated from IDL interface "PersistStorage"
 *	@author JacORB IDL compiler V 2.2, 7-May-2004
 */


public interface PersistStorageOperations
	extends org.gnu.bonobo.PersistOperations
{
	/* constants */
	/* operations  */
	void load(org.gnu.bonobo.Storage storage) throws org.gnu.bonobo.PersistPackage.WrongDataType,org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported;
	void save(org.gnu.bonobo.Storage storage, boolean same_as_loaded) throws org.gnu.bonobo.IOError,org.gnu.bonobo.NotSupported;
}
