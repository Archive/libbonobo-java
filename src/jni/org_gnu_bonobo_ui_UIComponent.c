/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <bonobo.h>

#ifndef _Included_org_gnu_bonobo_ui_UIComponent
#define _Included_org_gnu_bonobo_ui_UIComponent
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_gnu_bonobo_ui_UIComponent
 * Method:    bonobo_ui_component_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_UIComponent_bonobo_1ui_1component_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)bonobo_ui_component_get_type();
}

/*
 * Class:     org_gnu_bonobo_ui_UIComponent
 * Method:    bonobo_ui_component_new
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_UIComponent_bonobo_1ui_1component_1new
  (JNIEnv *env, jclass cls, jstring name) 
{
	const char* n = (*env)->GetStringUTFChars(env, name, NULL);
	jint ret = (jint)bonobo_ui_component_new(n);
	(*env)->ReleaseStringUTFChars(env, name, n);
	return ret;
}

/*
 * Class:     org_gnu_bonobo_ui_UIComponent
 * Method:    bonobo_ui_component_new_default
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_UIComponent_bonobo_1ui_1component_1new_1default
  (JNIEnv *env, jclass cls)
{
	return (jint)bonobo_ui_component_new_default();
}

/*
 * Class:     org_gnu_bonobo_ui_UIComponent
 * Method:    bonobo_ui_component_set_name
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_bonobo_ui_UIComponent_bonobo_1ui_1component_1set_1name
  (JNIEnv *env, jclass cls, jint comp, jstring name)
{
	const char* n = (*env)->GetStringUTFChars(env, name, NULL);
	bonobo_ui_component_set_name((BonoboUIComponent*)comp, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
}

/*
 * Class:     org_gnu_bonobo_ui_UIComponent
 * Method:    bonobo_ui_component_get_name
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_bonobo_ui_UIComponent_bonobo_1ui_1component_1get_1name
  (JNIEnv *env, jclass cls, jint comp)
{
	return (*env)->NewStringUTF(env, bonobo_ui_component_get_name((BonoboUIComponent*)comp));
}

/*
 * Class:     org_gnu_bonobo_ui_UIComponent
 * Method:    bonobo_ui_component_set_container
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_org_gnu_bonobo_ui_UIComponent_bonobo_1ui_1component_1set_1container
  (JNIEnv *env, jclass cls, jint comp, jint cont, jint optEv)
{
	bonobo_ui_component_set_container((BonoboUIComponent*)comp, (Bonobo_UIContainer)cont, (CORBA_Environment*)optEv);
}

/*
 * Class:     org_gnu_bonobo_ui_UIComponent
 * Method:    bonobo_ui_component_unset_container
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_bonobo_ui_UIComponent_bonobo_1ui_1component_1unset_1container
  (JNIEnv *env, jclass cls, jint comp, jint optEv)
{
	bonobo_ui_component_unset_container((BonoboUIComponent*)comp, (CORBA_Environment*)optEv);
}

/*
 * Class:     org_gnu_bonobo_ui_UIComponent
 * Method:    bonobo_ui_component_get_container
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_UIComponent_bonobo_1ui_1component_1get_1container
  (JNIEnv *env, jclass cls, jint comp)
{
	return (jint)bonobo_ui_component_get_container((BonoboUIComponent*)comp);
}

#ifdef __cplusplus
}
#endif
#endif
