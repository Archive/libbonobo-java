/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <bonobo.h>

#ifndef _Included_org_gnu_bonobo_ui_Widget
#define _Included_org_gnu_bonobo_ui_Widget
#ifdef __cplusplus
extern "C" {
#endif
/* Inaccessible static: evtMap */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024LifeCycleListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024FocusListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024KeyListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024MouseListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024ExposeListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024MouseMotionListener */
/* Inaccessible static: evtMap */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024ContainerListener */
/*
 * Class:     org_gnu_bonobo_ui_Widget
 * Method:    bonobo_widget_get_objref
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Widget_bonobo_1widget_1get_1objref
  (JNIEnv *env, jclass cls, jint hndl)
{
	return (jint)bonobo_widget_get_objref((BonoboWidget*)hndl);
}

/*
 * Class:     org_gnu_bonobo_ui_Widget
 * Method:    bonobo_widget_new_control
 * Signature: (Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Widget_bonobo_1widget_1new_1control
  (JNIEnv *env, jclass cls, jstring mon, jint uic)
{
	const char* moniker = (*env)->GetStringUTFChars(env, mon, NULL);
	jint ret = (jint)bonobo_widget_new_control(moniker, (Bonobo_UIContainer)uic);
	(*env)->ReleaseStringUTFChars(env, mon, moniker);
	return ret;
}

/*
 * Class:     org_gnu_bonobo_ui_Widget
 * Method:    bonobo_widget_new_control_from_objref
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Widget_bonobo_1widget_1new_1control_1from_1objref
  (JNIEnv *env, jclass cls, jint control, jint uic)
{
	return (jint)bonobo_widget_new_control_from_objref((Bonobo_Control)control, (Bonobo_UIContainer)uic);
}

/*
 * Class:     org_gnu_bonobo_ui_Widget
 * Method:    bonobo_widget_get_control_frame
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Widget_bonobo_1widget_1get_1control_1frame
  (JNIEnv *env, jclass cls, jint hndl)
{
	return (jint)bonobo_widget_get_control_frame((BonoboWidget*)hndl);
}

/*
 * Class:     org_gnu_bonobo_ui_Widget
 * Method:    bonobo_widget_get_ui_container
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Widget_bonobo_1widget_1get_1ui_1container
  (JNIEnv *env, jclass cls, jint hndl)
{
	return (jint)bonobo_widget_get_ui_container((BonoboWidget*)hndl);
}

#ifdef __cplusplus
}
#endif
#endif
