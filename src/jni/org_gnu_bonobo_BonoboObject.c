/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <bonobo.h>


#ifndef _Included_org_gnu_bonobo_BonoboObject
#define _Included_org_gnu_bonobo_BonoboObject
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_gnu_bonobo_BonoboObject
 * Method:    bonobo_object_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_BonoboObject_bonobo_1object_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)bonobo_object_get_type();
}

/*
 * Class:     org_gnu_bonobo_BonoboObject
 * Method:    bonobo_object_add_interface
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_bonobo_BonoboObject_bonobo_1object_1add_1interface
  (JNIEnv *env, jclass cls, jint obj, jint newobj)
{
	bonobo_object_add_interface((BonoboObject*)obj, (BonoboObject*)newobj);
}

/*
 * Class:     org_gnu_bonobo_BonoboObject
 * Method:    bonobo_object_query_local_interface
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_BonoboObject_bonobo_1object_1query_1local_1interface
  (JNIEnv *env, jclass cls, jint obj, jstring repoId)
{
	const char* r = (*env)->GetStringUTFChars(env, repoId, NULL);
	jint ret = (jint)bonobo_object_query_local_interface((BonoboObject*)obj, r);
	(*env)->ReleaseStringUTFChars(env, repoId, r);
	return ret;
}

/*
 * Class:     org_gnu_bonobo_BonoboObject
 * Method:    bonobo_object_ref
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_BonoboObject_bonobo_1object_1ref
  (JNIEnv *env, jclass cls, jint obj)
{
	return (jint)bonobo_object_ref((gpointer)obj);
}

/*
 * Class:     org_gnu_bonobo_BonoboObject
 * Method:    bonobo_object_unref
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_BonoboObject_bonobo_1object_1unref
  (JNIEnv *env, jclass cls, jint obj)
{
	return (jint)bonobo_object_unref((gpointer)obj);
}

#ifdef __cplusplus
}
#endif
#endif
