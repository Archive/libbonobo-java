/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <bonobo.h>


#ifndef _Included_org_gnu_bonobo_ui_Window
#define _Included_org_gnu_bonobo_ui_Window
#ifdef __cplusplus
extern "C" {
#endif
/* Inaccessible static: evtMap */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024LifeCycleListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024FocusListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024KeyListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024MouseListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024ExposeListener */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024MouseMotionListener */
/* Inaccessible static: evtMap */
/* Inaccessible static: class_00024org_00024gnu_00024gtk_00024event_00024ContainerListener */
/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_get_type
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1get_1type
  (JNIEnv *env, jclass cls)
{
	return (jint)bonobo_window_get_type();
}

/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_new
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1new
  (JNIEnv *env, jclass cls, jstring name, jstring title)
{
	const char *n = (*env)->GetStringUTFChars(env, name, NULL);
	const char *t = (*env)->GetStringUTFChars(env, title, NULL);
	jint ret = (jint) bonobo_window_new(n, t);
	(*env)->ReleaseStringUTFChars(env, name, n);
	(*env)->ReleaseStringUTFChars(env, title, t);
	return ret;
}

/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_set_contents
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1set_1contents
  (JNIEnv *env, jclass cls, jint win, jint contents)
{
	bonobo_window_set_contents((BonoboWindow*)win, (GtkWidget*)contents);
}

/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_get_contents
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1get_1contents
  (JNIEnv *env, jclass cls, jint win)
{
	return (jint)bonobo_window_get_contents((BonoboWindow*)win);
}

/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_get_ui_engine
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1get_1ui_1engine
  (JNIEnv *env, jclass cls, jint win)
{
	return (jint)bonobo_window_get_ui_engine((BonoboWindow*)win);
}


/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_get_ui_container
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1get_1ui_1container
  (JNIEnv *env, jclass cls, jint win)
{
	return (jint)bonobo_window_get_ui_container((BonoboWindow*)win);
}

/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_set_name
 * Signature: (ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1set_1name
  (JNIEnv *env, jclass cls, jint win, jstring name)
{
	const char* n = (*env)->GetStringUTFChars(env, name, NULL);
	bonobo_window_set_name((BonoboWindow*)win, n);
	(*env)->ReleaseStringUTFChars(env, name, n);
}

/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_get_name
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1get_1name
  (JNIEnv *env, jclass cls, jint win)
{
	const char *name = bonobo_window_get_name((BonoboWindow*)win);
	return (*env)->NewStringUTF(env, name);
}

/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_get_accel_group
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1get_1accel_1group
  (JNIEnv *env, jclass cls, jint win)
{
	return (jint)bonobo_window_get_accel_group((BonoboWindow*)win);
}

/*
 * Class:     org_gnu_bonobo_ui_Window
 * Method:    bonobo_window_add_popup
 * Signature: (IILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_bonobo_ui_Window_bonobo_1window_1add_1popup
  (JNIEnv *env, jclass cls, jint win, jint popup, jstring path)
{
	const char* p = (*env)->GetStringUTFChars(env, path, NULL);
	bonobo_window_add_popup((BonoboWindow*)win, (GtkMenu*)popup, p);
	(*env)->ReleaseStringUTFChars(env, path, p);
}

#ifdef __cplusplus
}
#endif
#endif
